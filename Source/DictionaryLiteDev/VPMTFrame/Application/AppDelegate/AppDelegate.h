//
//  AppDelegate.h
//  VPMTFrame
//
//  Created by CHUONGPD2 on 6/18/14.
//  Copyright (c) 2014 CHUONGPD2. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MMDrawerController.h"
#import "VerbVC.h"
#import "GrammerVC.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) VerbVC *verbVC;
@property (strong, nonatomic) GrammerVC *oxfordVC;
@property (strong, nonatomic) MMDrawerController *drawerController;
@property (strong, nonatomic) UITabBarController *homeController;
@property (strong,nonatomic) NSMutableArray *arrayRecents;
@property (assign,nonatomic) TYPE_TABLE TYPE_TABLE_CURRENT;
@property (strong,nonatomic) NSString* curTable;
@property (strong,nonatomic) NSString* curIndex;
@property (strong,nonatomic) NSString* contentHtml;
+ (instancetype)sharedManager;

@end
