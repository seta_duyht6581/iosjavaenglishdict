//
//  AppDelegate.m
//  VPMTFrame
//
//  Created by CHUONGPD2 on 6/18/14.
//  Copyright (c) 2014 CHUONGPD2. All rights reserved.
//

#import "AppDelegate.h"

#import "FavoriteVC.h"
#import "RecentVC.h"
#import "SearchVC.h"
#import "TranslateVC.h"
#import "DetailVC.h"
#import "WordDao.h"
#import "LeftSideVC.h"
#import "SSZipArchive.h"
@implementation AppDelegate

static AppDelegate* sharedManager = nil;

+ (instancetype)sharedManager {
    @synchronized(self)
    {
        if (sharedManager && [sharedManager isKindOfClass:[self class]]){
            return sharedManager;
        }else{
            sharedManager = [[self alloc] init];
            
            return sharedManager;
        }
    }
}

- (void)testCode
{

    
    /*BaseDao *dao = [[BaseDao alloc] init];
        NSMutableArray *datas = [[NSMutableArray alloc] init];
        [dao queryWithStatement:@"select _id,explanation,explainorder,sentenceid from stringlist where sentenceid!=''" andProcessBlock:^id(sqlite3_stmt *querystatement) {
            NSNumber *_id = [dao getIntWithStmt:querystatement andIndex:0];
            NSString *explanation = [dao getStringWithStmt:querystatement andIndex:1];
            NSString *explainorder = [dao getStringWithStmt:querystatement andIndex:2];
            NSString *sentenceid = [dao getStringWithStmt:querystatement andIndex:3];
            NSArray *array = @[_id,explanation,explainorder,sentenceid];
         return array;
     } andFinishBlock:^(id result) {
         [datas addObjectsFromArray:result];
         NSLog(@"Query Xong");
         for (NSArray *arr in datas) {
            __block  NSNumber *_id = [arr objectAtIndex:0];
             __block NSString *explanation = [arr objectAtIndex:1];
             __block NSString *explainorder = [arr objectAtIndex:2];
             __block NSString *sentenceid = [arr objectAtIndex:3];
             NSArray *orderIndex = [explainorder componentsSeparatedByString:@","];
             NSArray *senIndex = [sentenceid componentsSeparatedByString:@"@"];
             NSArray *arryexplation = [explanation componentsSeparatedByString:@"\x01"];
             
             __block NSMutableArray *stringSentens = [[NSMutableArray alloc] init];
             for (NSString *sens in senIndex) {
                 
                 NSString *query = [NSString stringWithFormat:@"select sentenceboth from sentenceslist where _id in (%@)",sens];
                 [dao queryNoOperationWithStatement:query andProcessBlock:^id(sqlite3_stmt *querystatement) {
                     NSString *explanation = [dao getStringWithStmt:querystatement andIndex:0];
                     return explanation;
                 } andFinishBlock:^(id result) {
                     NSMutableString *resulStr = [[NSMutableString alloc] init];
                     for (NSString *str in result) {
                         if (resulStr.length>0) {
                             [resulStr appendString:@"###"];
                         }
                         [resulStr appendString:str];
                     }
                     [stringSentens addObject:resulStr];
                     
                 }];

             }
             
             NSMutableString *resulEnd = [[NSMutableString alloc] init];
             NSInteger n = arryexplation.count;
             int ind = 0;
             for (NSInteger i=0 ;i < n;i++) {
                 [resulEnd appendString:[arryexplation objectAtIndex:i]];
                 //cach voi example
                 if (ind < orderIndex.count && [[orderIndex objectAtIndex:ind] integerValue] == i) {
                     [resulEnd appendString:@"***"];
                     [resulEnd appendString:[stringSentens objectAtIndex:ind]];
                      ind++;
                 }
                 //cach voi cac tu vao cau rieng
                 [resulEnd appendString:@"\n"];
             }
             
             
         }

         
     }];*/


    

}

-(void)initFirstController{

    //Unzip database
   [DAO unzipDB];
    
    //test
    //[self testCode];
    
    
    //load Html
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"/demo/basic" ofType:@"html" inDirectory:@"ngon"]];
    self.contentHtml = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    
    
    
    //init StartApp
    STAStartAppSDK* sdk = [STAStartAppSDK sharedInstance];
    sdk.appID = STARTAPP_APP_ID;
    sdk.devID = STARTAPP_DEV_ID;
    if ([DicUltils isShowAds] && [DicUltils canShowFullAds]) {
        [sdk showSplashAd];
    }
    
    //init first
    self.arrayRecents = [[NSMutableArray alloc] init];
    self.TYPE_TABLE_CURRENT = TYPE_EV;
    self.curTable = Table_English;
    self.curIndex = INDEXED_UNSIGN_JAVI;
    
    //create MMDrawable Controller
    //UIViewController * leftSideDrawerViewController = [[LeftVC alloc] init];
    
    UIViewController * leftSideDrawerViewController = [[LeftSideVC alloc] init];
    //Center  View tabbar
    self.homeController = [[UITabBarController alloc] init];
    [self.homeController.tabBar setTintColor:[UIColor colorFromHexString:@"#048be0"]];
    
    SearchVC *view1 = [[SearchVC alloc] init];
    RecentVC *view2 = [[RecentVC alloc] init];
    FavoriteVC *view3 = [[FavoriteVC alloc] init];
    TranslateVC *view4 = [[TranslateVC alloc] init];
    
    
    //can't set this until after its added to the tab bar
    view1.tabBarItem =
    [[UITabBarItem alloc] initWithTitle:BAR_SEARCH
                                  image:[UIImage imageNamed:@"ic_searchbar"]
                                    selectedImage:[UIImage imageNamed:@"i_dictionary_active"]];
    [view1.tabBarItem setTag:1];
    
    view2.tabBarItem =
    [[UITabBarItem alloc] initWithTitle:BAR_RECENT
                                  image:[UIImage imageNamed:@"ic_recent"]
                                    selectedImage:[UIImage imageNamed:@"i_recents_active"]];
    [view2.tabBarItem setTag:2];
    
    view3.tabBarItem =
    [[UITabBarItem alloc] initWithTitle:BAR_FAVORITE
                                  image:[UIImage imageNamed:@"ic_favorite"]
                                     selectedImage:[UIImage imageNamed:@"i_favorites_active"]];
    [view3.tabBarItem setTag:3];
    
    view4.tabBarItem =
    [[UITabBarItem alloc] initWithTitle:BAR_TRANSLATE
                                  image:[UIImage imageNamed:@"ic_trans"]
                                    selectedImage:[UIImage imageNamed:@"ic_trans"]];
    [view4.tabBarItem setTag:4];
    

    NSMutableArray *tabViewControllers = [[NSMutableArray alloc] init];
    [tabViewControllers addObject:view1];
    [tabViewControllers addObject:view2];
    [tabViewControllers addObject:view3];
    [tabViewControllers addObject:view4];
    self.homeController.viewControllers = tabViewControllers;
    //***********

    UINavigationController * leftSideNavController = [[UINavigationController alloc] initWithRootViewController:leftSideDrawerViewController];
    
    self.drawerController = [[MMDrawerController alloc]
                             initWithCenterViewController:self.homeController
                             leftDrawerViewController:leftSideNavController
                             rightDrawerViewController:nil];
    [self.drawerController setShowsShadow:YES];
    [self.drawerController setMaximumLeftDrawerWidth:250.0];
    [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [self.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    UIColor * tintColor = [UIColor colorWithRed:29.0/255.0
                                          green:173.0/255.0
                                           blue:234.0/255.0
                                          alpha:1.0];
    [self.window setTintColor:tintColor];
    
    UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:self.drawerController];
    navi.navigationBarHidden = YES;
    self.drawerController.edgesForExtendedLayout = UIEventSubtypeNone;
    [self.window setRootViewController:navi];
    [self.window makeKeyAndVisible];
    
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    sharedManager = self;

    [self initFirstController];
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{

}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
}

- (void)applicationWillTerminate:(UIApplication *)application
{
}

-(void)applicationDidReceiveMemoryWarning:(UIApplication *)application{
    [[CacheUtils sharedManager] clearAllCache];
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options
{
    
    return YES;
}


@end
