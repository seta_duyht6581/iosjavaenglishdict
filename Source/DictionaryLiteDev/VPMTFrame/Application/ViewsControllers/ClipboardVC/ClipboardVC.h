//
//  ClipboardVC.h
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/18/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import "BaseSearchVC.h"
@class ClipboardVC;
@protocol ClipboardDelegate <NSObject>

-(void)clipboardAndKeyword:(NSString*)keyword;
@end
@interface ClipboardVC : BaseSearchVC
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) id<ClipboardDelegate> delegate;
@end
