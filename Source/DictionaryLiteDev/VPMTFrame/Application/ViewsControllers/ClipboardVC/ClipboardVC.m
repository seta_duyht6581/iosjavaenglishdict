//
//  ClipboardVC.m
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/18/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import "ClipboardVC.h"
#import "WordDao.h"
#import "DetailVC.h"
@interface ClipboardVC ()
{
    NSMutableArray *clipboard;
    WordDao *dao;
}
@end

@implementation ClipboardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    dao = [[WordDao alloc] init];
    clipboard = [[NSMutableArray alloc] init];
    [self createHeaderBarWithTitle:@"Clipboard" andLeftButton:@"i_back_white" andRightButton:nil];
    // Do any additional setup after loading the view from its nib.
}
- (void)viewWillAppear:(BOOL)animated
{
    UIPasteboard * pasteboard=[UIPasteboard generalPasteboard];
    NSString *tmp = [pasteboard string];
    if (tmp!=nil) {
        [clipboard removeAllObjects];
        [clipboard addObjectsFromArray:[tmp componentsSeparatedByString:@" "]];
        [self.tableView reloadData];
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITABLEVIEWDELEGATE
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *key = [clipboard objectAtIndex:indexPath.row];
    if (self.delegate && [self.delegate respondsToSelector:@selector(clipboardAndKeyword:)])
    {
        [self.delegate clipboardAndKeyword:key];
    }
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark UITABLEVIEWDATASOURCE
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [clipboard count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    static NSString *indentifier=@"UITableView";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] init];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.textLabel.text = [clipboard objectAtIndex:indexPath.row];
    return cell;
    
}

- (void)pressRightButton:(id)sender
{
    
}
- (void)pressLeftButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
