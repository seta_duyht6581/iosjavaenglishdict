//
//  TranslateVC.m
//  VPMTFrameExample
//
//  Created by Duy on 10/12/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import "TranslateVC.h"

@interface TranslateVC ()<UIWebViewDelegate,UIScrollViewDelegate>

@end

@implementation TranslateVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self createHeaderBarWithTitle:TITLE_TRANSLATE];
    [self initLayout];
    
}
- (void)viewWillAppear:(BOOL)animated
{
    if ([[Network sharedManager] isNetworkAvaiable])
    {
        self.webview.hidden = NO;
        self.viewNoconnect.hidden = YES;
        [MBProgressHUD showHUDWithString:LOADING addedTo:self.view animated:YES];
        NSString *urlAddress = LINK_TRANSLATE;
        //Create a URL object.
        NSURL *url = [NSURL URLWithString:urlAddress];
        //URL Requst Object
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [self.webview loadRequest:requestObj];
    }else
    {
        self.webview.hidden = YES;
        self.viewNoconnect.hidden = NO;
    }
}
- (void)viewWillDisappear:(BOOL)animated
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initLayout{

    self.webview.delegate = self;
    self.webview.scrollView.delegate = self;
    //Search bar
    
    
}

#pragma mark UIWebviewDelegate
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

@end
