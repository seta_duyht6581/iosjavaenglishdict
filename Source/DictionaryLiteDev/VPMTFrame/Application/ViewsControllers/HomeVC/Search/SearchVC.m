//
//  SearchVC.m
//  VPMTFrameExample
//
//  Created by Duy on 10/12/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import "SearchVC.h"
#import "WordItem.h"
#import "WordSearchCell.h"
#import "WordDao.h"
#import "DetailVC.h"
@interface SearchVC ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIScrollViewDelegate,GADInterstitialDelegate>
{
    UITableView *tableview;
    NSMutableArray *datas;
    WordDao *searchBs;
    DetailVC *detail;
    NSString *keywordCurrent;
    int i ;
}
@end

@implementation SearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
     [self createHeaderBarWithSearchController];
    
     [self initLayout];
    self.searchBar.searchField.delegate = self;
    datas = [[NSMutableArray alloc] init];
    searchBs = [[WordDao alloc] init];
    //search default
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self performSelector:@selector(loadDatabase) withObject:nil afterDelay:0.1];
    //detailVC
    detail = [[DetailVC alloc] init];

}

- (void)setBannerAds
{
    //create ads
    if (!self.bannerView) {
        self.startAppBanner_auto = [[STABannerView alloc] initWithSize:STA_AutoAdSize
                                                            autoOrigin:STAAdOrigin_Bottom
                                                              withView:self.viewCenter withDelegate:nil];
        [self.viewCenter addSubview:self.startAppBanner_auto];
    }

    if (![self.viewCenter viewWithTag:1000]) {
        //Google ads
        self.bannerView.adUnitID = GDA_ADDUNIT_ID_BANNER2;
        self.bannerView.rootViewController = self;
        GADRequest *request = [GADRequest request];
        [self.bannerView loadRequest:request];
        CGRect frame = self.bannerView.frame;
        [self.bannerView removeFromSuperview];
        self.bannerView.frame = frame;
        self.bannerView.tag = 1000;
        [self.viewCenter addSubview:self.bannerView];
    }
    
    
}
- (void)loadDatabase
{
    if ([AppDelegate sharedManager].TYPE_TABLE_CURRENT == TYPE_VE) {
        [self.searchBar.searchField  setPlaceholder:@"English"];
    }else
    {
        [self.searchBar.searchField  setPlaceholder:@"Romaji, 漢字, Hiragana, Katakana"];
    }
    [searchBs searchDefaultTable:[AppDelegate sharedManager].TYPE_TABLE_CURRENT andfinishBlock:^(id result) {
        [datas removeAllObjects];
        [datas addObjectsFromArray:(NSArray*)result];
        [tableview reloadData];
        [tableview setContentOffset:CGPointMake(0, 0) animated: YES];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
}
-(void)initLayout
{
    
    CGRect frame =self.viewCenter.frame;
    tableview = [[UITableView alloc] initWithFrame:CGRectMake(0,0, frame.size.width, frame.size.height)];
    tableview.autoresizingMask = 63;
    tableview.delegate = self ;
    tableview.dataSource = self;
    [self.viewCenter addSubview:tableview];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidAppear:(BOOL)animated
{
    if (self.showKeyboard) {
        [self.searchBar.searchField becomeFirstResponder];
    }

}
-(void)viewWillDisappear:(BOOL)animated
{
    [self.searchBar.searchField resignFirstResponder];
}

- (void)loadAds
{
    if ([DicUltils isShowAds]) {
        if ([[Network sharedManager] isNetworkAvaiable]) {
            [self setBannerAds];
            if([DicUltils canShowFullAds]){
            
                if (i % 2 == 0) {
                    [STAStartAppAdBasic showAd];
                }else {
                    self.interstitial =
                    [[GADInterstitial alloc] initWithAdUnitID:GDA_ADD_FULLSCREEN];
                    [self.interstitial setDelegate:self];
                    GADRequest *requestFill = [GADRequest request];
                    [self.interstitial loadRequest:requestFill];
                }
                i++;
            }
        }

    }
    
}
#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldClear:(UITextField *)textField
{

    [searchBs  searchDefaultTable:[AppDelegate sharedManager].TYPE_TABLE_CURRENT andfinishBlock:^(id result) {
        
        NSArray *data = (NSArray*)result;
        if (data!=nil) {
            [datas removeAllObjects];
            [datas addObjectsFromArray:data];
            [tableview reloadData];
            [tableview setContentOffset:CGPointMake(0, 0) animated: YES];
        }
        
    }];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSString *keywork = [[textField text] stringByReplacingCharactersInRange:range withString:string];
    keywordCurrent = keywork;
    if (keywork == nil || keywork.length == 0) {

              [searchBs  searchDefaultTable:[AppDelegate sharedManager].TYPE_TABLE_CURRENT andfinishBlock:^(id result) {

                    NSArray *data = (NSArray*)result;
                    if (data!=nil) {
                        [datas removeAllObjects];
                        [datas addObjectsFromArray:data];
                        [tableview reloadData];
                        [tableview setContentOffset:CGPointMake(0, 0) animated: YES];
                    }
                
            }];


        return YES;
    }

    //Search by keyword

        [searchBs searchByText:keywork andTable:[AppDelegate sharedManager].TYPE_TABLE_CURRENT andFinishBlock:^(id result) {

            NSArray *data = (NSArray*)result;
            if (data!=nil) {
                [datas removeAllObjects];
                [datas addObjectsFromArray:data];
                [tableview reloadData];
                [tableview setContentOffset:CGPointMake(0, 0) animated: YES];
                
            }
            //type unicode text
            if ([AppDelegate sharedManager].TYPE_TABLE_CURRENT == TYPE_VE) {
                    [self performSelector:@selector(checkUnicodeText) withObject:nil afterDelay:0.1f];
            }
            }];
     
    return YES;
}
-(void)checkUnicodeText
{
    NSString *keyword = [self.searchBar.searchField.text trims];
    if (![keyword canBeConvertedToEncoding:NSASCIIStringEncoding]) {
        //la xau unicode
        [searchBs searchByText:keyword andTable:TYPE_VE andFinishBlock:^(id result) {
            NSArray *data = (NSArray*)result;
            if (data!=nil && [data count]>0) {
                [datas removeAllObjects];
                [datas addObjectsFromArray:data];
                [tableview reloadData];
                [tableview setContentOffset:CGPointMake(0, 0) animated: YES];
                
            }
            
        }];
    }
   

}
#pragma mark SCrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
     [self.searchBar.searchField resignFirstResponder];
}
#pragma mark UITABLEVIEWDELEGATE
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    detail = nil;
    detail = [[DetailVC alloc] init];
    WordItem *item = (WordItem*)[datas objectAtIndex:indexPath.row];
    detail.arrayWordItem = [[NSMutableArray alloc] init];
    [detail.arrayWordItem addObject:item];
    [[AppDelegate sharedManager].drawerController.navigationController pushViewController:detail animated:YES];
}

#pragma mark UITABLEVIEWDATASOURCE
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 58.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [datas count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int row = (int)indexPath.row;
    static NSString *indentifier=@"WordSearchCell";
    WordSearchCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    if(cell == nil)
    {
        cell = (WordSearchCell*)[[NSBundle mainBundle] loadNibNamed:indentifier owner:self options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.lbdetail.text = @"";
        cell.lbword.text = @"";
        cell.lbpronuncia.text = @"";
    }
    WordItem *item = (WordItem*)[datas objectAtIndex:row];
    if ([AppDelegate sharedManager].TYPE_TABLE_CURRENT == TYPE_VE) {
        [cell setDataVEItem:item];
        [cell.lbword setTextColor:[UIColor colorFromHexString:COLOR_VE_TEXT]];
    }else {
        [cell setDataItem:item];
        [cell.lbword setTextColor:[UIColor colorFromHexString:COLOR_EV_TEXT]];
    }

    return cell;
    
}

-(void)pressLeftButton:(id)sender
{
    [super pressLeftButton:sender];
    [self.searchBar.searchField resignFirstResponder];
}
- (void)pressRightButton:(id)sender
{
    [super pressRightButton:sender];
}
#pragma mark GADFullscreenDelegate
/// Called when an interstitial ad request succeeded. Show it at the next transition point in your
/// application such as when transitioning between view controllers.
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad
{
    if (self.interstitial.isReady) {
        [self.interstitial presentFromRootViewController:self];
    }
}


#pragma mark Clipboard Delegate
- (void)clipboardAndKeyword:(NSString *)keyword
{
    [super clipboardAndKeyword:keyword];
    self.searchBar.searchField.text = keyword;
    [searchBs searchByText:keyword andTable:[AppDelegate sharedManager].TYPE_TABLE_CURRENT andFinishBlock:^(id result) {
        
        NSArray *data = (NSArray*)result;
        if (data!=nil) {
            [datas removeAllObjects];
            [datas addObjectsFromArray:data];
            [tableview reloadData];
            [tableview setContentOffset:CGPointMake(0, 0) animated: YES];
            
        }
        //type unicode text
        if ([AppDelegate sharedManager].TYPE_TABLE_CURRENT == TYPE_VE) {
            [self performSelector:@selector(checkUnicodeText) withObject:nil afterDelay:0.1f];
        }
    }];


    
}
@end
