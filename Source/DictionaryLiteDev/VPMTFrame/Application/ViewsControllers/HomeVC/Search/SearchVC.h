//
//  SearchVC.h
//  VPMTFrameExample
//
//  Created by Duy on 10/12/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseSearchVC.h"
@import GoogleMobileAds;
@interface SearchVC : BaseSearchVC
@property(nonatomic, weak) IBOutlet GADBannerView *bannerView;
@property(nonatomic, strong) IBOutlet GADInterstitial *interstitial;
@property(nonatomic, strong)  STABannerView *startAppBanner_auto;

- (void)loadAds;
- (void)loadDatabase;
- (void)setBannerAds;
@end
