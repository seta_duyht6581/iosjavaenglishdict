//
//  RecentVC.m
//  VPMTFrameExample
//
//  Created by Duy on 10/12/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import "RecentVC.h"
#import "DetailVC.h"
#import "WordDao.h"
#import "WordItem.h"
#import "WordSearchCell.h"
#import "FavoriteVC.h"
@interface FavoriteVC ()<UIAlertViewDelegate>
{
    UITableView *tableview;
    NSMutableArray *datas;
    WordDao *wordDao;
    int indexDelete;
}
@end



@implementation FavoriteVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self createHeaderBarWithSearchController];
    //init layout
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
    self.lbfavorite1.text = MESSAGE_DONT_HAVE_FAVORITE;
    self.lbFavorite2.text = MESSAGE_DONT_HAVE_FAVORITE2;
    [self.btnClearall setTitle:BUTTON_DELETE_ALL forState:UIControlStateNormal];
    
    self.searchBar.searchField.delegate = self;
    datas = [[NSMutableArray alloc] init];
    wordDao = [[WordDao alloc] init];
    
    self.viewEmptyData.hidden = YES;
    self.viewCenter.hidden = NO;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
     [self resetData];
}
- (void)viewDidAppear:(BOOL)animated
{
    [[AppDelegate sharedManager].drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];
     //[[AppDelegate sharedManager].drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeNone];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [[AppDelegate sharedManager].drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
}
- (void)resetData
{
    [wordDao getFavoriteTable:[AppDelegate sharedManager].TYPE_TABLE_CURRENT FinishBlock:^(id result) {
                NSArray *data = (NSArray*)result;
                if (data!=nil) {
                    [datas removeAllObjects];
                    [datas addObjectsFromArray:data];
                    if ([datas count] == 0) {
                        self.viewEmptyData.hidden = NO;
                        self.viewCenter.hidden = YES;
                    } else {
                        self.viewEmptyData.hidden = YES;
                        self.viewCenter.hidden = NO;
                        [self.tableView reloadData];
                    }
                }
    }];
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [[AppDelegate sharedManager].homeController setSelectedIndex:0];
    BaseSearchVC *baseVC = (BaseSearchVC*)[[AppDelegate sharedManager].homeController.viewControllers objectAtIndex:0];
    baseVC.showKeyboard = true;
    return NO;
}

- (IBAction)clickClearAll:(id)sender {
  
    [AlertViewUniversal showAlertInViewController:self withTitle:APP_NAME message:MESSAGE_CONFIRM_DELETE cancelButtonTitle:BUTTON_CANCLE destructiveButtonTitle:nil otherButtonTitles:[NSArray arrayWithObjects:BUTTON_OK, nil] tapBlock:^(AlertViewUniversal * _Nonnull alert, NSInteger buttonIndex) {
        if (buttonIndex >= alert.firstOtherButtonIndex) {
            [wordDao removeAllFavoriteAndfinishBlock:^(id result) {
                [self resetData];
            }];
        }
    }];

}


#pragma mark UITABLEVIEWDELEGATE
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    DetailVC *detail = [[DetailVC alloc] init];
    WordItem *item = (WordItem*)[datas objectAtIndex:indexPath.row];
    detail.arrayWordItem = [[NSMutableArray alloc] init];
    [detail.arrayWordItem addObject:item];
    [[AppDelegate sharedManager].drawerController.navigationController pushViewController:detail animated:YES];
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        indexDelete = indexPath.row;
        __weak FavoriteVC *this = self;
        [AlertViewUniversal showAlertInViewController:self withTitle:APP_NAME message:MESSAGE_DELETE_WORD cancelButtonTitle:BUTTON_CANCLE destructiveButtonTitle:nil otherButtonTitles:[NSArray arrayWithObjects:BUTTON_OK, nil] tapBlock:^(AlertViewUniversal * _Nonnull alert, NSInteger buttonIndex) {
            
            if (buttonIndex >= alert.firstOtherButtonIndex) {
                WordItem *item = [datas objectAtIndex:indexDelete];
                [wordDao setFavoriteForWordItem:item isFavorite:!item.favorite.boolValue andfinishBlock:^(id result) {
                    [this resetData];
                }];
            }
            
            
        }];


    }
}
#pragma mark UITABLEVIEWDATASOURCE
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 58.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [datas count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int row = indexPath.row;
    static NSString *indentifier=@"WordSearchCell";
    WordSearchCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    if(cell == nil)
    {
        cell = (WordSearchCell*)[[NSBundle mainBundle] loadNibNamed:indentifier owner:self options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.lbdetail.text = @"";
        cell.lbword.text = @"";
        cell.lbpronuncia.text = @"";
    }
    WordItem *item = (WordItem*)[datas objectAtIndex:row];
    if ([AppDelegate sharedManager].TYPE_TABLE_CURRENT == TYPE_EV) {
        [cell setDataItem:item];
        [cell.lbword setTextColor:[UIColor colorFromHexString:COLOR_EV_TEXT]];
    }else
    {
        [cell setDataVEItem:item];
        [cell.lbword setTextColor:[UIColor colorFromHexString:COLOR_VE_TEXT]];
    }
    return cell;
    
}

@end
