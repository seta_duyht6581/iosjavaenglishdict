//
//  RecentVC.h
//  VPMTFrameExample
//
//  Created by Duy on 10/12/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseSearchVC.h"
@interface FavoriteVC : BaseSearchVC<UITableViewDataSource,UITableViewDelegate>
- (IBAction)clickClearAll:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *viewEmptyData;
@property (weak, nonatomic) IBOutlet UILabel *lbfavorite1;
@property (weak, nonatomic) IBOutlet UILabel *lbFavorite2;
@property (weak, nonatomic) IBOutlet UIButton *btnClearall;

@end
