//
//  WordSearchCell.m
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/16/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import "WordSearchCell.h"
#import "WordItem.h"
@implementation WordSearchCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setDataItem:(WordItem*)item
{
    self.lbword.text = item.fromSign;
    self.lbdetail.text = item.shortmean;
    self.lbpronuncia.text = item.pronuncea;
}
-(void)setDataVEItem:(WordItem*)item
{
    self.lbword.text = item.fromSign;
    self.lbdetail.text = item.shortmean;
    self.lbpronuncia.text = item.pronuncea;
}
@end
