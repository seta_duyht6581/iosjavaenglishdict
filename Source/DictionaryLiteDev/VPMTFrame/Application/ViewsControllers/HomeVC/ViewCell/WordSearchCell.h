//
//  WordSearchCell.h
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/16/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WordItem.h"
@interface WordSearchCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbword;
@property (weak, nonatomic) IBOutlet UILabel *lbpronuncia;
@property (weak, nonatomic) IBOutlet UILabel *lbdetail;
-(void)setDataItem:(WordItem*)item;
-(void)setDataVEItem:(WordItem*)item;
@end
