//
//  AboutVC.m
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/11/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import "RemoveAds.h"

@interface RemoveAds ()<GADInterstitialDelegate>

@end

@implementation RemoveAds

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    // Do any additional setup after loading the view from its nib.
    [self createHeaderBarWithTitle:TITLE_REMOVE_ADS andLeftButton:@"i_back_white" andRightButton:nil];
    [self.btnFull setTitle:BUTTON_FULL forState:UIControlStateNormal];
    [self.tvDescription setText:REMOVE_ADS_MESSAGE];
    [self.btnFull addTarget:self action:@selector(clickBuy:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnRate addTarget:self action:@selector(clickBuy:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnRate setTitle:TITLE_RATEAPP forState:UIControlStateNormal];
}
- (void)clickBuy:(id)sender
{
    //get full
    if ([sender isEqual:self.btnFull]) {
         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:ITUn_FULL_APP]];
    }else
    {
    //rate app
         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:ITUN_MYAPP]];
    }
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)pressLeftButton:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
