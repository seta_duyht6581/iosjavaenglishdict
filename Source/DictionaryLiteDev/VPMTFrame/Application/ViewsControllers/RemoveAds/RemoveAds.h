//
//  AboutVC.h
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/11/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseSearchVC.h"

@interface RemoveAds : BaseSearchVC
@property (weak, nonatomic) IBOutlet UITextView *tvDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnRate;
@property (weak, nonatomic) IBOutlet UIButton *btnFull;
@property (nonatomic,strong) GADInterstitial *interstitial;
@end
