//
//  DetailVC.h
//  VPMTFrameExample
//
//  Created by Duy on 10/13/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseSearchVC.h"
#import "SearchResultV.h"
@import GoogleMobileAds;
@interface DetailVC : BaseSearchVC<SearchResultVDelegate>
@property (nonatomic, strong) SearchResultV *searchResult;
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic, strong) NSMutableArray *arrayWordItem;
@property (weak, nonatomic) IBOutlet UIButton *btnSound;

#pragma mark Define
@property (nonatomic,strong) NSNumber *idOfWord;



@property (nonatomic,strong) GADInterstitial *interstitial;
@end
