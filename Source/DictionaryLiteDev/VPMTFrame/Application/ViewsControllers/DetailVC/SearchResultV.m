//
//  SearchResultV.m
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/14/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import "SearchResultV.h"
#import "DetailVC.h"
#import "WordDao.h"
#import "WordItem.h"
#import "WordSearchCell.h"
@interface SearchResultV () <UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIScrollViewDelegate>
{
    UITableView *tableview;
    NSMutableArray *datas;
    WordDao *searchBs;
    
}
@end

@implementation SearchResultV

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createHeaderBarWithSearchResult];
    self.viewBackground.alpha = 0.5f;
    [self.viewBackground setBackgroundColor:[UIColor blackColor]];
    if ([AppDelegate sharedManager].TYPE_TABLE_CURRENT == TYPE_VE) {
        [self.searchBar.searchField  setPlaceholder:@"English"];
    }else
    {
        [self.searchBar.searchField  setPlaceholder:@"Romaji, 漢字, Hiragana, Katakana"];
    }
    //init layout
    self.searchBar.searchField.delegate = self;
    datas = [[NSMutableArray alloc] init];
    searchBs = [[WordDao alloc] init];

    //layout
    CGRect frame =self.viewCenter.frame;
    tableview = [[UITableView alloc] initWithFrame:CGRectMake(0,0, frame.size.width, frame.size.height)];
    tableview.autoresizingMask = 63;
    tableview.delegate = self ;
    tableview.dataSource = self;
    [self.viewCenter addSubview:tableview];
    self.viewCenter.hidden = YES;
    //gesture hide keyboard
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.viewBackground addGestureRecognizer:singleFingerTap];
    
}
-(void)adjustlayout
{
    
    CGRect frame =self.viewCenter.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    [tableview setFrame:frame];
    if ([datas count] > 0) {
        self.viewCenter.hidden = NO;
    }

}
- (void)resetData
{
    //[datas removeAllObjects];
    //[tableview reloadData];
    //[self.searchBar.searchField setText:@""];
    //self.viewCenter.hidden = YES;
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    
    [self pressRightButton:nil];
    //[self resetData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
   
}

- (void)pressRightButton:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(pressCancle)]) {
        [self.delegate pressCancle];
    }
    //[self resetData];
    
}
#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    self.viewCenter.hidden = YES;
    [datas removeAllObjects];
    [tableview reloadData];
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *keywork = textField.text;
    if (string.length != 0) {
        keywork = [NSString stringWithFormat:@"%@%@",keywork,string];
    }else
    {
        if (keywork.length>0) {
            keywork = [keywork substringToIndex:keywork.length - 1];
        }
    }
    if (keywork == nil || keywork.length == 0) {
        
        self.viewCenter.hidden = YES;
        [datas removeAllObjects];
        [tableview reloadData];
        return YES;
    }
    
    //Search by keyword
    
    [searchBs searchByText:keywork andTable:[AppDelegate sharedManager].TYPE_TABLE_CURRENT andFinishBlock:^(id result) {

            NSArray *data = (NSArray*)result;
            if (data!=nil) {
                [datas removeAllObjects];
                [datas addObjectsFromArray:data];
                [tableview reloadData];
                [tableview setContentOffset:CGPointMake(0, 0) animated: YES];
                if ([data count] >= 1) {
                    self.viewCenter.hidden = NO;
                }else
                {
                    [self.viewCenter setHidden:YES];
                }
            }
    }];
    
    return YES;
}

#pragma mark UITABLEVIEWDELEGATE
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate!= nil && [self.delegate respondsToSelector:@selector(searchResult:didselectedWordItem:)]) {
        [self.delegate searchResult:self didselectedWordItem:[datas objectAtIndex:indexPath.row]];
    }
    [self pressRightButton:nil];
}

#pragma mark UITABLEVIEWDATASOURCE
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 58.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [datas count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    static NSString *indentifier=@"WordSearchCell";
    WordSearchCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    if(cell == nil)
    {
        cell = (WordSearchCell*)[[NSBundle mainBundle] loadNibNamed:indentifier owner:self options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.lbdetail.text = @"";
        cell.lbword.text = @"";
        cell.lbpronuncia.text = @"";
    }
    WordItem *item = (WordItem*)[datas objectAtIndex:row];
    if ([AppDelegate sharedManager].TYPE_TABLE_CURRENT == TYPE_EV) {
        [cell setDataItem:item];
        [cell.lbword setTextColor:[UIColor colorFromHexString:COLOR_EV_TEXT]];
    }else
    {
        [cell setDataVEItem:item];
        [cell.lbword setTextColor:[UIColor colorFromHexString:COLOR_VE_TEXT]];
    }

    return cell;
    
}

@end
