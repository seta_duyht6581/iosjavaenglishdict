//
//  SearchResultV.h
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/14/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseSearchVC.h"
@class SearchResultV;
@protocol SearchResultVDelegate <NSObject>
-(void)pressCancle;
-(void)searchResult:(SearchResultV*)searchResult didselectedWordItem:(WordItem*)wordItem;
@end

@interface SearchResultV : BaseSearchVC
@property (weak, nonatomic) IBOutlet UIView *viewBackground;
@property (strong,nonatomic) UIViewController *detail;
@property (strong,nonatomic) id<SearchResultVDelegate> delegate;
-(void)adjustlayout;
@end
