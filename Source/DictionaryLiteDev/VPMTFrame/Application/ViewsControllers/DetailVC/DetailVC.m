//
//  DetailVC.m
//  VPMTFrameExample
//
//  Created by Duy on 10/13/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import "DetailVC.h"
#import "SearchResultV.h"
#import "WordDao.h"
#import "WordItem.h"
#define TAG_FAVORITE 11
#define TAG_BACK 10

@interface DetailVC ()<AVSpeechSynthesizerDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (weak, nonatomic) IBOutlet UILabel *lbpronunciace;
@property (weak, nonatomic) IBOutlet UILabel *lbnewword;

@property (weak, nonatomic) IBOutlet UIView *viewTop;


@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (strong,nonatomic) WordDao *workDao;
@property (strong,nonatomic) WordItem *worditem;


@end

@implementation DetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self createHeaderBarWithTitle:TITLE_DETAIL andLeftButton:@"i_back_white" andRightButton:@"nav_search_icon"];
    [self initLayout];
    
    // Do any additional setup after loading the view from its nib.
    [self.webView setScalesPageToFit:YES];
    [self.webView.scrollView setScrollEnabled:true];
    
    self.workDao = [[WordDao alloc] init];
    [self.webView setScalesPageToFit:YES];
    [self.webView.scrollView setScrollEnabled:true];
    
    if ([DicUltils isShowAds] && [[Network sharedManager] isNetworkAvaiable]) {
        STABannerView *startAppBanner_auto = [[STABannerView alloc] initWithSize:STA_AutoAdSize
                                                                      autoOrigin:STAAdOrigin_Bottom
                                                                        withView:self.webView withDelegate:nil];
        [self.webView addSubview:startAppBanner_auto];
        

        self.interstitial =
        [[GADInterstitial alloc] initWithAdUnitID:GDA_ADD_FULLSCREEN];
        if ([DicUltils canShowFullAds]){
            GADRequest *request = [GADRequest request];
            [self.interstitial loadRequest:request];
        }
    }
    
    
    
}
- (void)lookUpProcessWithResult:(id)result
{
    [super lookUpProcessWithResult:result];
    WordItem *item = (WordItem*)[result objectAtIndex:0];
    [self.arrayWordItem addObject:item];
    self.worditem  = item;
    [self  setDataTolayout];
}
- (void)viewDidDisappear:(BOOL)animated
{
}

-(void)initLayout{
    //set background
    
    UIButton *btn_back = [[UIButton alloc] initWithFrame:CGRectMake(20, 0, 40, 40)];
    [btn_back setTag:TAG_BACK];
    [btn_back setImage:[UIImage imageNamed:@"i_w_back"] forState:UIControlStateNormal];
    [btn_back addTarget:self action:@selector(clickBack:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnFavo = [[UIButton alloc] initWithFrame:CGRectMake(270, 0, 40, 40)];
    [btnFavo setImage:[UIImage imageNamed:@"ic_favorite"] forState:UIControlStateNormal];
    [btnFavo addTarget:self action:@selector(clickFavo:) forControlEvents:UIControlEventTouchUpInside];
    [btnFavo setTag:TAG_FAVORITE];
    
    /*UIButton *btnNew = [[UIButton alloc] initWithFrame:CGRectMake(260, 0, 40, 40)];
    [btnNew setImage:[UIImage imageNamed:@"ic_note"] forState:UIControlStateNormal];
    [btnNew addTarget:self action:@selector(clickNew:) forControlEvents:UIControlEventTouchUpInside];*/
    //[self.toolbar addSubview:btnNew];
    [self.toolbar addSubview:btnFavo];
    [self.toolbar addSubview:btn_back];
    
    [self.toolbar setBackgroundImage:[UIImage imageNamed:@"tabbar_bg"] forToolbarPosition:UIBarPositionBottom barMetrics:UIBarMetricsDefault];
    //toolbar top
    
   
    
    CGRect frame = self.hearderView.frame;
    CGRect frameTopbar = self.viewTop.frame;
    frameTopbar.origin.y = frame.origin.y + frame.size.height;
    self.viewTop.frame = frameTopbar;
    
    //webview
    frame = self.webView.frame;
    frame.size.height = self.toolbar.frame.origin.y - (self.viewTop.frame.origin.y+self.viewTop.frame.size.height);
    frame.origin.y = self.viewTop.frame.size.height + self.viewTop.frame.origin.y;
    [self.webView setFrame:frame];
    
    
    //Search bar
    self.searchResult = [[SearchResultV alloc] init];
    self.searchResult.view.hidden = YES;
    self.searchResult.delegate =self;
    [self.view addSubview:self.searchResult.view];
    

}
- (void)setDataTolayout
{

    if (self.worditem == nil) {
        return;
    }
    
        self.lbnewword.text = self.worditem.fromSign;
        self.lbpronunciace.text = self.worditem.pronuncea;
        [self.lbnewword setTextColor:[UIColor colorFromHexString:COLOR_EV_TEXT]];

    

    
    NSString *stringHtml;
    stringHtml = [DicUltils parseMeanEVWordtoHtml:self.worditem];
    [self.webView loadHTMLString:stringHtml baseURL:nil];
   
    //save recents;
    WordItem *itemtmp ;
    for (WordItem *item in [AppDelegate sharedManager].arrayRecents) {
        if ([item.idOfword longValue] == [[self.worditem idOfword] longValue]) {
            itemtmp = item;
            break;
        }
    }
    [[AppDelegate sharedManager].arrayRecents removeObject:itemtmp];
    itemtmp = [[self arrayWordItem] lastObject];
    [[AppDelegate sharedManager].arrayRecents  insertObject:itemtmp atIndex:0];
    
    if ([self.arrayWordItem count] == 1) {
        UIView *view = [self.toolbar viewWithTag:TAG_BACK];
        view.hidden = YES;
    }else
    {
        UIView *view = [self.toolbar viewWithTag:TAG_BACK];
        view.hidden = NO;
    }
    UIButton *btnFavo = (UIButton*)[self.toolbar viewWithTag:TAG_FAVORITE];
    if ([self.worditem.favorite intValue] == 1) {
        [btnFavo setImage:[UIImage imageNamed:@"i_favorites_active"] forState:UIControlStateNormal];
    }else
    {
        [btnFavo setImage:[UIImage imageNamed:@"ic_favorite"] forState:UIControlStateNormal];
    }
    
}


    

-(void)viewWillAppear:(BOOL)animated
{
    self.btnSound.enabled = true;
    self.worditem = (WordItem*)[self.arrayWordItem objectAtIndex:0];
    [self setDataTolayout];
    self.btnSound.hidden = NO;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)pressLeftButton:(id)sender
{

    [self.navigationController popViewControllerAnimated:YES];
    

}
- (void)pressRightButton:(id)sender
{

    [[AppDelegate sharedManager].drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];
    
    CGRect frame = self.searchResult.view.frame;
    frame.origin.x = 320;
    frame.origin.y = 20;
    frame.size.width = 0;
    frame.size.height = 0;
    self.searchResult.view.frame = frame;
    self.searchResult.view.hidden = NO;
    self.searchResult.viewCenter.hidden = YES;
    [UIView animateWithDuration:0.2f
                     animations:^{

                         CGRect frame = self.searchResult.view.frame;
                         frame.origin.x = 0;
                         frame.origin.y = 0;
                         frame.size.width = self.view.frame.size.width;
                         frame.size.height = self.view.frame.size.height;
                         self.searchResult.view.frame = frame;
                         
                     }
                     completion:^(BOOL finished) {
                         
                         [self.searchResult adjustlayout];
                         [self.searchResult.searchBar.searchField becomeFirstResponder];
                     }];

 
}
#pragma mark - Action
- (IBAction)clickFavo:(id)sender {
    __block UIButton *btnFavo = (UIButton*)[self.toolbar viewWithTag:TAG_FAVORITE];
    WordItem *item =self.worditem;
    self.worditem.favorite =  [NSNumber numberWithBool:!self.worditem.favorite.boolValue];
    [self.workDao setFavoriteForWordItem:self.worditem isFavorite:self.worditem.favorite.intValue andfinishBlock:^(id result) {
        item.favorite = [NSNumber numberWithInt:item.favorite.intValue];
        if (item.favorite.boolValue) {
            [btnFavo setImage:[UIImage imageNamed:@"i_favorites_active"] forState:UIControlStateNormal];
        }else
        {
            [btnFavo setImage:[UIImage imageNamed:@"ic_favorite"] forState:UIControlStateNormal];
        }

    }];
}
/**
 *  click back webview
 *
 *  @param sender <#sender description#>
 */
- (IBAction)clickBack:(id)sender {
    [self.arrayWordItem removeLastObject];
    self.worditem  = (WordItem*)[self.arrayWordItem lastObject];
    [self  setDataTolayout];
}

- (IBAction)clickSound:(id)sender {
    [[TextToSpeech sharedManager] synthesizer].delegate = self;
    
    if (self.worditem.tableType == TYPE_VE) {
        [[TextToSpeech sharedManager] speakText:self.worditem.fromSign andLangue:English_United_States];
    }else {
        [[TextToSpeech sharedManager] speakText:self.worditem.fromSign andLangue:Japanese_Japan];
    }
}

#pragma mark - SearchResultDelegate
- (void)pressCancle
{

    [UIView animateWithDuration:0.2f
                     animations:^{
                         
                         CGRect frame = self.searchResult.view.frame;
                         frame.origin.x = 320;
                         frame.origin.y = 20;
                         frame.size.width = 0;
                         frame.size.height = 0;
                         self.searchResult.view.frame = frame;
                     }
                     completion:^(BOOL finished) {
                         
                          self.searchResult.view.hidden = YES;
                         [self.searchResult.searchBar.searchField resignFirstResponder];
                         [[AppDelegate sharedManager].drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
                     }];
}
-(void)searchResult:(SearchResultV *)searchResult didselectedWordItem:(WordItem *)wordItem
{
    [self.arrayWordItem addObject:wordItem];
    self.worditem  = wordItem;
    [self  setDataTolayout];
}

#pragma mark - AVSpeechSynthesizerDelegate
- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didStartSpeechUtterance:(AVSpeechUtterance *)utterance
{
    self.btnSound.enabled = false;
}
- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance
{
    self.btnSound.enabled = true;
}
@end
