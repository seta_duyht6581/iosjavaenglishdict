

#import "BaseVC.h"
#import "MMDrawerBarButtonItem.h"
@interface BaseVC ()

@end

@implementation BaseVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
}
//create header bar
- (void)createNavigationBar
{
    UIColor * barColor = [UIColor
                          colorWithRed:247.0/255.0
                          green:249.0/255.0
                          blue:250.0/255.0
                          alpha:1.0];
    [self.navigationController.navigationBar setBarTintColor:barColor];
    [self.navigationController.view.layer setCornerRadius:10.0f];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor greenColor]];
    self.edgesForExtendedLayout = UIRectEdgeNone;

}
- (void) createNavigationBarWithBack
{
    [self createNavigationBar];
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];

}

- (void) createNavigationBarWithLeftButton:(NSString*)iconLeft
                             andRighButton:(NSString*)rightIcon
{
    [self createNavigationBar];
    
    if (iconLeft!=nil) {
        UIBarButtonItem * leftDrawerButton =[[UIBarButtonItem alloc] init];
        [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
    }
    if (rightIcon!=nil) {
        MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
        [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
    }

    
}

#pragma mark - Action
- (void)leftDrawerButtonPress:(id)sender
{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

- (void)viewWillAppear:(BOOL)animated
{

}

- (void)viewDidAppear:(BOOL)animated
{
    
}


@end
