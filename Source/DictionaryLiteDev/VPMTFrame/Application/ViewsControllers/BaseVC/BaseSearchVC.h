//
//  BaseSearchVC.h
//  VPMTFrameExample
//
//  Created by Duy on 10/12/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHSearchBar.h"
@interface BaseSearchVC : UIViewController<UITextFieldDelegate>
@property (strong,nonatomic) DHSearchBar *searchBar;
@property (assign,nonatomic) BOOL showKeyboard;
@property (strong,nonatomic) UIView *hearderView;
@property (strong,nonatomic) UILabel *lbTitle;
@property (strong,nonatomic) UIButton *leftButton;
@property (strong,nonatomic) UIButton *rightButton;
@property (weak, nonatomic) IBOutlet UIView *viewCenter;
- (void)pressLeftButton:(id)sender;
- (void)pressRightButton:(id)sender;
- (BOOL)textFieldShouldReturn:(UITextField *)textField;
- (void)createHeaderBarWithTitle:(NSString*)title;
- (void)createHeaderBarWithSearchController;
- (void)createHeaderBarWithTitle:(NSString*)title
                   andLeftButton:(NSString*)imageLName
                  andRightButton:(NSString*)imageRName;
- (void)createHeaderBarWithSearchResult;
- (void)createHeaderBarNavi:(NSString*)title;
-(void)clipboardAndKeyword:(NSString*)keyword;
/**
 *  @author DuyHt, 15-11-14 21:11:25
 *
 *  Xu ly luc tim kiem bang selected tu menu item
 *
 *  @param result du lieu dau vao
 */
- (void)lookUpProcessWithResult:(id) result;
@end
