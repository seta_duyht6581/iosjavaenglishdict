

#import <UIKit/UIKit.h>

@interface BaseVC : UIViewController

- (void)createNavigationBar;
- (void) createNavigationBarWithBack;
- (void)leftDrawerButtonPress:(id)sender;
@end
