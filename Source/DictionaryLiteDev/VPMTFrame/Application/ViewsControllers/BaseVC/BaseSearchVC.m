//
//  BaseSearchVC.m
//  VPMTFrameExample
//
//  Created by Duy on 10/12/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//
#define HEIGHT_HEADER 66.0f
#import "BaseSearchVC.h"
#import "DHSearchBar.h"
#import "ClipboardVC.h"
#import "WordDao.h"
#define HEIGHT_BUTTON 40.0f
#define WIDTH_BUTTON 50.0f
#define Y_BUTTON 20.0f
@interface BaseSearchVC ()<ClipboardDelegate>
{
    int paddingOfButton;
    NSInteger widofdevice;
    NSInteger heiofdevice;
}
@end

@implementation BaseSearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    paddingOfButton = 2;
    widofdevice = [DeviceUtils screenWidth];
    heiofdevice = [DeviceUtils screenHeight];
    // Do any additional setup after loading the view.
    
    //addjust layout
    //webview
    if (self.viewCenter != nil) {
        CGRect frame = self.viewCenter.frame;
        frame.origin.y += 20;
        frame.size.height -= 20;
        self.viewCenter.frame = frame;
    }
  
    UIMenuItem *customMenuItem1 = [[UIMenuItem alloc] initWithTitle:STRING_TRANSLATE action:@selector(lookupword)];
    [[UIMenuController sharedMenuController] setMenuItems:[NSArray arrayWithObjects:customMenuItem1, nil]];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CreateHeader
- (void)createHeader
{
    self.hearderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, widofdevice, HEIGHT_HEADER)];
    /*
     UIViewAutoresizingFlexibleLeftMargin   = 1 << 0,
     UIViewAutoresizingFlexibleWidth        = 1 << 1,
     UIViewAutoresizingFlexibleRightMargin  = 1 << 2,
     UIViewAutoresizingFlexibleTopMargin    = 1 << 3,
     UIViewAutoresizingFlexibleHeight       = 1 << 4,
     UIViewAutoresizingFlexibleBottomMargin = 1 << 5
     */
    self.hearderView.autoresizingMask = UIViewAutoresizingFlexibleWidth ;
    [self.hearderView setBackgroundColor:[UIColor colorFromHexString:@"#0986a1"]];
}
- (void)createHeaderBarWithTitle:(NSString*)title
                   andLeftButton:(NSString*)imageLName
                  andRightButton:(NSString*)imageRName
{


    [self createHeader];
    if (imageLName!=nil) {
        self.leftButton = [[UIButton alloc] initWithFrame:CGRectMake(paddingOfButton ,Y_BUTTON, HEIGHT_BUTTON, WIDTH_BUTTON)];
        [self.leftButton setImage:[UIImage imageNamed:imageLName] forState:UIControlStateNormal];
        [self.leftButton addTarget:self action:@selector(pressLeftButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.hearderView addSubview:self.leftButton];
    }

    if (imageRName!=nil) {
        //Right Button
        self.rightButton = [[UIButton alloc] initWithFrame:CGRectMake(widofdevice - 40 -paddingOfButton,Y_BUTTON, HEIGHT_BUTTON, WIDTH_BUTTON)];
        [self.rightButton addTarget:self action:@selector(pressRightButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.rightButton setImage:[UIImage imageNamed:@"nav_search_icon"] forState:UIControlStateNormal];
        self.rightButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin ;
        [self.hearderView addSubview:self.rightButton];
    }
    
    
    if (title!=nil) {
        //Add search controller
        self.lbTitle = [[UILabel alloc] init];
        self.lbTitle.frame = CGRectMake(55, 26, widofdevice - 110,34);
        self.lbTitle.textAlignment = NSTextAlignmentCenter;
        [self.lbTitle setBackgroundColor:[UIColor clearColor]];
        [self.lbTitle setFont:[UIFont systemFontOfSize:17.0f]];
        [self.lbTitle setTextColor:[UIColor whiteColor]];
        [self.lbTitle setText:title];
        [self.hearderView addSubview:self.lbTitle];
    }

    [self.view addSubview:self.hearderView];
    
    //Add view to self.view
}

- (void)createHeaderBarWithTitle:(NSString*)title
{

    [self createHeader];
    

    //Left Button
    self.leftButton = [[UIButton alloc] initWithFrame:CGRectMake(paddingOfButton ,Y_BUTTON, HEIGHT_BUTTON, WIDTH_BUTTON)];
    self.leftButton.contentMode = UIViewContentModeScaleAspectFit;
    [self.leftButton setImage:[UIImage imageNamed:@"btn_left"] forState:UIControlStateNormal];

    [self.leftButton addTarget:self action:@selector(pressLeftButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.hearderView addSubview:self.leftButton];
    
    
    //Add search controller
    self.lbTitle = [[UILabel alloc] init];
    self.lbTitle.frame = CGRectMake(55, 26, widofdevice - 110,34);
    self.lbTitle.textAlignment = NSTextAlignmentCenter;
    [self.lbTitle setBackgroundColor:[UIColor clearColor]];
    [self.lbTitle setFont:[UIFont systemFontOfSize:17.0f]];
    [self.lbTitle setTextColor:[UIColor whiteColor]];
    [self.lbTitle setText:title];
    [self.hearderView addSubview:self.lbTitle];
    [self.view addSubview:self.hearderView];
    
    //Add view to self.view
}
- (void)createHeaderBarNavi:(NSString*)title
{
    
     [self createHeader];
    
    //Add search controller
    self.lbTitle = [[UILabel alloc] init];
    self.lbTitle.frame = CGRectMake(15, 26, 210,34);
    self.lbTitle.textAlignment = NSTextAlignmentCenter;
    [self.lbTitle setBackgroundColor:[UIColor clearColor]];
    [self.lbTitle setFont:[UIFont systemFontOfSize:17.0f]];
    [self.lbTitle setTextColor:[UIColor whiteColor]];
    [self.lbTitle setText:title];
    [self.hearderView addSubview:self.lbTitle];
    [self.view addSubview:self.hearderView];
    
    //Add view to self.view
}

-(void)createHeaderBarWithSearchController
{

     [self createHeader];
    
    //Left Button
    self.leftButton = [[UIButton alloc] initWithFrame:CGRectMake(paddingOfButton ,Y_BUTTON, HEIGHT_BUTTON, WIDTH_BUTTON)];
    self.leftButton.contentMode = UIViewContentModeScaleAspectFit;
    [self.leftButton setImage:[UIImage imageNamed:@"btn_left"] forState:UIControlStateNormal];
    [self.leftButton addTarget:self action:@selector(pressLeftButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.hearderView addSubview:self.leftButton];
    
    //Right Button
    self.rightButton = [[UIButton alloc] initWithFrame:CGRectMake(widofdevice - 40 -paddingOfButton,Y_BUTTON-3, HEIGHT_BUTTON, WIDTH_BUTTON)];
    self.rightButton.contentMode = UIViewContentModeScaleAspectFit;
    [self.rightButton setImage:[UIImage imageNamed:@"general_pasteboard_icon"] forState:UIControlStateNormal];
    [self.rightButton addTarget:self action:@selector(pressRightButton:) forControlEvents:UIControlEventTouchUpInside];
    self.rightButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    [self.hearderView addSubview:self.rightButton];//general_pasteboard_icon
    
    //Add search controller
    self.searchBar = [[DHSearchBar alloc] initWithFrame:CGRectMake(45, 26,widofdevice - 90,34)];
    self.searchBar.searchField.delegate = self;
    [self.hearderView addSubview:self.searchBar];
    
    //Add view to self.view
    [self.view addSubview:self.hearderView];

}

-(void)createHeaderBarWithSearchResult
{
    int y = 22;
    [self createHeader];

    
    //Right Button
    self.rightButton = [[UIButton alloc] initWithFrame:CGRectMake(widofdevice - 60 -paddingOfButton,y, 65, 40)];
    self.rightButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin ;
    [self.rightButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [self.rightButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [self.rightButton setTitleColor:[UIColor grayColor] forState:UIControlStateSelected];
    [self.rightButton addTarget:self action:@selector(pressRightButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.hearderView addSubview:self.rightButton];//general_pasteboard_icon
    
    //Add search controller
    self.searchBar = [[DHSearchBar alloc] initWithFrame:CGRectMake(5, 26, widofdevice - 70,34)];
    self.searchBar.searchField.delegate = self;
    [self.hearderView addSubview:self.searchBar];
    
    //Add view to self.view
    [self.view addSubview:self.hearderView];
    
}

#pragma mark - Action
- (void)pressLeftButton:(id)sender
{
    [[AppDelegate sharedManager].drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
    [self.searchBar.searchField resignFirstResponder];
}
- (void)pressRightButton:(id)sender
{
    [self.searchBar.searchField resignFirstResponder];
    ClipboardVC *clip = [[ClipboardVC alloc] init];
    clip.delegate = self;
    [self.navigationController pushViewController:clip animated:YES];
    
}

#pragma mark - UITextFiledDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{

    [self.searchBar.searchField resignFirstResponder];
    return YES;
}

#pragma mark ClipboardDelegate
-(void)clipboardAndKeyword:(NSString*)keyword
{
    keyword = [keyword trims];
}

#pragma mark Lookup
- (void)lookupword
{
    [[UIApplication sharedApplication] sendAction:@selector(copy:) to:nil from:self forEvent:nil];
    NSString *text =  [UIPasteboard generalPasteboard].string;
    __block WordDao *dao = [[WordDao alloc] init];
    if ([AppDelegate sharedManager].TYPE_TABLE_CURRENT == TYPE_EV || [AppDelegate sharedManager].TYPE_TABLE_CURRENT == TYPE_VE  ) {
        [dao searchByText:text andTable:[AppDelegate sharedManager].TYPE_TABLE_CURRENT andFinishBlock:^(id result) {
            NSArray *data = (NSArray*)result;
            if (data!=nil && data.count > 0) {
                [self lookUpProcessWithResult:result];
                
            }else
            {
                [AlertViewUniversal showAlertInViewController:self withTitle:APP_NAME message:MESSAGE_NOTFOUND_WORD cancelButtonTitle:BUTTON_OK destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:nil];
                
            }
            dao = nil;
        }];
    }else if ([AppDelegate sharedManager].TYPE_TABLE_CURRENT == TYPE_KANJI)
    {
        
        [dao searchKanjiKey:text andFinishBlock:^(id result) {
            NSArray *data = (NSArray*)result;
            if (data!=nil && data.count > 0) {
                [self lookUpProcessWithResult:result];
                
            }else
            {
                [AlertViewUniversal showAlertInViewController:self withTitle:APP_NAME message:MESSAGE_NOTFOUND_WORD cancelButtonTitle:BUTTON_OK destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:nil];
                
            }
        }];

    }

}
- (void)lookUpProcessWithResult:(id) result
{
    
}
@end
