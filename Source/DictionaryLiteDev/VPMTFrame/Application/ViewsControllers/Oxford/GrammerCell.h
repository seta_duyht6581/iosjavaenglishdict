//
//  MyCell.h
//  VPMTFrameExample
//
//  Created by Duy Hoang on 9/18/14.
//  Copyright (c) 2014 CHUONGPD2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GrammerItem.h"
@class GrammerCell;
@protocol GrammerCellDelegate <NSObject>

-(void)oxfordCell:(GrammerCell*)verbCell andButton:(id)sender;
@end

@interface GrammerCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbPresenter;
@property (weak, nonatomic) IBOutlet UILabel *lbPast;

-(void)setDataItem:(GrammerItem*)data1;

@property (weak, nonatomic) id<GrammerCellDelegate> delegate;
- (IBAction)touchFavorite:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnFavorite;
@end
