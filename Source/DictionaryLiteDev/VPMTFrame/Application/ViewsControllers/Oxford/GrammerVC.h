//
//  RecentVC.h
//  VPMTFrameExample
//
//  Created by Duy on 10/12/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseSearchVC.h"
@import GoogleMobileAds;
@interface GrammerVC : BaseSearchVC<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic, weak) IBOutlet GADBannerView *bannerView;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segment;
- (IBAction)segmentChanged:(id)sender;
- (IBAction)clearAllFavorite:(id)sender;
@end
