//
//  MyCell.m
//  VPMTFrameExample
//
//  Created by Duy Hoang on 9/18/14.
//  Copyright (c) 2014 CHUONGPD2. All rights reserved.
//

#import "GrammerCell.h"

@implementation GrammerCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)touchFavorite:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(oxfordCell:andButton:)])
    {
        [self.delegate oxfordCell:self andButton:sender];
    }
}
-(void)setDataItem:(GrammerItem*)data1
{
    self.lbPast.text = [NSString stringWithFormat:@"%@",data1.fromStr];
    self.lbPresenter.text = [NSString stringWithFormat:@"JLPT N%ld",data1.level.integerValue];
    
    if(data1.level.intValue == 2)
    {
        self.lbPresenter.text = [NSString stringWithFormat:@"JLPT N%ld, N3",data1.level.integerValue];
    }
        
    if (data1.favorite.boolValue) {
        [self.btnFavorite setImage:[UIImage imageNamed:@"i_favorites_active"] forState:UIControlStateNormal];
    }else
    {
        [self.btnFavorite setImage:[UIImage imageNamed:@"ic_favorite"] forState:UIControlStateNormal];
    }
}

@end
