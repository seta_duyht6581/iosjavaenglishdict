//
//  RecentVC.m
//  VPMTFrameExample
//
//  Created by Duy on 10/12/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import "VerbVC.h"
#import "DetailGrammer.h"
#import "WordDao.h"
#import "GrammerCell.h"
#import "GrammerVC.h"
#import "VerbCell.h"
#import "ValidateUtils.h"
@interface GrammerVC ()<UIAlertViewDelegate,UIScrollViewDelegate,GrammerCellDelegate>
{
    
    BOOL isSearch;
    NSMutableArray *datas;
    NSMutableArray *dataSearch;
    WordDao *dao;
    NSInteger selectedIndex;
    int index[3300];
}
@end



@implementation GrammerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self createHeaderBarWithSearchController];
    
    self.searchBar.searchField.placeholder = PLACEHOLDER_GRAMMAR;
    [self.segment setTitle:SEG_ALL forSegmentAtIndex:0];
    [self.segment setTitle:SEG_FAVORITE forSegmentAtIndex:1];
    self.rightButton.hidden = YES;
    //init layout
    self.searchBar.searchField.delegate = self;
    datas = [[NSMutableArray alloc] init];
    dataSearch = [[NSMutableArray alloc] init];
    dao = [[WordDao alloc] init];
    [self loadDatabase];
    
    //GDA ads
    self.bannerView.adUnitID = GDA_ADDUNIT_ID_BANNER2;
    self.bannerView.rootViewController = self;
    


}
- (void)loadDatabase
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [dao getGrammarFinishBlock:^(id result) {
        [datas removeAllObjects];
        [datas addObjectsFromArray:result];
        NSInteger n = [datas count];
        for (int i = 0; i < n; i++) {
            index[i] = i+1;
        }
        [self.tableView reloadData];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidAppear:(BOOL)animated
{
    if ([DicUltils isShowAds] && [[Network sharedManager] isNetworkAvaiable]) {
        GADRequest *request = [GADRequest request];
        [self.bannerView loadRequest:request];
    }
    if ([datas count]>0) {
        if (self.segment.selectedSegmentIndex == 0) {
            
            [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:selectedIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        }else
        {
            [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:selectedIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        }
    }

}
#pragma mark UITextFiledDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}
- (BOOL) textFieldShouldClear:(UITextField *)textField
{
    isSearch = false;
    [self.tableView reloadData];
    return YES;
}

#pragma mark SCrollViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.searchBar.searchField resignFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *keywork = textField.text;
    if (string.length != 0) {
        keywork = [NSString stringWithFormat:@"%@%@",keywork,string];
    }else
    {
        if (keywork.length>0) {
            keywork = [keywork substringToIndex:keywork.length - 1];
        }
    }
    if (keywork == nil || keywork.length == 0) {
        isSearch = false;
        [self.tableView reloadData];
        //Search by default
        return YES;
    }
    
    //Search by keyword
    isSearch =true;
    if ([keywork containsString:@"3"]) {
        keywork = @"2";
    }
    NSString *queryString = [NSString stringWithFormat:@"SELF.searchStr == '%@'",[keywork trims]];
    NSPredicate *keywordPredicate = [NSPredicate predicateWithFormat:queryString];
    NSArray *beginWithB = [datas filteredArrayUsingPredicate:keywordPredicate];
    [dataSearch removeAllObjects];
    [dataSearch addObjectsFromArray:beginWithB];
    [self.tableView reloadData];
    return YES;
}

#pragma mark UITABLEVIEWDELEGATE
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    DetailGrammer *detail = [[DetailGrammer alloc] init];
    GrammerItem *item = nil;
    selectedIndex = indexPath.row;
    if (isSearch) {
        item = [dataSearch objectAtIndex:indexPath.row];
    }else
    {
        item = [datas objectAtIndex:indexPath.row];
    }
    
    detail.arrayWordItem = [[NSMutableArray alloc] init];
    [detail.arrayWordItem addObject:item];
    [[AppDelegate sharedManager].drawerController.navigationController pushViewController:detail animated:YES];
    

}

#pragma mark UITABLEVIEWDATASOURCE
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 51.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isSearch) {
        return [dataSearch count];
    }
    return [datas count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    static NSString *indentifier=@"GrammerCell";
    GrammerCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    if(cell == nil)
    {
        cell = (GrammerCell*)[[NSBundle mainBundle] loadNibNamed:indentifier owner:self options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.delegate = self;
        [cell.lbPast setTextColor:[UIColor colorFromHexString:COLOR_EV_TEXT]];

    }
    GrammerItem *item = nil;
    if (isSearch) {
        item = (GrammerItem*)[dataSearch objectAtIndex:row];
    }else
    {
        item = (GrammerItem*)[datas objectAtIndex:row];
    }

    [cell setDataItem:item];
    [cell.btnFavorite setTag:indexPath.row];
    
    return cell;
    
}


- (IBAction)segmentChanged:(id)sender {
    NSLog(@"%ld",self.segment.selectedSegmentIndex);
    if (self.segment.selectedSegmentIndex == 0) {
        //all
        [self loadDatabase];
    }else
    {
        //favorite
        __block GrammerVC *this = self;
        [dao getGrammarFavoriteFinishBlock:^(id result) {
            [datas removeAllObjects];
            [datas addObjectsFromArray:result];
            [this.tableView reloadData];
        }];
        
    }
}
#pragma mark Action
- (IBAction)clearAllFavorite:(id)sender {
    __block GrammerVC *this = self;
    [AlertViewUniversal showAlertInViewController:self withTitle:APP_NAME message:MESSAGE_CONFIRM_DELETE cancelButtonTitle:BUTTON_CANCLE destructiveButtonTitle:nil otherButtonTitles:[NSArray arrayWithObjects:BUTTON_OK, nil] tapBlock:^(AlertViewUniversal * _Nonnull alert, NSInteger buttonIndex) {
        if (buttonIndex >= alert.firstOtherButtonIndex) {
            [dao removeAllFavoriteInTable:Table_Grammer AndfinishBlock:^(id result) {
                [this loadDatabase];
            }];
        }
    }];
}

#pragma mark - Favorite Cell Delegate
- (void)oxfordCell:(GrammerCell *)verbCell andButton:(id)sender
{
    __block GrammerVC *this = self;
    GrammerItem *item = nil;
    if (isSearch) {
        item = [dataSearch objectAtIndex:[sender tag]];
    }else
    {
        item = [datas objectAtIndex:[sender tag]];
    }
    item.favorite = [NSNumber numberWithBool:!item.favorite.boolValue];
    [dao setFavoriteForWordItem:item._id isFavorite:item.favorite.boolValue andtable:Table_Grammer andfinishBlock:^(id result) {
       [this.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:[sender tag] inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    }];
}

@end
