//
//  DetailVC.m
//  VPMTFrameExample
//
//  Created by Duy on 10/13/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import "DetailGrammer.h"
#import "SearchResultV.h"
#import "WordDao.h"
#import "GrammerItem.h"
#import "VerbItem.h"
#define TAG_FAVORITE 11
#define TAG_BACK 10
@interface DetailGrammer ()<AVSpeechSynthesizerDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (weak, nonatomic) IBOutlet UILabel *lbpronunciace;
@property (weak, nonatomic) IBOutlet UILabel *lbnewword;

@property (weak, nonatomic) IBOutlet UIView *viewTop;


@property (strong,nonatomic) WordDao *workDao;
@property (strong,nonatomic) GrammerItem *worditem;


@end

@implementation DetailGrammer

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self createHeaderBarWithTitle:TITLE_DETAIL andLeftButton:@"i_back_white" andRightButton:nil];
    
    // Do any additional setup after loading the view from its nib.
    [self.webView setScalesPageToFit:YES];
    [self.webView.scrollView setScrollEnabled:true];
    
    self.workDao = [[WordDao alloc] init];
    [self.webView setScalesPageToFit:YES];
    [self.webView.scrollView setScrollEnabled:true];
    
    if ([DicUltils isShowAds] && [[Network sharedManager] isNetworkAvaiable]) {
        STABannerView *startAppBanner_auto = [[STABannerView alloc] initWithSize:STA_AutoAdSize
                                                                      autoOrigin:STAAdOrigin_Bottom
                                                                        withView:self.webView withDelegate:nil];
        [self.webView addSubview:startAppBanner_auto];
        
        
        self.interstitial =
        [[GADInterstitial alloc] initWithAdUnitID:GDA_ADD_FULLSCREEN];
        GADRequest *request = [GADRequest request];
        if ( [DicUltils canShowFullAds]) {
                [self.interstitial loadRequest:request];
        }

    }
    
    
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    
}


- (void)setDataTolayout
{

    if (self.worditem == nil) {
        return;
    }
    
    if (self.worditem.favorite.boolValue) {
        [self.btnFavorite setImage:[UIImage imageNamed:@"i_favorites_active"] forState:UIControlStateNormal];
    }else
    {
        [self.btnFavorite setImage:[UIImage imageNamed:@"ic_favorite"] forState:UIControlStateNormal];
    }
    
    self.lbnewword.text = self.worditem.fromStr;
    self.lbpronunciace.text = [NSString stringWithFormat:@"JLPT N%d",self.worditem.level.intValue];
    [self.lbnewword setTextColor:[UIColor colorFromHexString:COLOR_EV_TEXT]];
    
    NSString *stringHtml = [DicUltils parseMeanGrammertoHtml:self.worditem];
    
    [self.webView loadHTMLString:stringHtml baseURL:nil];
   
}

-(void)viewWillAppear:(BOOL)animated
{
    self.worditem = (GrammerItem*)[self.arrayWordItem objectAtIndex:0];
    [self setDataTolayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)pressLeftButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];

}

#pragma mark - Action
- (IBAction)clickFavo:(id)sender {
    GrammerItem *item =self.worditem;
    __block DetailGrammer *this = self;
    item.favorite = [NSNumber numberWithBool:!item.favorite.boolValue];
    [self.workDao setFavoriteForWordItem:item._id isFavorite:item.favorite.boolValue andtable:Table_Grammer andfinishBlock:^(id result) {
        
        if (item.favorite.boolValue) {
            [this.btnFavorite setImage:[UIImage imageNamed:@"i_favorites_active"] forState:UIControlStateNormal];
        }else
        {
            [this.btnFavorite setImage:[UIImage imageNamed:@"ic_favorite"] forState:UIControlStateNormal];
        }
    }];
}



@end
