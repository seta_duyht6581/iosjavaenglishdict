//
//  DetailVC.m
//  VPMTFrameExample
//
//  Created by Duy on 10/13/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import "DetailKanji.h"
#import "SearchResultV.h"
#import "WordDao.h"
#import "WordItem.h"
#import "VerbItem.h"
#define TAG_FAVORITE 11
#define TAG_BACK 10

@interface DetailKanji ()<AVSpeechSynthesizerDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (weak, nonatomic) IBOutlet UILabel *lbpronunciace;
@property (weak, nonatomic) IBOutlet UILabel *lbnewword;

@property (weak, nonatomic) IBOutlet UIView *viewTop;


@property (strong,nonatomic) WordDao *workDao;
@property (strong,nonatomic) VerbItem *worditem;


@end

@implementation DetailKanji

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self createHeaderBarWithTitle:TITLE_DETAIL andLeftButton:@"i_back_white" andRightButton:nil];
    
    // Do any additional setup after loading the view from its nib.
    [self.webView setScalesPageToFit:YES];
    [self.webView.scrollView setScrollEnabled:true];
    
    self.workDao = [[WordDao alloc] init];
    [self.webView setScalesPageToFit:YES];
    [self.webView.scrollView setScrollEnabled:true];
    
    if ([DicUltils isShowAds] && [[Network sharedManager] isNetworkAvaiable]) {
        STABannerView *startAppBanner_auto = [[STABannerView alloc] initWithSize:STA_AutoAdSize
                                                                      autoOrigin:STAAdOrigin_Bottom
                                                                        withView:self.webView withDelegate:nil];
        [self.webView addSubview:startAppBanner_auto];
        
        self.interstitial =
        [[GADInterstitial alloc] initWithAdUnitID:GDA_ADD_FULLSCREEN];
        if ([DicUltils canShowFullAds]) {
            GADRequest *request = [GADRequest request];
            [self.interstitial loadRequest:request];
        }

    }
    
}

- (void)viewDidDisappear:(BOOL)animated
{
  
}

- (void)setDataTolayout
{
    if (self.worditem == nil) {
        return;
    }
    
    if (self.worditem.favorite.boolValue) {
        [self.btnFavorite setImage:[UIImage imageNamed:@"i_favorites_active"] forState:UIControlStateNormal];
    }else
    {
        [self.btnFavorite setImage:[UIImage imageNamed:@"ic_favorite"] forState:UIControlStateNormal];
    }
    
    self.lbnewword.text = self.worditem.Nhat;
    self.lbpronunciace.text = self.worditem.OnYomi;
    [self.lbnewword setTextColor:[UIColor colorFromHexString:COLOR_EV_TEXT]];
    
    NSString *stringHtml = [DicUltils parseKanjiWordtoHtml:self.worditem];
    
    //Load from contentsource
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"/demo/basic" ofType:@"html" inDirectory:@"ngon"]];
    [self.webView loadHTMLString:stringHtml baseURL:url];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.worditem = (VerbItem*)[self.arrayWordItem objectAtIndex:0];
    [self setDataTolayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)pressLeftButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Action
- (IBAction)clickFavo:(id)sender {
    VerbItem *item =self.worditem;
    __block DetailKanji *this = self;
    item.favorite = [NSNumber numberWithBool:!item.favorite.boolValue];
    [self.workDao setFavoriteForWordItem:item._id isFavorite:item.favorite.boolValue andtable:Table_KANJI andfinishBlock:^(id result) {
        if (item.favorite.boolValue) {
            [this.btnFavorite setImage:[UIImage imageNamed:@"i_favorites_active"] forState:UIControlStateNormal];
        }else
        {
            [this.btnFavorite setImage:[UIImage imageNamed:@"ic_favorite"] forState:UIControlStateNormal];
        }
    }];
    
}


/**
 *  click back webview
 *
 *  @param sender
 */
- (IBAction)clickBack:(id)sender {
    [self.arrayWordItem removeLastObject];
    self.worditem  = (VerbItem*)[self.arrayWordItem lastObject];
    [self  setDataTolayout];
}

- (IBAction)clickSound:(id)sender {
    [[TextToSpeech sharedManager] synthesizer].delegate = self;
    [[TextToSpeech sharedManager] speakText:self.worditem.Nhat andLangue:Japanese_Japan];
}

- (void)lookUpProcessWithResult:(id)result
{
    [super lookUpProcessWithResult:result];
    VerbItem *item = (VerbItem*)[result objectAtIndex:0];
    [self.arrayWordItem addObject:item];
    self.worditem  = item;
    [self  setDataTolayout];
}


@end
