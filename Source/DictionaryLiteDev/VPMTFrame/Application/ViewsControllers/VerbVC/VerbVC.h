//
//  RecentVC.h
//  VPMTFrameExample
//
//  Created by Duy on 10/12/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseSearchVC.h"
@import GoogleMobileAds;
@interface VerbVC : BaseSearchVC<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UISegmentedControl *segment;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)segmentChanged:(id)sender;
- (IBAction)clearAllFavorite:(id)sender;
@property(nonatomic, weak) IBOutlet GADBannerView *bannerView;
@end
