//
//  DetailVC.h
//  VPMTFrameExample
//
//  Created by Duy on 10/13/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseSearchVC.h"

@import GoogleMobileAds;
@interface DetailKanji : BaseSearchVC
@property (nonatomic, strong) NSMutableArray *arrayWordItem;
@property (weak, nonatomic) IBOutlet UIButton *btnFavorite;


#pragma mark Define
@property (nonatomic,strong) NSNumber *idOfWord;



@property (nonatomic,strong) GADInterstitial *interstitial;
@end
