//
//  MyCell.m
//  VPMTFrameExample
//
//  Created by Duy Hoang on 9/18/14.
//  Copyright (c) 2014 CHUONGPD2. All rights reserved.
//

#import "VerbCell.h"

@implementation VerbCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)touchFavorite:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(verbCell:andButton:)])
    {
        [self.delegate verbCell:self andButton:sender];
    }
}

-(void)setData:(VerbItem*)data1
{
    self.lbPresenter.text = data1.Nhat;
    self.lbPast.text = data1.OnYomi;
    NSArray *content = [data1.Content componentsSeparatedByString:@"_"];
    if (content == nil) {
        return;
    }
    if (content.count>1) {
        self.lbContent.text = [NSString stringWithFormat:@"%@, %@",[content objectAtIndex:0],[content objectAtIndex:1]];
    }else
    {
        self.lbContent.text = data1.Content;
    }
    

    
}

@end
