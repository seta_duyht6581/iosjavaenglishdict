//
//  RecentVC.m
//  VPMTFrameExample
//
//  Created by Duy on 10/12/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import "VerbVC.h"
#import "DetailKanji.h"
#import "WordDao.h"
#import "WordItem.h"
#import "WordSearchCell.h"
#import "VerbCell.h"
#import "VerbItem.h"

@interface VerbVC ()<UIAlertViewDelegate,UIScrollViewDelegate,VerbCellDelegate>
{
    
    NSMutableArray *datas;
    WordDao *dao;
    NSInteger selectedIndex;
}
@end



@implementation VerbVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self createHeaderBarWithSearchController];
    [self.searchBar.searchField  setPlaceholder:PLACEHOLDER_KANJI];
    [self.segment setTitle:SEG_ALL forSegmentAtIndex:0];
    [self.segment setTitle:SEG_FAVORITE forSegmentAtIndex:1];
    
    
    //init layout
    self.searchBar.searchField.delegate = self;
    datas = [[NSMutableArray alloc] init];
    dao = [[WordDao alloc] init];
    [dao getKanjiDefaultFinishBlock:^(id result) {
        [datas addObjectsFromArray:result];
        [self.tableView reloadData];
    }];
    //Google ads
    self.bannerView.adUnitID = GDA_ADDUNIT_ID_BANNER;
    self.bannerView.rootViewController = self;

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidAppear:(BOOL)animated
{
    if ([DicUltils isShowAds] && [[Network sharedManager] isNetworkAvaiable]) {
        GADRequest *request = [GADRequest request];
        [self.bannerView loadRequest:request];
    }
    if ([datas count]>0) {
        if (self.segment.selectedSegmentIndex == 0) {
            
            [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:selectedIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        }else
        {
            [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:selectedIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        }
    }

}
#pragma mark UITextFiledDelegate

#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{

    return YES;
}
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    
   [dao getKanjiDefaultFinishBlock:^(id result) {
       NSArray *data = (NSArray*)result;
       if (data!=nil) {
           [datas removeAllObjects];
           [datas addObjectsFromArray:data];
           [self.tableView reloadData];
           [self.tableView setContentOffset:CGPointMake(0, 0) animated: YES];
       }
   }];
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSString *keywork = [[textField text] stringByReplacingCharactersInRange:range withString:string];
    if (keywork == nil || keywork.length == 0) {
        [dao searchKanjiKey:keywork andFinishBlock:^(id result) {
            NSArray *data = (NSArray*)result;
            if (data!=nil) {
                [datas removeAllObjects];
                [datas addObjectsFromArray:data];
                [self.tableView reloadData];
                [self.tableView setContentOffset:CGPointMake(0, 0) animated: YES];
            }
        }];
        return YES;
    }
    
    //Search by keyword
    [dao searchKanjiKey:keywork andFinishBlock:^(id result) {
            NSArray *data = (NSArray*)result;
            if (data!=nil) {
                [datas removeAllObjects];
                [datas addObjectsFromArray:data];
                [self.tableView reloadData];
                [self.tableView setContentOffset:CGPointMake(0, 0) animated: YES];
            }
    }];

    
    return YES;
}



#pragma mark SCrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.searchBar.searchField resignFirstResponder];
}


#pragma mark UITABLEVIEWDELEGATE
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    selectedIndex = indexPath.row;
    VerbItem *item =(VerbItem*) [datas objectAtIndex:indexPath.row];
    DetailKanji *detail = [[DetailKanji alloc] init];
    detail.arrayWordItem = [[NSMutableArray alloc] init];
    [detail.arrayWordItem addObject:item];
    [[AppDelegate sharedManager].drawerController.navigationController pushViewController:detail animated:YES];

}

#pragma mark UITABLEVIEWDATASOURCE
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 58.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [datas count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    static NSString *indentifier=@"VerbCell";
    VerbCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    if(cell == nil)
    {
        cell = (VerbCell*)[[NSBundle mainBundle] loadNibNamed:indentifier owner:self options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.lbPast.text = @"";
        cell.delegate = self;
        cell.lbPastPraticiple.text = @"";
        cell.lbPresenter.text = @"";
        [cell.lbPresenter setTextColor:[UIColor colorFromHexString:COLOR_EV_TEXT]];
        //[cell.lbPast setTextColor:[UIColor colorFromHexString:COLOR_EV_TEXT]];
    }
    VerbItem *item;
    item = (VerbItem*)[datas objectAtIndex:row];
    [cell setData:item];
    [cell.buttonFavorite setTag:row];
    if (item.favorite.boolValue) {
        [cell.buttonFavorite setImage:[UIImage imageNamed:@"i_favorites_active"] forState:UIControlStateNormal];
    }else
    {
        [cell.buttonFavorite setImage:[UIImage imageNamed:@"ic_favorite"] forState:UIControlStateNormal];
    }

    return cell;
    
}

- (IBAction)segmentChanged:(id)sender {
    NSLog(@"%ld",self.segment.selectedSegmentIndex);
    if (self.segment.selectedSegmentIndex == 0) {
        //all
        __block VerbVC *this = self;
        [dao getKanjiDefaultFinishBlock:^(id result) {
            [datas removeAllObjects];
            [datas addObjectsFromArray:result];
            [this.tableView reloadData];;
        }];
        
    }else
    {
        //favorite
        __block VerbVC *this = self;
        [dao getKanjiFavoriteFinishBlock:^(id result) {
            [datas removeAllObjects];
            [datas addObjectsFromArray:result];
            [this.tableView reloadData];
        }];
        
    }
}

- (IBAction)clearAllFavorite:(id)sender {
    __block VerbVC *this = self;
    [AlertViewUniversal showAlertInViewController:self withTitle:APP_NAME message:MESSAGE_CONFIRM_DELETE cancelButtonTitle:BUTTON_CANCLE destructiveButtonTitle:nil otherButtonTitles:[NSArray arrayWithObjects:BUTTON_OK, nil] tapBlock:^(AlertViewUniversal * _Nonnull alert, NSInteger buttonIndex) {
        if (buttonIndex >= alert.firstOtherButtonIndex) {
            [dao removeAllFavoriteInTable:Table_KANJI AndfinishBlock:^(id result) {
                 [dao getKanjiDefaultFinishBlock:^(id result) {
                     [datas removeAllObjects];
                     [datas addObjectsFromArray:result];
                     [this.tableView reloadData];
                 }];
            }];
        }
    }];
}

#pragma mark - Favorite Cell Delegate
- (void)verbCell:(VerbCell *)verbCell andButton:(id)sender
{
    __block VerbVC *this = self;
    VerbItem *item = [datas objectAtIndex:[sender tag]];
    item.favorite = [NSNumber numberWithBool:!item.favorite.boolValue];
    [dao setFavoriteForWordItem:item._id isFavorite:item.favorite.boolValue andtable:Table_KANJI andfinishBlock:^(id result)
    {
        [this.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:[sender tag] inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    }];
}

#pragma mark Clipboard Delegate
-(void)clipboardAndKeyword:(NSString*)keyword
{
    [super clipboardAndKeyword:keyword];
    self.segment.selectedSegmentIndex = 0;
    self.searchBar.searchField.text = keyword;
    [dao searchKanjiKey:keyword andFinishBlock:^(id result) {
        NSArray *data = (NSArray*)result;
        if (data!=nil) {
            [datas removeAllObjects];
            [datas addObjectsFromArray:data];
            [self.tableView reloadData];
            [self.tableView setContentOffset:CGPointMake(0, 0) animated: YES];
        }
    }];
    
}
@end
