//
//  MyCell.h
//  VPMTFrameExample
//
//  Created by Duy Hoang on 9/18/14.
//  Copyright (c) 2014 CHUONGPD2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VerbItem.h"
@class VerbCell;
@protocol VerbCellDelegate <NSObject>

-(void)verbCell:(VerbCell*)verbCell andButton:(id)sender;
@end
@interface VerbCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbPresenter;
@property (weak, nonatomic) IBOutlet UILabel *lbPast;
@property (weak, nonatomic) IBOutlet UILabel *lbContent;
@property (weak, nonatomic) IBOutlet UILabel *lbPastPraticiple;
@property (weak, nonatomic) id<VerbCellDelegate> delegate;
- (IBAction)touchFavorite:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *buttonFavorite;
-(void)setData:(VerbItem*)data1;
@end
