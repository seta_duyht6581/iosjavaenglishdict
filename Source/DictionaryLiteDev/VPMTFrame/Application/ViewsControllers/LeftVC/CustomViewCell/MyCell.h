//
//  MyCell.h
//  VPMTFrameExample
//
//  Created by Duy Hoang on 9/18/14.
//  Copyright (c) 2014 CHUONGPD2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIView *separator;

@end
