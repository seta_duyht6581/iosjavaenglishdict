//
//  AboutVC.h
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/11/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseSearchVC.h"

@interface AboutVC : BaseSearchVC
@property (nonatomic,strong) GADInterstitial *interstitial;
@end
