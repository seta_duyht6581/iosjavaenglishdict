//
//  AboutVC.m
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/11/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import "AboutVC.h"

@interface AboutVC ()<GADInterstitialDelegate>

@end

@implementation AboutVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    // Do any additional setup after loading the view from its nib.
    [self createHeaderBarWithTitle:TITLE_ABOUT andLeftButton:@"i_back_white" andRightButton:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)pressLeftButton:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
