// Copyright (c) 2013 Mutual Mobile (http://mutualmobile.com/)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


#import "LeftSideVC.h"
#import "MMSideDrawerSectionHeaderView.h"
#import "SearchVC.h"
#import "VerbVC.h"
#import "AboutVC.h"
#import <MessageUI/MessageUI.h>
#import "RemoveAds.h"

@interface LeftSideVC()<MFMailComposeViewControllerDelegate>
{
    MFMailComposeViewController * mcvc;
    int selectIndex ;
}
@end

@implementation LeftSideVC

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self createHeaderBarNavi:APP_NAME];
    CGRect frame = self.hearderView.frame;
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height - frame.size.height) style:UITableViewStyleGrouped];
    self.tableView.showsVerticalScrollIndicator = NO;
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    [self.view addSubview:self.tableView];
   
    [self.tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    
    UIColor * tableViewBackgroundColor;
    tableViewBackgroundColor = [UIColor colorWithRed:110.0/255.0
                                               green:113.0/255.0
                                                blue:115.0/255.0
                                               alpha:1.0];
    //[self.tableView setBackgroundColor:tableViewBackgroundColor];
    [self.tableView setBackgroundColor:[UIColor colorFromHexString:@"#d6d6d6"]];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    self.datas = @[TITLE_RATEAPP,
              TITLE_SHARE,TITLE_FEEDBACK,TITLE_ABOUT,OUR_APP];
    self.navigationController.navigationBarHidden = YES;
    //[self setTitle:APP_NAME];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    SearchVC  *search =  (SearchVC*)[[AppDelegate sharedManager].homeController.viewControllers objectAtIndex:0];
    if (search!=nil) {
        [search.searchBar.searchField resignFirstResponder];
    }
    if ( [AppDelegate sharedManager].verbVC !=nil)
    {
        [[AppDelegate sharedManager].verbVC.searchBar.searchField resignFirstResponder];
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)contentSizeDidChange:(NSString *)size{
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    switch (section) {
        case HeaderMain:
            return 3;//Other
        case HeaderExtra:
            return 5;//Our app
        case HeaderAds:
            return 0;//Ads
        default:
            return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
        
    }
    [cell.textLabel setFont:[UIFont systemFontOfSize:15.0f]];
    [cell.textLabel setTextColor:[UIColor grayColor]];
    switch (indexPath.section) {
        case HeaderMain:
            if(indexPath.row == 0){
                [cell.textLabel setText:TITLE_DICTIONARY_EV];
            }else if (indexPath.row == 1)
            {
                [cell.textLabel setText:TITLE_DICTIONARY_VE];
            } else if (indexPath.row == 2)
            {
                [cell.textLabel setText:VERB];
            }else if (indexPath.row == 3)
            {
                [cell.textLabel setText:OTHER];
            }
            
            if (selectIndex == indexPath.row) {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            }else
            {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
            break;
        case HeaderExtra:{
             [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            cell.textLabel.text = [self.datas objectAtIndex:indexPath.row];
            break;
        }
        case HeaderAds:
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            cell.textLabel.text = REMOVEADS;
            break;
        default:
            break;
    }
    
    return cell;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    switch (section) {
        case HeaderMain:
            return MAINFUNTION;
        case HeaderExtra:
            return EXTRAFUNTION;
        case HeaderAds:
            return ADSFUNCTION;
        default:
            return nil;
    }
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    MMSideDrawerSectionHeaderView * headerView;
    headerView =  [[MMSideDrawerSectionHeaderView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.bounds), 40.0)];
    [headerView setAutoresizingMask:UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth];
    [headerView setTitle:[tableView.dataSource tableView:tableView titleForHeaderInSection:section]];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self checkReviewPopup];
    switch (indexPath.section) {
        case HeaderMain:{
            selectIndex = (int)indexPath.row;
            switch (indexPath.row) {
                case 0:
                {
                    [AppDelegate sharedManager].TYPE_TABLE_CURRENT = TYPE_EV;
                    [AppDelegate sharedManager].curTable = Table_English;
                    [AppDelegate sharedManager].curIndex = INDEXED_EV;
                    SearchVC *searchVC = [[AppDelegate sharedManager].homeController.viewControllers  objectAtIndex:0];
                    [searchVC  loadDatabase];
                    
                    [searchVC loadAds];
                    searchVC.searchBar.searchField.text = @"";
                    [self.mm_drawerController setCenterViewController:[AppDelegate sharedManager].homeController];
                    [[AppDelegate sharedManager].homeController setSelectedIndex:0];
                    break;
                }
                case 1:
                {
                    [AppDelegate sharedManager].TYPE_TABLE_CURRENT = TYPE_VE;
                    [AppDelegate sharedManager].curTable = Table_VE;
                    [AppDelegate sharedManager].curIndex = INDEXED_VE;
                    
                    SearchVC *searchVC = [[AppDelegate sharedManager].homeController.viewControllers  objectAtIndex:0];
                    [searchVC  loadDatabase];
                    [searchVC loadAds];
                    searchVC.searchBar.searchField.text = @"";
                    [self.mm_drawerController setCenterViewController:[AppDelegate sharedManager].homeController];
                    [[AppDelegate sharedManager].homeController setSelectedIndex:0];
                    break;
                }
                case 2:
                    if ([AppDelegate sharedManager].verbVC == nil) {
                        [AppDelegate sharedManager].verbVC = [[VerbVC alloc] init];
                    }
                    [AppDelegate sharedManager].TYPE_TABLE_CURRENT = TYPE_KANJI;
                    [AppDelegate sharedManager].curTable = Table_KANJI;
                    [AppDelegate sharedManager].curIndex = INDEXED_EV;
                    [self.mm_drawerController setCenterViewController:[AppDelegate sharedManager].verbVC];
                    
                    
                    break;
                case 3:
                    if ([AppDelegate sharedManager].oxfordVC == nil) {
                        [AppDelegate sharedManager].oxfordVC = [[GrammerVC alloc] init];
                    }
                    [AppDelegate sharedManager].TYPE_TABLE_CURRENT = TYPE_GRAMMAR;
                    [AppDelegate sharedManager].curTable = Table_Grammer;
                    [AppDelegate sharedManager].curIndex = INDEXED_EV;
                    [self.mm_drawerController setCenterViewController:[AppDelegate sharedManager].oxfordVC];
                    break;
                default:
                    break;
            }
            
            break;
        }
            
        case HeaderExtra:
            switch (indexPath.row) {
                case 0:
                     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:ITUN_MYAPP]];
                    break;
                case 1:
                    [self shareText];
                    break;
                case 2:
                {
                    
                    
                    if ([MFMailComposeViewController canSendMail])
                    {
                        mcvc = [[MFMailComposeViewController alloc] init];
                        mcvc.mailComposeDelegate = self;
                        NSString *toAddress = MY_EMAIL;
                        [mcvc setToRecipients:[NSArray arrayWithObjects:toAddress,nil]];
                        [mcvc setSubject:SUBJECT_EMAIL];
                        [self presentViewController:mcvc animated:YES completion:nil];
                        
                    }
                    else
                    {
                    
                        [AlertViewUniversal showAlertInViewController:self withTitle:APP_NAME message:MESSAGE_NOT_SETEMAIL cancelButtonTitle:BUTTON_CANCLE destructiveButtonTitle:nil otherButtonTitles:[NSArray arrayWithObjects:BUTTON_OK, nil] tapBlock:^(AlertViewUniversal * _Nonnull alert, NSInteger buttonIndex) {
                         
                        }];
                        
                    }
                    
                    
                    
                }

                    break;
                case 3:
                {
                    
                    AboutVC *about = [[AboutVC alloc] init];
                    [self presentViewController:about animated:YES completion:nil];
                }
                    break;
                    
                case 4:
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/developer/id1056900139"]];
                default:
                    break;
            }
            break;
            
            case HeaderAds:
                {
                    RemoveAds *remove = [[RemoveAds alloc] init];
                    [self presentViewController:remove animated:YES completion:nil];
                }
        default:
            break;
    }
    //[tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
    [self.tableView reloadData];
}
- (void)shareText
{
    NSString *textToShare = MESSAGE_SHARE_APP;
    NSURL *myWebsite = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",LINK_APP,ITUN_APP_ID]];
    
    NSArray *objectsToShare = @[textToShare, myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}
#pragma mark MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    switch(result) {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
            break;
        case MFMailComposeResultFailed:
            break;
        default:
            break;
    }
    [mcvc dismissViewControllerAnimated:YES completion:nil];
}

- (void)checkReviewPopup
{
       NSString*key = [UserDefault getObjectWithKey:KEY_REVIEW];
    if (key == nil) {
        [DicUltils showReviewPopUpInViewController:self];
        return;
    }
    if ( [key isEqualToString:REVIEW_CANCEL] || [key isEqualToString:REVIEWED]) {
        return;
    }
    if ([key isEqualToString:REVIEW_LATER]) {
        NSNumber *number = [UserDefault getObjectWithKey:KEY_REVIEW_LATER];
        if (number == nil) {
            [UserDefault addObjectForKey:[NSNumber numberWithInt:0] key:KEY_REVIEW_LATER];
            return;
        }
        if ( (number.intValue % 2) == 0) {
            [DicUltils showReviewPopUpInViewController:self];
        }else
        {
            number = [NSNumber numberWithInt:number.intValue+1];
            [UserDefault addObjectForKey:number key:KEY_REVIEW_LATER];
        }
    }
}
@end
