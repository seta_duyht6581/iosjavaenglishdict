//
//  Api.h
//
//  Created by viettel on 7/10/14.
//  Copyright (c) 2014 ChuongPD. All rights reserved.
//

#import "DbConfig.h"

@implementation DbConfig

static DbConfig* sharedManager = nil;

+ (instancetype)sharedManager {
    @synchronized(self)
    {
        if (sharedManager && [sharedManager isKindOfClass:[self class]]){
            return sharedManager;
        }else{
            sharedManager = [[self alloc] init];
            
            return sharedManager;
        }
    }
}


@end
