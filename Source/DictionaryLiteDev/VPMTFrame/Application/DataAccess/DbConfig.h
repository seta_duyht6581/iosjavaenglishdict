//
//  ApiHelper.h
//
//  Created by viettel on 7/10/14.
//  Copyright (c) 2014 ChuongPD. All rights reserved.
//


#define CL_FAVORITE @"favorite"
#define CL_ID @"_id"
#define CL_POS @"pos"

#define CL_WORD @"word_unsign"
#define CL_WORD_SIGN @"word"
#define CL_MEAN @"content"
#define CL_TYPE @"type"

#define CL_GRAMMAR_KEY @"key"
#define CL_GRAMMAR_MEAN @"mean"
#define Table_Verb @"verb"
#define Table_Grammer @"bumpou"

#define INDEXED_EV @"id_ev"
#define INDEXED_VE @"id_ve"


#define CL_FIRST_NHAT @"_first_nhat"
#define CL_FIST_ONYO @"_first_onyo"


#define CL_IMG @"img"
#define CL_PARTS @"parts"
#define CL_EXAMPLES @"examples"

#define CL_LEVEL @"level"
#define CL_HAN @"Han"
#define CL_NHAT @"Nhat"
#define CL_ONYOMI @"OnYomi"
#define CL_KUNYOMI @"kunYomi"

#define CL_NHAT_UNSIGN @"nhat_unsign"
#define CL_ONYOMI_UNSIGN @"onyomi_unsign"

#define Table_English @"ja_en"
#define Table_VE @"en_ja"
#define Table_KANJI @"kanji2"
#define INDEXED_ID_JAVI @"idx_javi"
#define INDEXED_ID_VIJA @"idx_vija"
#define INDEXED_UNSIGN_JAVI @"idjaen_first"
#define INDEXED_UNSIGN_VIJA @"idenja_first"
#define INDEXED_KANJI_UNSIGN @"idkanji"
@interface DbConfig: NSObject


@end
