//
//  BaseDao.m
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/16/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//


#import "BaseDao.h"
#define DB_TYPE_INTEGER @"INTEGER"
#define DB_TYPE_TEXT @"TEXT"
#define DB_TYPE_VARCHAR @"VARCHAR"
#define DB_TYPE_FLOAT @"FLOAT"
#define DB_TYPE_DOUBLE @"DOUBLE"
@implementation BaseDao
#pragma mark - GetType

-(id)init
{
    if (self = [super init])
    {
        
        self.operationQueue = [[NSOperationQueue alloc] init];
        [self.operationQueue setMaxConcurrentOperationCount:1];
        
    }
    return self;
}

-(id) getStringWithStmt:(sqlite3_stmt*)stmt andIndex:(int)index
 {
 
     const unsigned char* data =sqlite3_column_text(stmt, index);
     if(data != NULL)
         return [NSString stringWithUTF8String:(const char *)data];
     else
         return [NSString stringWithFormat:@""];
 
 }
 -(id)getIntWithStmt:(sqlite3_stmt*)stmt andIndex:(int)index
 {
     const unsigned char* data = sqlite3_column_text(stmt, index);
     if(data != NULL)
         return [NSNumber numberWithInt:sqlite3_column_int(stmt, index)];
     else
         return [NSNull null];
 }

- (void)bindData:(id)data
        andIndex:(NSInteger)index
    andStatement:(sqlite3_stmt*)stmt
{
    // check if the data is of the type nsnumber of the string type
    if([data isKindOfClass:[NSNumber class]])
    {
        // if the data is of the type nsnumber then check if the
        // value of the data is of which primitive type as follows
        // and bind it to the data base as follows.
        if(strcmp([data objCType], @encode(int))==0)
        {
            sqlite3_bind_int(stmt, index, [data integerValue]);
        }
    }
    else if([data isKindOfClass:[NSString class]])
    {
        // if the data is of the string type then just bind the text to
        // the text binding as follows.
        sqlite3_bind_text(stmt, index, [data  UTF8String], -1, SQLITE_TRANSIENT);
    }
}

- (int)getIndexByColumnName:(sqlite3_stmt*)stmt
             withColumnName:(NSString*)columnName
{
    int n = sqlite3_data_count(stmt);
    for (int i=0; i < n; i++) {
       const char *name =  sqlite3_column_name(stmt, i);
        NSString *nameUTF8 = [NSString stringWithUTF8String:name];
        if ([columnName isEqualToString:nameUTF8]) {
            return i;
        }
     }
    return -1;
}
-(NSString*)getTypeOfColumn:(sqlite3_stmt*)stmt
             atColumnIndex:(int)indexColum
{
    const char *str = sqlite3_column_decltype(stmt, indexColum);
    return [[NSString stringWithUTF8String:str] uppercaseString];
}
#pragma mark - Query
/**/

- (void) queryWithStatement:(NSString*)queryString
            andProcessBlock:(ProcessQuery)processquery
             andFinishBlock:(ProcessQueryResult)finish
{
    [self.operationQueue cancelAllOperations];
    _currentOperation = nil;
    _currentOperation = [[MyOperation alloc] init];
    _currentOperation.queryString = queryString;
    _currentOperation.ProcessQuery = processquery;
    _currentOperation.ProcessQueryResult = finish;
    [self.operationQueue addOperation:[[NSInvocationOperation alloc] initWithTarget:self selector:@selector(queryWithStatementExcecute) object:nil]];
    

}

- (void) queryNoOperationWithStatement:(NSString*)queryString
            andProcessBlock:(ProcessQuery)processquery
             andFinishBlock:(ProcessQueryResult)finish
{

    sqlite3_stmt    *get_stmt;
    bool dbAvail = false;
    NSMutableArray *data=[[NSMutableArray alloc] init];
    
    if ([DAO openDB])
    {
        dbAvail = true;
        
        
    }else
    {
        return;
    }
    
    
    if(sqlite3_prepare_v2([DAO OttoDB], [queryString UTF8String],-1, &get_stmt, NULL) == SQLITE_OK)
    {
        int n =0;
        while ((n = sqlite3_step(get_stmt)) == SQLITE_ROW) {
            id item = processquery(get_stmt);
            [data addObject:item];
        }
        
    }
    
    if(sqlite3_finalize(get_stmt) == SQLITE_OK) {
        
        //NSLog(@"finalized the statement successfully");
        
    } else
    {
        NSLog(@"%s ", sqlite3_errmsg([DAO OttoDB]));
    }
    
    if (dbAvail)
    {
        [DAO closeDB];
    }
    //finish
    finish(data);

}
- (void) queryWithStatementExcecute
{
    
   NSString* queryString = _currentOperation.queryString;
   ProcessQuery processquery = _currentOperation.ProcessQuery;
   ProcessQueryResult finish = _currentOperation.ProcessQueryResult;
    
    sqlite3_stmt    *get_stmt;
    bool dbAvail = false;
    NSMutableArray *data=[[NSMutableArray alloc] init];
    
    if ([DAO openDB])
    {
        dbAvail = true;
        
        
    }else
    {
        return;
    }
    
    
    if(sqlite3_prepare_v2([DAO OttoDB], [queryString UTF8String],-1, &get_stmt, NULL) == SQLITE_OK)
    {
        int n =0;
        while ((n = sqlite3_step(get_stmt)) == SQLITE_ROW) {
            id item = processquery(get_stmt);
            [data addObject:item];
        }
        
    }
    
    if(sqlite3_finalize(get_stmt) == SQLITE_OK) {
        
        //NSLog(@"finalized the statement successfully");
        
    } else
    {
        NSLog(@"%s ", sqlite3_errmsg([DAO OttoDB]));
    }
    
    if (dbAvail)
    {
        [DAO closeDB];
    }
    //finish
    finish(data);
    
    
}
/**
 *  @author DuyHt, 15-10-23 11:10:11
 *
 *  <#Description#>
 *
 *  @param dataOfColumns chua du lieu can bind theo thu tu index tu 0 - n
 *  @param queryString   cau query ko dua du lieu vao ma dua dau ?
 *  @param processquery  <#processquery description#>
 *  @param finish        <#finish description#>
 */

- (void) queryWithWithData:(NSArray*)dataOfColumns
            andQueryString:(NSString*)queryString
           andProcessBlock:(ProcessQuery)processquery
            andFinishBlock:(ProcessQueryResult)finish
{
    [self.operationQueue cancelAllOperations];
    _currentOperation = nil;
    _currentOperation = [[MyOperation alloc] init];
    _currentOperation.queryString = queryString;
    _currentOperation.dataOfColumns = dataOfColumns;
    _currentOperation.ProcessQuery = processquery;
    _currentOperation.ProcessQueryResult = finish;
    [self.operationQueue addOperation:[[NSInvocationOperation alloc] initWithTarget:self selector:@selector(queryWithWithDataExecute) object:nil]];
    
    
}
- (void) queryWithWithDataExecute
{

    NSArray* dataOfColumns = _currentOperation.dataOfColumns;
    NSString* queryString = _currentOperation.queryString;
    ProcessQuery processquery = _currentOperation.ProcessQuery;
    ProcessQueryResult finish =  _currentOperation.ProcessQueryResult;
    
    sqlite3_stmt    *get_stmt;
    bool dbAvail = false;
    NSMutableArray *data=[[NSMutableArray alloc] init];
    
    if ([DAO openDB])
    {
        dbAvail = true;
        
        
    }else
    {
        return;
    }
    //Parse string
    //INSERT INTO %@  (id, name,totem, gender, details,updated) values(?,?,?,?,?,?)
    //queryString = @"SELECT _id,l_from,l_to,favorite,l_from_sign FROM vieteng where (l_from like 'ơ%') or l_from_sign like 'ơ%'";
    //create sqlite
    //queryString = @"SELECT _id,l_from,l_to,favorite,l_from_sign FROM vieteng INDEXED BY id_ve where (l_from like 'hôn%' or l_from_sign like 'hôn%') and _first = 'H' limit 100";

    //******

    if(sqlite3_prepare_v2([DAO OttoDB], [queryString UTF8String],-1, &get_stmt, NULL) == SQLITE_OK)
    {
        int n =0;
        //bind data
        for (id item in dataOfColumns) {
            n++;
            [self bindData:item andIndex:n andStatement:get_stmt];
            
        }
        while ( sqlite3_step(get_stmt) == SQLITE_ROW) {
            n++;
            id item = processquery(get_stmt);
            [data addObject:item];
        }
        
    }

    if(sqlite3_finalize(get_stmt) == SQLITE_OK) {
        
        //NSLog(@"finalized the statement successfully");
        
    } else
    {
        NSLog(@"%s ", sqlite3_errmsg([DAO OttoDB]));
    }
   
    if (dbAvail)
    {
        [DAO closeDB];
    }
    //finish
    finish(data);
    
    
}

#pragma mark - Update
/**
 *  Update nhieu column
 *
 *  @param dataOfColumns Array chua cac rows, moi row la 1 Dictionnary
 */
-(void)updateRowsWithData:(NSArray*)dataOfColumns
                fromTable:(NSString*)tableName
       andConditionClause:(NSString*)whereClause
           andFinishBlock:(ProcessQueryResult)finish
{
    [self.operationQueue cancelAllOperations];
    _currentOperation = nil;
    _currentOperation = [[MyOperation alloc] init];
    _currentOperation.tableName = tableName;
    _currentOperation.whereClause = whereClause;
    _currentOperation.dataOfColumns = dataOfColumns;
    _currentOperation.ProcessQueryResult = finish;
    [self.operationQueue addOperation:[[NSInvocationOperation alloc] initWithTarget:self selector:@selector(updateRowsWithDataExecute) object:nil]];
    
    
}
-(void)updateNoOperationRowsWithData:(NSArray*)dataOfColumns
                fromTable:(NSString*)tableName
       andConditionClause:(NSString*)whereClause
           andFinishBlock:(ProcessQueryResult)finish
{

    sqlite3_stmt    *update_stm = nil;
    bool dbAvail = false;
    BOOL isSuccessfull=false;
    if ([DAO openDB])
    {
        dbAvail = true;
        
    }else
    {
        return;
    }
    
    if (dbAvail)
    {
        
        //create sqlite
        int n = [dataOfColumns count];
        NSDictionary *row = [dataOfColumns objectAtIndex:0];
        NSMutableString *sqlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"UPDATE %@ SET ",tableName]];
        
        NSArray *arrayColumnName = [row allKeys];
        //create query
        for(NSString *columName in arrayColumnName)
        {
            [sqlString appendString:[NSString stringWithFormat:@" %@ = ? ,",columName]];
        }
        NSString *stringRemovelastChar =  [sqlString substringToIndex:sqlString.length - 2];
        sqlString = [[NSMutableString alloc] initWithString:stringRemovelastChar];
        if (whereClause!=nil) {
            [sqlString appendString:@" where "];
            [sqlString appendString:whereClause];
        }
        if(sqlite3_prepare_v2([DAO OttoDB], [sqlString UTF8String],-1, &update_stm, NULL) == SQLITE_OK)
        {
            
            for(int i = 0; i < n; i++) {
                NSDictionary *rowitem = [dataOfColumns objectAtIndex:i];
                int j = 1;
                for (NSString *columName in arrayColumnName) {
                    [self bindData:[rowitem objectOrNilForKey:columName] andIndex:j andStatement:update_stm];
                    j++;
                }
                
                
                if (sqlite3_step(update_stm) == SQLITE_DONE) {
                    if (i == (n-1))
                        sqlite3_finalize(update_stm);
                    else
                        sqlite3_reset(update_stm);
                }
                else {
                    NSLog(@"%s ", sqlite3_errmsg([DAO OttoDB]));
                }
                
            }
            
        }
        
        
        
        if (dbAvail)
        {
            [DAO closeDB];
        }
        finish([NSNumber numberWithBool:isSuccessfull]);
    }

}
-(void)updateRowsWithDataExecute{
    
    
    NSArray* dataOfColumns =_currentOperation.dataOfColumns;
    NSString* tableName = _currentOperation.tableName;
    NSString* whereClause = _currentOperation.whereClause;
    ProcessQueryResult finish = _currentOperation.ProcessQueryResult;
    sqlite3_stmt    *update_stm = nil;
    bool dbAvail = false;
    BOOL isSuccessfull=false;
    if ([DAO openDB])
    {
        dbAvail = true;
        
    }else
    {
        return;
    }
    
    if (dbAvail)
    {
        
        //create sqlite
        int n = [dataOfColumns count];
        NSDictionary *row = [dataOfColumns objectAtIndex:0];
        NSMutableString *sqlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"UPDATE %@ SET ",tableName]];
        
        NSArray *arrayColumnName = [row allKeys];
        //create query
        for(NSString *columName in arrayColumnName)
        {
            [sqlString appendString:[NSString stringWithFormat:@" %@ = ? ,",columName]];
        }
        NSString *stringRemovelastChar =  [sqlString substringToIndex:sqlString.length - 2];
        sqlString = [[NSMutableString alloc] initWithString:stringRemovelastChar];
        if (whereClause!=nil) {
            [sqlString appendString:@" where "];
            [sqlString appendString:whereClause];
        }
            if(sqlite3_prepare_v2([DAO OttoDB], [sqlString UTF8String],-1, &update_stm, NULL) == SQLITE_OK)
            {
                
                for(int i = 0; i < n; i++) {
                    NSDictionary *rowitem = [dataOfColumns objectAtIndex:i];
                    int j = 1;
                    for (NSString *columName in arrayColumnName) {
                        [self bindData:[rowitem objectOrNilForKey:columName] andIndex:j andStatement:update_stm];
                        j++;
                    }
                

                    if (sqlite3_step(update_stm) == SQLITE_DONE) {
                        if (i == (n-1))
                           sqlite3_finalize(update_stm);
                        else
                            sqlite3_reset(update_stm);
                    }
                    else {
                        NSLog(@"%s ", sqlite3_errmsg([DAO OttoDB]));
                    }

                }
               
            }


        
        if (dbAvail)
        {
            [DAO closeDB];
        }
        finish([NSNumber numberWithBool:isSuccessfull]);
    }

}

-(void)insertRowsWithData:(NSArray*)dataOfColumns
                fromTable:(NSString*)tableName
       andConditionClause:(NSString*)whereClause
           andFinishBlock:(ProcessQueryResult)finish{
    
    sqlite3_stmt    *update_stm = nil;
    bool dbAvail = false;
    BOOL isSuccessfull=false;
    if ([DAO openDB])
    {
        dbAvail = true;
        
    }else
    {
        return;
    }
    
    if (dbAvail)
    {
        //INSERT INTO %@  (id, name,totem, gender, details,updated) values(?,?,?,?,?,?)
        //create sqlite
        int n = [dataOfColumns count];
        NSDictionary *row = [dataOfColumns objectAtIndex:0];
        NSMutableString *sqlString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"INSERT INTO %@ (",tableName]];
        
        NSArray *arrayColumnName = [row allKeys];
        NSMutableString *valuesString = [[NSMutableString alloc] init];
        [valuesString appendString:@" values("];
        //create query
        for(NSString *columName in arrayColumnName)
        {
            [sqlString appendString:[NSString stringWithFormat:@" %@,",columName]];
            [valuesString appendString:@"?,"];
        }
        NSString *stringRemovelastChar =  [sqlString substringToIndex:sqlString.length - 1];
        sqlString = [[NSMutableString alloc] initWithString:stringRemovelastChar];
        [sqlString appendString:@") "];
        
        NSString *removelastChar = [valuesString substringToIndex:valuesString.length - 1];
        valuesString = [[NSMutableString alloc] initWithString:removelastChar];
        [valuesString appendString:@") "];
        
        [sqlString appendString:valuesString];
        
        if (whereClause!=nil) {
            [sqlString appendString:@" where "];
            [sqlString appendString:whereClause];
        }
        
        if(sqlite3_prepare_v2([DAO OttoDB], [sqlString UTF8String],-1, &update_stm, NULL) == SQLITE_OK)
        {
            
            for(int i = 0; i < n; i++) {
                NSDictionary *rowitem = [dataOfColumns objectAtIndex:i];
                int j = 1;
                for (NSString *columName in arrayColumnName) {
                    [self bindData:[rowitem objectOrNilForKey:columName] andIndex:j andStatement:update_stm];
                    j++;
                }
                
                
                if (sqlite3_step(update_stm) == SQLITE_DONE) {
                    if (i == (n-1))
                        sqlite3_finalize(update_stm);
                    else
                        sqlite3_reset(update_stm);
                }
                else {
                    NSLog(@"%s ", sqlite3_errmsg([DAO OttoDB]));
                }
                
            }
            
        }
        
        
        
        if (dbAvail)
        {
            [DAO closeDB];
        }
        finish([NSNumber numberWithBool:isSuccessfull]);
    }
    
}

- (NSDictionary*)getInforOfTable:(NSString*)tableName
{
    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    sqlite3_stmt    *get_stmt;
    bool dbAvail = false;
    
    if ([DAO openDB])
    {
        dbAvail = true;
    }else
    {
        return NULL;
    }
    NSString *queryString = [NSString stringWithFormat:@"select * from %@ where _id = 44",tableName];
    
    if(sqlite3_prepare_v2([DAO OttoDB], [queryString UTF8String],-1, &get_stmt, NULL) == SQLITE_OK)
    {
        while (sqlite3_step(get_stmt) == SQLITE_ROW) {
            int n1  = sqlite3_data_count(get_stmt);
            for (int i=0; i<n1 ; i++) {
                const char *name =  sqlite3_column_name(get_stmt, i);
                NSString *nameCol = [NSString stringWithUTF8String:name];
                NSString *type = [self getTypeOfColumn:get_stmt atColumnIndex:i];
                [result setValue:type forKey:nameCol];
                
            }
            
        }
        
    }
    

    if(sqlite3_finalize(get_stmt) == SQLITE_OK) {
        
        //NSLog(@"finalized the statement successfully");
        
    } else
    {
        NSLog(@"%s ", sqlite3_errmsg([DAO OttoDB]));
    }
    if (dbAvail)
    {
        [DAO closeDB];
    }
    //finish
    

    return result;
}

- (void) deleteWithTable:(NSString*)table
          andWhereClause:(NSString*)whereClause
            andProcessBlock:(ProcessQuery)processquery
             andFinishBlock:(ProcessQueryResult)finish
{
    sqlite3_stmt    *insert_stmt;
    bool dbAvail = false;
    if ([DAO openDB])
    {
        dbAvail = true;

    }
    if (dbAvail)
    {
        NSString *sqlString  = [NSString stringWithFormat:@"Delete from %@ where %@",table,whereClause];
        if(sqlite3_prepare_v2([DAO OttoDB], [sqlString UTF8String],-1, &insert_stmt, NULL) == SQLITE_OK)
        {

        }
        if(sqlite3_step(insert_stmt) == SQLITE_DONE) {

            sqlite3_finalize(insert_stmt);

        }
        if (dbAvail)
        {
            [DAO closeDB];
        }

    }
    finish(nil);
}
@end

