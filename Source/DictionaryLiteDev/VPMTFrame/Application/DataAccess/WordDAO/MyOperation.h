//
//  MyOperation.h
//  VPMTFrameExample
//
//  Created by Duy on 11/25/15.
//  Copyright © 2015 CHUONGPD2. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyOperation : NSObject
@property (nonatomic,strong) NSString *queryString;

@property (nonatomic,copy) id (^ProcessQuery)(sqlite3_stmt* querystatement);

@property (nonatomic,copy) void (^ProcessQueryResult)(id result);

@property (nonatomic,strong) NSArray *dataOfColumns;

@property (nonatomic,strong) NSString *whereClause;

@property (nonatomic,strong) NSString *tableName;


@end
