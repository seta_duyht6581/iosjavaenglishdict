//
//  BaseDao.h
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/16/15.
//  Copyright (c) 2015 DuyHoang. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "DAO.h"
#import "MyOperation.h"

/**
 *   Kieu block xu ly khi truy van xong
 *
 */

@interface BaseDao: NSObject
typedef id (^ProcessQuery)(sqlite3_stmt* querystatement);
typedef void (^ProcessQueryResult)(id result);
@property (strong, nonatomic) NSOperationQueue *operationQueue;
@property (strong, nonatomic) MyOperation *currentOperation;

/**
 *  Get String type
 *
 */
-(id)getStringWithStmt:(sqlite3_stmt*)stmt andIndex:(int)index;
-(id)getIntWithStmt:(sqlite3_stmt*)stmt andIndex:(int)index;

/**
 *  Query by string
 *
 */
- (void) queryWithStatement:(NSString*)queryString
            andProcessBlock:(ProcessQuery)processquery
             andFinishBlock:(ProcessQueryResult)finish;

/**
 *  Update nhieu column
 *
 *  @param dataOfColumns Array chua cac rows, moi row la 1 Dictionnary
 *  @param whereClause   <#whereClause description#>
 *  @param finish        <#finish description#>
 */
-(void)updateRowsWithData:(NSArray*)dataOfColumns
                fromTable:(NSString*)tableName
       andConditionClause:(NSString*)whereClause
           andFinishBlock:(ProcessQueryResult)finish;
/**
 *  Get Infor Of table
 *
 *  @param tableName <#tableName description#>
 *
 *  @return Dictionary contain key = columname, value = Type of data
 */
- (NSDictionary*)getInforOfTable:(NSString*)tableName;
- (void) deleteWithTable:(NSString*)table
          andWhereClause:(NSString*)whereClause
         andProcessBlock:(ProcessQuery)processquery
          andFinishBlock:(ProcessQueryResult)finish;
-(void)insertRowsWithData:(NSArray*)dataOfColumns
                fromTable:(NSString*)tableName
       andConditionClause:(NSString*)whereClause
           andFinishBlock:(ProcessQueryResult)finish;
/**
 *  @author DuyHt, 15-10-23 11:10:11
 *
 *  <#Description#>
 *
 *  @param dataOfColumns chua du lieu can bind theo thu tu index tu 0 - n
 *  @param queryString   cau query ko dua du lieu vao ma dua dau ?
 *  @param processquery  <#processquery description#>
 *  @param finish        <#finish description#>
 */
- (void) queryWithWithData:(NSArray*)dataOfColumns
            andQueryString:(NSString*)queryString
           andProcessBlock:(ProcessQuery)processquery
            andFinishBlock:(ProcessQueryResult)finish;

- (void) queryNoOperationWithStatement:(NSString*)queryString
                       andProcessBlock:(ProcessQuery)processquery
                        andFinishBlock:(ProcessQueryResult)finish;
-(void)updateNoOperationRowsWithData:(NSArray*)dataOfColumns
                           fromTable:(NSString*)tableName
                  andConditionClause:(NSString*)whereClause
                      andFinishBlock:(ProcessQueryResult)finish;
@end
