//
//  WordDao.h
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/16/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseDao.h"
#import "GrammerItem.h"
@interface WordDao : BaseDao
-(void)searchByText:(NSString*)keyword andTable:(TYPE_TABLE)table andFinishBlock:(ProcessQueryResult)finishBlock;
//Search default
- (void)searchDefaultTable:(TYPE_TABLE)table andfinishBlock:(ProcessQueryResult)finishBlock;
//Search bykeyword
-(void)getFavoriteTable:(TYPE_TABLE)table FinishBlock:(ProcessQueryResult)finishBlock;
- (void)setFavoriteForWordItem:(WordItem*)item
                    isFavorite:(BOOL)isfavorite
                andfinishBlock:(ProcessQueryResult)finishBlock;
- (void)removeAllFavoriteAndfinishBlock:(ProcessQueryResult)finishBlock;- (void)removeFavoriteWithid:(int)inword andFinishBlock:(ProcessQueryResult)finishBlock;
- (void)deleteWordItem:(WordItem*)item
        andfinishBlock:(ProcessQueryResult)finishBlock;

/**
 *  Get 3000 word oxford
 *
 *  @param finishBlock
 */
-(void)getGrammarFinishBlock:(ProcessQueryResult)finishBlock;

#pragma mark Kanji
/**
 *  @author DuyHt, 15-11-13 09:11:50
 *
 *  Kanji query
 *
 */
-(void)searchKanjiKey:(NSString*)key
       andFinishBlock:(ProcessQueryResult)finishBlock;
- (void)setFavoriteForWordItem:(NSNumber*)_id
                    isFavorite:(BOOL)isfavorite
                      andtable:(NSString*)table
                andfinishBlock:(ProcessQueryResult)finishBlock;
- (void)removeFavoriteWithid:(int)inword
                    andtable:(NSString*)table
              andFinishBlock:(ProcessQueryResult)finishBlock;
-(void)getKanjiFavoriteFinishBlock:(ProcessQueryResult)finishBlock;
-(void)getKanjiDefaultFinishBlock:(ProcessQueryResult)finishBlock;
- (void)removeAllFavoriteInTable:(NSString*)table AndfinishBlock:(ProcessQueryResult)finishBlock;
#pragma mark Grammar
-(void)getGrammarFavoriteFinishBlock:(ProcessQueryResult)finishBlock;
@end
