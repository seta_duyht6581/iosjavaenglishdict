//
//  WordDao.m
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/16/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import "WordDao.h"
#import "WordItem.h"
#import "VerbItem.h"
#import "NSString+Japanese.h"
/*
 // returns the type of text contained in the string (mixed or just hiragana / katakan / kanji etc)
 +(SLGJapaneseStringType)japaneseStringTypeForString:(NSString*)string;
 
 // checks if any of the chars in the string are katakana or hiragana or kanij
 +(BOOL)stringContainsJapaneseText:(NSString*)aString;
 */
/*
 NSOperationQueue *operationQueue = [NSOperationQueue new];
 [operationQueue setMaxConcurrentOperationCount:10];
 for( id imageURL in imageURLs)
 {
 NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:
 selector:@selector(processLoadingImageInNewThread:)
 object:imageURL];
 [operationQueue addOperation:operation];
 [operation release];
 }
 [operationQueue start];
 */

@implementation WordDao

//Search bykeyword
-(void)searchByText:(NSString*)keyword andTable:(TYPE_TABLE)table andFinishBlock:(ProcessQueryResult)finishBlock
{
    
    NSString *first;
    NSString *columnSearch = CL_WORD;
    keyword = [keyword lowercaseString];
    if (table == TYPE_EV) {
        if ([NSString stringContainsJapaneseText:keyword]) {
            if (keyword.length > 2) {
                keyword  = [keyword stringByTransliteratingJapaneseToRomaji];
                first = [keyword substringToIndex:1];
            }else
            {
                columnSearch = CL_WORD_SIGN;
                first = [[keyword stringByTransliteratingJapaneseToRomaji] substringToIndex:1];
            }
            
        } else {
            first = [keyword substringToIndex:1];
        }
        NSString *query = @"";
        if ([columnSearch isEqualToString:CL_WORD_SIGN]) {
            query = [NSString stringWithFormat:@"SELECT %@,%@,%@,%@,%@,%@ FROM %@ INDEXED BY %@ where (%@ like '%@%%' or pos like '%@%%') and _first = '%@' limit 200",CL_ID,CL_WORD,CL_MEAN,CL_FAVORITE,CL_WORD_SIGN,CL_POS,Table_English,INDEXED_UNSIGN_JAVI,columnSearch,keyword,keyword,first];
        }else {
            query = [NSString stringWithFormat:@"SELECT %@,%@,%@,%@,%@,%@ FROM %@ INDEXED BY %@ where %@ like '%@%%' and _first = '%@' limit 200",CL_ID,CL_WORD,CL_MEAN,CL_FAVORITE,CL_WORD_SIGN,CL_POS,Table_English,INDEXED_UNSIGN_JAVI,columnSearch,keyword,first];
        }
        [self queryWithStatement:query andProcessBlock:^id(sqlite3_stmt *querystatement) {
            WordItem *item = [[WordItem alloc] init];
            item.tableType = table;
            item.idOfword = [self getIntWithStmt:querystatement andIndex:0];
            item.fromStr = [self getStringWithStmt:querystatement andIndex:1];
            item.toStr = [self getStringWithStmt:querystatement andIndex:2];
            id favo = [self getIntWithStmt:querystatement andIndex:3];
            item.favorite = (favo == [NSNull null])? [NSNumber numberWithInt:0]:favo;
            item.fromSign = [self getStringWithStmt:querystatement andIndex:4];
            item.pronuncea = [self getStringWithStmt:querystatement andIndex:5];
            [item parseMeanOfWord];
            return item;
        } andFinishBlock:^(id result) {
            dispatch_async(dispatch_get_main_queue(), ^{
                finishBlock(result);
            });
        }];
        
    }else
    {
        
        //NSString *straccii = [NSString convertASCIIStringFromVietnameseString:keyword];
        first = [keyword substringToIndex:1];
        NSString *query = [NSString stringWithFormat:@"SELECT %@,%@,%@,%@,%@,%@ FROM %@ INDEXED BY %@ where (%@ like %@) and _first = '%@' limit 200",CL_ID,CL_WORD,CL_MEAN,CL_FAVORITE,CL_WORD_SIGN,CL_POS,Table_VE,INDEXED_UNSIGN_VIJA,CL_WORD_SIGN,@"?",first];
        NSMutableArray *arrayData = [[NSMutableArray alloc] init];
        keyword = [NSString stringWithFormat:@"%@%%",keyword];
        [arrayData addObject:keyword];
        [arrayData addObject:keyword];
        [self queryWithWithData:arrayData andQueryString:query andProcessBlock:^id(sqlite3_stmt *querystatement) {
            WordItem *item = [[WordItem alloc] init];
            item.tableType = table;
            item.idOfword = [self getIntWithStmt:querystatement andIndex:0];
            item.fromSign = [self getStringWithStmt:querystatement andIndex:4];
            item.fromStr = item.fromSign;
            item.toStr = [self getStringWithStmt:querystatement andIndex:2];
            id favo = [self getIntWithStmt:querystatement andIndex:3];
            item.favorite = (favo == [NSNull null])? [NSNumber numberWithInt:0]:favo;
            
            item.pronuncea = [self getStringWithStmt:querystatement andIndex:5];
            [item parseMeanOfWord];
            
            return item;
        } andFinishBlock:^(id result) {
            dispatch_async(dispatch_get_main_queue(), ^{
                finishBlock(result);
            });
        }];
        
        
    }
    
    
}

- (void)searchDefaultTable:(TYPE_TABLE)table andfinishBlock:(ProcessQueryResult)finishBlock
{
    
    NSString *query = [NSString stringWithFormat:@"SELECT %@,%@,%@,%@,%@,%@ FROM %@ where rowid > 20 limit 1000 ",CL_ID,CL_WORD,CL_MEAN,CL_FAVORITE,CL_WORD_SIGN,CL_POS,[AppDelegate sharedManager].curTable];
    [self queryWithStatement:query andProcessBlock:^id(sqlite3_stmt *querystatement) {
        WordItem *item = [[WordItem alloc] init];
        item.tableType = table;
        item.idOfword = [self getIntWithStmt:querystatement andIndex:0];
        item.fromStr = [self getStringWithStmt:querystatement andIndex:1];
        item.toStr = [self getStringWithStmt:querystatement andIndex:2];
        id favo = [self getIntWithStmt:querystatement andIndex:3];
        item.favorite = (favo == [NSNull null])? [NSNumber numberWithInt:0]:favo;
        item.fromSign = [self getStringWithStmt:querystatement andIndex:4];
        
        if ([AppDelegate sharedManager].TYPE_TABLE_CURRENT == TYPE_VE) {
            item.fromSign = [self getStringWithStmt:querystatement andIndex:4];
            item.fromStr = item.fromSign;
        }
        
        item.pronuncea = [self getStringWithStmt:querystatement andIndex:5];
        [item parseMeanOfWord];
        return item;
    } andFinishBlock:^(id result) {
        dispatch_async(dispatch_get_main_queue(), ^{
            finishBlock(result);
        });
    }];
}

//Search bykeyword
-(void)getFavoriteTable:(TYPE_TABLE)table FinishBlock:(ProcessQueryResult)finishBlock
{
    NSString *query = [NSString stringWithFormat:@"SELECT %@,%@,%@,%@,%@,%@ FROM %@ where favorite = 1 ",CL_ID,CL_WORD,CL_MEAN,CL_FAVORITE,CL_WORD_SIGN,CL_POS,[AppDelegate sharedManager].curTable];
    [self queryWithStatement:query andProcessBlock:^id(sqlite3_stmt *querystatement) {
        WordItem *item = [[WordItem alloc] init];
        item.tableType = table;
        item.idOfword = [self getIntWithStmt:querystatement andIndex:0];
        item.fromStr = [self getStringWithStmt:querystatement andIndex:1];
        item.toStr = [self getStringWithStmt:querystatement andIndex:2];
        id favo = [self getIntWithStmt:querystatement andIndex:3];
        item.favorite = (favo == [NSNull null])? [NSNumber numberWithInt:0]:favo;
        item.fromSign = [self getStringWithStmt:querystatement andIndex:4];
        
        if ([AppDelegate sharedManager].TYPE_TABLE_CURRENT == TYPE_VE) {
            item.fromSign = [self getStringWithStmt:querystatement andIndex:4];
            item.fromStr = item.fromSign;
        }
        
        item.pronuncea = [self getStringWithStmt:querystatement andIndex:5];
        [item parseMeanOfWord];
        return item;
    } andFinishBlock:^(id result) {
        dispatch_async(dispatch_get_main_queue(), ^{
            finishBlock(result);
        });
    }];
    
}
- (void)removeFavoriteWithid:(int)inword andFinishBlock:(ProcessQueryResult)finishBlock
{
    NSMutableArray *datas = [[NSMutableArray alloc] init];
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setValue:[NSNumber numberWithInt:0] forKey:@"favorite"];
    [datas addObject:dic];
    NSString *whereclause = [NSString stringWithFormat:@" _id = %d",inword];
    [self updateRowsWithData:datas fromTable:[AppDelegate sharedManager].curTable andConditionClause:whereclause andFinishBlock:^(id result) {
        dispatch_async(dispatch_get_main_queue(), ^{
            finishBlock(result);
        });
    }];
}
- (void)removeAllFavoriteAndfinishBlock:(ProcessQueryResult)finishBlock
{
    NSMutableArray *datas = [[NSMutableArray alloc] init];
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setValue:[NSNumber numberWithInt:0] forKey:@"favorite"];
    [datas addObject:dic];
    NSString *whereclause = [NSString stringWithFormat:@" favorite = 1"];
    [self updateRowsWithData:datas fromTable:[AppDelegate sharedManager].curTable andConditionClause:whereclause andFinishBlock:^(id result) {
        dispatch_async(dispatch_get_main_queue(), ^{
            finishBlock(result);
        });
    }];
}
- (void)setFavoriteForWordItem:(WordItem*)item
                    isFavorite:(BOOL)isfavorite
                andfinishBlock:(ProcessQueryResult)finishBlock
{
    NSMutableArray *datas = [[NSMutableArray alloc] init];
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setValue:[NSNumber numberWithInt:(isfavorite?1:0)] forKey:@"favorite"];
    [datas addObject:dic];
    NSString *tablename = Table_English;
    if (item.tableType == TYPE_VE) {
        tablename = Table_VE;
    }
    
    NSString *whereclause = [NSString stringWithFormat:@" _id = %ld",[item.idOfword longValue]];
    [self updateRowsWithData:datas fromTable:tablename andConditionClause:whereclause andFinishBlock:^(id result) {
        dispatch_async(dispatch_get_main_queue(), ^{
            finishBlock(result);
        });
    }];
}
- (void)deleteWordItem:(WordItem*)item
        andfinishBlock:(ProcessQueryResult)finishBlock
{
    [self deleteWordItem:item andfinishBlock:^(id result) {
        finishBlock(result);
    }];
}

#pragma mark Kanji

- (void)setFavoriteForWordItem:(NSNumber*)_id
                    isFavorite:(BOOL)isfavorite
                      andtable:(NSString*)table
                andfinishBlock:(ProcessQueryResult)finishBlock
{
    NSMutableArray *datas = [[NSMutableArray alloc] init];
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setValue:[NSNumber numberWithInt:(isfavorite?1:0)] forKey:@"favorite"];
    [datas addObject:dic];
    
    NSString *whereclause = [NSString stringWithFormat:@" _id = %ld",[_id longValue]];
    [self updateRowsWithData:datas fromTable:table andConditionClause:whereclause andFinishBlock:^(id result) {
        dispatch_async(dispatch_get_main_queue(), ^{
            finishBlock(result);
        });
    }];
}
- (void)removeAllFavoriteInTable:(NSString*)table AndfinishBlock:(ProcessQueryResult)finishBlock
{
    NSMutableArray *datas = [[NSMutableArray alloc] init];
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setValue:[NSNumber numberWithInt:0] forKey:@"favorite"];
    [datas addObject:dic];
    NSString *whereclause = [NSString stringWithFormat:@" favorite = 1"];
    [self updateRowsWithData:datas fromTable:table andConditionClause:whereclause andFinishBlock:^(id result) {
        dispatch_async(dispatch_get_main_queue(), ^{
            finishBlock(result);
        });
    }];
}
- (void)removeFavoriteWithid:(int)inword
                    andtable:(NSString*)table
              andFinishBlock:(ProcessQueryResult)finishBlock
{
    NSMutableArray *datas = [[NSMutableArray alloc] init];
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setValue:[NSNumber numberWithInt:0] forKey:@"favorite"];
    [datas addObject:dic];
    NSString *whereclause = [NSString stringWithFormat:@" _id = %d",inword];
    [self updateRowsWithData:datas fromTable:table andConditionClause:whereclause andFinishBlock:^(id result) {
        dispatch_async(dispatch_get_main_queue(), ^{
            finishBlock(result);
        });
    }];
}
-(void)getKanjiDefaultFinishBlock:(ProcessQueryResult)finishBlock
{
    NSString *query = [NSString stringWithFormat:@"SELECT _id,Nhat,OnYomi,kunyomi,img,content,examples,stroke_count,favorite FROM %@ where (img is not null) limit 500",Table_KANJI];
    [self queryWithStatement:query andProcessBlock:^id(sqlite3_stmt *querystatement) {
        return [self parseKanjiWithStatement:querystatement];
    } andFinishBlock:^(id result) {
        dispatch_async(dispatch_get_main_queue(), ^{
            finishBlock(result);
        });
    }];
    
}

//Search bykeyword
-(void)getKanjiFavoriteFinishBlock:(ProcessQueryResult)finishBlock
{
    NSString *query = [NSString stringWithFormat:@"SELECT _id,Nhat,OnYomi,kunyomi,img,content,examples,stroke_count,favorite FROM %@ where (favorite = 1) limit 500",Table_KANJI];
    [self queryWithStatement:query andProcessBlock:^id(sqlite3_stmt *querystatement) {
        return [self parseKanjiWithStatement:querystatement];
    } andFinishBlock:^(id result) {
        dispatch_async(dispatch_get_main_queue(), ^{
            finishBlock(result);
        });
    }];
    
}

-(void)searchKanjiKey:(NSString*)key
       andFinishBlock:(ProcessQueryResult)finishBlock
{

    NSString *query;
    key = [key lowercaseString];
    SLGJapaneseStringType type = [NSString japaneseStringTypeForString:key];
    if (type == SLGJapaneseStringTypeKanji) {
        query = [NSString stringWithFormat:@"SELECT _id,Nhat,OnYomi,kunyomi,img,content,examples,stroke_count,favorite FROM %@ where Nhat = '%@' limit 200",Table_KANJI,key];
    }else {
        key = [key stringByTransliteratingJapaneseToRomaji];
        query = [NSString stringWithFormat:@"SELECT _id,Nhat,OnYomi,kunyomi,img,content,examples,stroke_count,favorite FROM %@ where (%@ like '%@%%') and (%@ like '%@%%' ) limit 200",Table_KANJI,CL_ONYOMI_UNSIGN,key,CL_NHAT_UNSIGN,key];
    }
    [self queryWithStatement:query andProcessBlock:^id(sqlite3_stmt *querystatement) {
        return [self parseKanjiWithStatement:querystatement];
    } andFinishBlock:^(id result) {
        dispatch_async(dispatch_get_main_queue(), ^{
            finishBlock(result);
        });
    }];
    
}
- (VerbItem*)parseKanjiWithStatement:(sqlite3_stmt *)querystatement
{
    VerbItem *verb = [[VerbItem alloc] init];
    verb._id = [self getIntWithStmt:querystatement andIndex:0];
    verb.Nhat= [self getStringWithStmt:querystatement andIndex:1];
    verb.OnYomi = [self getStringWithStmt:querystatement andIndex:2];
    verb.kunYomi = [self getStringWithStmt:querystatement andIndex:3];
    verb.img = [self getStringWithStmt:querystatement andIndex:4];
    verb.Content = [self getStringWithStmt:querystatement andIndex:5];
    verb.examples = [self getStringWithStmt:querystatement andIndex:6];
    verb.stroke_count = [self getIntWithStmt:querystatement andIndex:7];
    id favo = [self getIntWithStmt:querystatement andIndex:8];
    verb.favorite = (favo == [NSNull null])? [NSNumber numberWithInt:0]:favo;
    return verb;
}
#pragma mark - Grammar
/**
 *  Get 3000 word oxford
 *
 *  @param finishBlock
 */
-(void)getGrammarFinishBlock:(ProcessQueryResult)finishBlock
{
    NSString *query = [NSString stringWithFormat:@"SELECT key,mean,level,_id,favorite FROM %@",Table_Grammer];
    [self queryWithStatement:query andProcessBlock:^id(sqlite3_stmt *querystatement) {
        
        GrammerItem *item = [[GrammerItem alloc] init];
        item.fromStr = [self getStringWithStmt:querystatement andIndex:0];
        item.toStr = [self getStringWithStmt:querystatement andIndex:1];
        item.level = [self getIntWithStmt:querystatement andIndex:2];
        item._id = [self getIntWithStmt:querystatement andIndex:3];
        id favo = [self getIntWithStmt:querystatement andIndex:4];
        item.favorite = (favo == [NSNull null])? [NSNumber numberWithInt:0]:favo;
        
        [item parseMeanWord];
        return item;
    } andFinishBlock:^(id result) {
        dispatch_async(dispatch_get_main_queue(), ^{
            finishBlock(result);
        });
    }];
    
}
-(void)getGrammarFavoriteFinishBlock:(ProcessQueryResult)finishBlock
{
    NSString *query = [NSString stringWithFormat:@"SELECT key,mean,level,_id,favorite FROM %@ where favorite = 1",Table_Grammer];
    [self queryWithStatement:query andProcessBlock:^id(sqlite3_stmt *querystatement) {
        
        GrammerItem *item = [[GrammerItem alloc] init];
        item.fromStr = [self getStringWithStmt:querystatement andIndex:0];
        item.toStr = [self getStringWithStmt:querystatement andIndex:1];
        item.level = [self getIntWithStmt:querystatement andIndex:2];
        item._id = [self getIntWithStmt:querystatement andIndex:3];
        id favo = [self getIntWithStmt:querystatement andIndex:4];
        item.favorite = (favo == [NSNull null])? [NSNumber numberWithInt:0]:favo;
        [item parseMeanWord];
        return item;
    } andFinishBlock:^(id result) {
        dispatch_async(dispatch_get_main_queue(), ^{
            finishBlock(result);
        });
    }];
    
}
@end
