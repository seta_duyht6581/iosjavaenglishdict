//
//  VerbItem.h
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/18/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//


#import <Foundation/Foundation.h>
/*
 CREATE TABLE "kanji" ("_id" INTEGER PRIMARY KEY  NOT NULL  DEFAULT (null) ,"Nhat" TEXT DEFAULT (null) ,"Han" TEXT DEFAULT (null) ,"level" INTEGER,"OnYomi" TEXT DEFAULT (null) ,"kunYomi" TEXT DEFAULT (null) ,"img" VARCHAR,"Content" TEXT DEFAULT (null) ,"stroke_count" INTEGER,"parts" TEXT DEFAULT (null) ,"examples" TEXT,"nhat_unsign" TEXT,"onyomi_unsign" TEXT,"_first_nhat" CHAR DEFAULT (null) ,"_first_onyo" CHAR)
 */
@interface VerbItem : NSObject
@property (strong,nonatomic) NSString *verb;
@property (strong,nonatomic) NSString *verbPast;
@property (strong,nonatomic) NSString *verbPastPast;
@property (assign,nonatomic) BOOL isFilter;
@property (strong,nonatomic) NSNumber *_id;
@property (strong,nonatomic) NSString *Nhat;
@property (strong,nonatomic) NSString *Han;
@property (strong,nonatomic) NSString *level;
@property (strong,nonatomic) NSString *OnYomi;
@property (strong,nonatomic) NSString *kunYomi;
@property (strong,nonatomic) NSString *img;
@property (strong,nonatomic) NSString *Content;
@property (strong,nonatomic) NSString *parts;
@property (strong,nonatomic) NSString *examples;
@property (strong,nonatomic) NSString *nhat_unsign;
@property (strong,nonatomic) NSString *onyomi_unsign;
@property (strong,nonatomic) NSNumber *stroke_count;
@property (strong,nonatomic) NSNumber *favorite;
-(void)parseText;
@end
