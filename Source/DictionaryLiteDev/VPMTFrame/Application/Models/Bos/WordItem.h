//
//  WordItem.h
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/12/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface WordItem : NSObject
@property (strong,nonatomic) NSString *fromStr;//tu goc
@property (strong,nonatomic) NSString *fromSign;//tu co dau
@property (strong,nonatomic) NSString *toStr;//nghia
@property (strong,nonatomic) NSString *pronuncea;// phien am
@property (strong,nonatomic) NSString *shortmean;// nghia ngan
@property (strong,nonatomic) NSNumber *idOfword;//id
@property (strong,nonatomic) NSNumber *favorite;//favorite
@property (strong,nonatomic) NSNumber *level;//favorite
@property (assign,nonatomic) TYPE_TABLE tableType;
- (void)parseMeanOfWord;
@end
