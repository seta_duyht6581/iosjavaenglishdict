//
//  WordItem.m
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/12/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import "GrammerItem.h"

@implementation GrammerItem


- (void)parseMeanWord
{
    self.toStr = [self.toStr stringByReplacingOccurrencesOfString:@"$$" withString:@"\n"];
    self.searchStr = [NSString stringWithFormat:@"%d",self.level.intValue];
    if (self.level.intValue == 3) {
        self.searchStr = @"23";
    }
}

@end
