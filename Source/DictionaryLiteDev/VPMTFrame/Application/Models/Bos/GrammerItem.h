//
//  WordItem.h
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/12/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface GrammerItem : NSObject
@property (strong,nonatomic) NSString *fromStr;//tu goc
@property (strong,nonatomic) NSString *toStr;//nghia
@property (strong,nonatomic) NSNumber *level;//favorite
@property (strong,nonatomic) NSNumber *_id;//id
@property (strong,nonatomic) NSNumber *favorite;//favorite
@property (assign,nonatomic) TYPE_TABLE tableType;
@property (strong,nonatomic) NSString *searchStr;
- (void)parseMeanWord;
@end
