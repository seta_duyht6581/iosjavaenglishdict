//
//  ConfigUserDefault.h
//  VPMTFrameExample
//
//  Created by Duy on 11/15/15.
//  Copyright © 2015 CHUONGPD2. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface ConfigUserDefault : NSObject
@property (nonatomic,assign) BOOL isFirstStart;
@property (nonatomic,assign) NSInteger numRequestFullAds;
@property (nonatomic,assign) NSInteger numRequestBannerAds;
@property (nonatomic,strong) NSDate *dateRemoveAds;
@end
