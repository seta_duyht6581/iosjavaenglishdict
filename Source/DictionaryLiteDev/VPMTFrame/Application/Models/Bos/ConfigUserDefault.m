//
//  ConfigUserDefault.m
//  VPMTFrameExample
//
//  Created by Duy on 11/15/15.
//  Copyright © 2015 CHUONGPD2. All rights reserved.
//

#import "ConfigUserDefault.h"

@implementation ConfigUserDefault
- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeBool:self.isFirstStart forKey:@"isFirstStart"];
    [encoder encodeInteger:self.numRequestFullAds forKey:@"numRequestFullAds"];
    [encoder encodeInteger:self.numRequestBannerAds forKey:@"numRequestBannerAds"];
    [encoder encodeObject:self.dateRemoveAds forKey:@"dateRemoveAds"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.isFirstStart = [decoder decodeBoolForKey:@"isFirstStart"];
        self.numRequestFullAds = [decoder decodeIntegerForKey:@"numRequestFullAds"];
        self.numRequestBannerAds = [decoder decodeIntegerForKey:@"numRequestBannerAds"];
        self.dateRemoveAds = [decoder decodeObjectForKey:@"dateRemoveAds"];
    }
    return self;
}
@end
