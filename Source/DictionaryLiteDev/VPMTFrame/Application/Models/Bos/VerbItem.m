//
//  VerbItem.m
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/18/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import "VerbItem.h"

@implementation VerbItem
-(void)parseText
{

    NSArray *array = [self.verb componentsSeparatedByString:@","];
    self.verb = [array objectAtIndex:0];
    self.verbPast = [array objectAtIndex:1];
    self.verbPastPast = [array objectAtIndex:2];
}


@end
