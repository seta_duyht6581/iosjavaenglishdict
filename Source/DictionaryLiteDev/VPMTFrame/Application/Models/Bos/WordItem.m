//
//  WordItem.m
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/12/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import "WordItem.h"

@implementation WordItem



- (void)parseMeanOfWord
{
    //"\x01"
    NSArray *arrayData;
    if ([self.toStr containsString:@"***"]) {
        arrayData = [self.toStr componentsSeparatedByString:@"***"];
    }else {
        arrayData = [self.toStr componentsSeparatedByString:@"\x01"];
    }
    if ([arrayData count]>1) {
        NSString *item0 = [arrayData objectAtIndex:0];
        NSString *item1 = [arrayData objectAtIndex:1];
        NSArray *array0 = [item0 componentsSeparatedByString:@"$$$"];
        NSArray *array1 = [item1 componentsSeparatedByString:@"$$$"];
        self.shortmean = [NSString stringWithFormat:@"%@\n%@",[array0 objectAtIndex:0],[array1 objectAtIndex:0]];
    }else
    {
        NSString *item0 = [arrayData objectAtIndex:0];
        NSArray *array0 = [item0 componentsSeparatedByString:@"$$$"];
        self.shortmean = [array0 objectAtIndex:0];
    }
    
}
- (NSString*)removeFirstletterAndtrims:(NSString*)string
{
    if (string.length>0) {
        string = [[string substringFromIndex:1] trims];
        return string;
    }
    return string;
    
}

@end
