//
//  ModelDelegate.h
//
//  Created by viettel on 6/7/14.
//  Copyright (c) 2014 Pham Duc Chuong. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ModelFinishBlock)(id result);
typedef void (^ModelErrorBlock)(NSError *error);
@interface BaseBS : NSObject

    + (instancetype)sharedManager;

    -(void)temp:(id)params onFinish:(ModelFinishBlock)finish;

    -(void)temp:(id)params onFinish:(ModelFinishBlock)finish onError:(ModelErrorBlock)error;
@end
