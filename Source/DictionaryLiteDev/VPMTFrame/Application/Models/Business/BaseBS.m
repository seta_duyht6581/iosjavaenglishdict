//
//  ModelDelegate.m
//
//  Created by viettel on 6/7/14.
//  Copyright (c) 2014 Pham Duc Chuong. All rights reserved.
//

#import "BaseBS.h"

@implementation BaseBS

- (id)init
{
    self = [super init];
    if (self) {
       
    }
    return  self;
}


static BaseBS* sharedManager = nil;

+ (instancetype)sharedManager {
    @synchronized(self)
    {
        if (sharedManager && [sharedManager isKindOfClass:[self class]]){
            return sharedManager;
        }else{
            sharedManager = [[self alloc] init];
            
            return sharedManager;
        }
    }
}



-(void)temp:(id)params onFinish:(ModelFinishBlock)finish{
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
}

-(void)temp:(id)params onFinish:(ModelFinishBlock)finish onError:(ModelErrorBlock)error{
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
}
@end
