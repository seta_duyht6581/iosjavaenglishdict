//
//  LanguageDefine.h
//  VPMTFrame
//
//  Created by CHUONGPD2 on 6/18/14.
//  Copyright (c) 2014 CHUONGPD2. All rights reserved.
//
//


//***********************************************************//
// HELLO
#define APP_NAME NSLocalizedString(@"APP_NAME",@"")
#define MESSAGE_NOTFOUND_WORD NSLocalizedString(@"MESSAGE_NOTFOUND_WORD",@"")
#define TITLE_CLIPBOARD NSLocalizedString(@"TITLE_CLIPBOARD",@"")
#define BUTTON_CANCLE NSLocalizedString(@"BUTTON_CANCLE",@"")
#define BUTTON_OK NSLocalizedString(@"BUTTON_OK",@"")
#define BUTTON_DELETE_ALL NSLocalizedString(@"BUTTON_DELETE_ALL",@"")

#define TITLE_DETAIL NSLocalizedString(@"TITLE_DETAIL",@"")
#define TITLE_ABOUT NSLocalizedString(@"TITLE_ABOUT",@"")
#define TITLE_REMOVE_ADS NSLocalizedString(@"TITLE_REMOVE_ADS",@"")
#define TITLE_TRANSLATE NSLocalizedString(@"TITLE_TRANSLATE",@"")
#define TITLE_RATEAPP NSLocalizedString(@"TITLE_RATEAPP",@"")
#define TITLE_SHARE NSLocalizedString(@"TITLE_SHARE",@"")
#define TITLE_FEEDBACK NSLocalizedString(@"TITLE_FEEDBACK",@"")


#define OUR_APP NSLocalizedString(@"OUR_APP",@"")
#define REMOVEADS NSLocalizedString(@"REMOVEADS",@"")
#define ADSFUNCTION NSLocalizedString(@"ADSFUNCTION",@"")
#define OTHER NSLocalizedString(@"OTHER",@"")
#define TITLE_DICTIONARY NSLocalizedString(@"DICTIONARY",@"")
#define TITLE_DICTIONARY_EV NSLocalizedString(@"TITLE_DICTIONARY_EV",@"")
#define TITLE_DICTIONARY_VE NSLocalizedString(@"TITLE_DICTIONARY_VE",@"")
#define VERB NSLocalizedString(@"VERB",@"")
#define MAINFUNTION NSLocalizedString(@"MAINFUNTION",@"")
#define EXTRAFUNTION NSLocalizedString(@"EXTRAFUNTION",@"")
#define ADSFUNCTION NSLocalizedString(@"ADSFUNCTION",@"")
#define MESSAGE_NOT_SETEMAIL NSLocalizedString(@"MESSAGE_NOT_SETEMAIL",@"")
#define MESSAGE_SHARE_APP NSLocalizedString(@"MESSAGE_SHARE_APP",@"")
#define MESSAGE_CONFIRM_DELETE NSLocalizedString(@"MESSAGE_CONFIRM_DELETE",@"")
#define MESSAGE_DELETE_WORD NSLocalizedString(@"MESSAGE_DELETE_WORD",@"")
#define LOADING NSLocalizedString(@"LOADING",@"")

#define RATE_APP_MESSAGE NSLocalizedString(@"RATE_APP_MESSAGE",@"")
#define RATE_APP_RATE_NOW NSLocalizedString(@"RATE_APP_RATE_NOW",@"")
#define RATE_APP_REMIND_LATER NSLocalizedString(@"RATE_APP_REMIND_LATER",@"")
#define RATE_APP_NO_THANKS NSLocalizedString(@"RATE_APP_NO_THANKS",@"")
#define RATE_PLEASE NSLocalizedString(@"RATE_PLEASE",@"")

#define MESSAGE_DONT_HAVE_FAVORITE NSLocalizedString(@"MESSAGE_DONT_HAVE_FAVORITE",@"")
#define MESSAGE_DONT_HAVE_FAVORITE2 NSLocalizedString(@"MESSAGE_DONT_HAVE_FAVORITE2",@"")
#define MESSAGE_DONT_HAVE_RECENT NSLocalizedString(@"MESSAGE_DONT_HAVE_RECENT" ,@"")
#define MESSAGE_DONT_HAVE_RECENT2 NSLocalizedString(@"MESSAGE_DONT_HAVE_RECENT2",@"")
#define BAR_SEARCH NSLocalizedString(@"BAR_SEARCH" ,@"")
#define BAR_RECENT NSLocalizedString(@"BAR_RECENT",@"")
#define BAR_FAVORITE NSLocalizedString(@"BAR_FAVORITE",@"")
#define BAR_TRANSLATE NSLocalizedString(@"BAR_TRANSLATE",@"")
#define STRING_TRANSLATE NSLocalizedString(@"STRING_TRANSLATE",@"")

#define SEG_ALL NSLocalizedString(@"SEG_ALL",@"")
#define SEG_FAVORITE NSLocalizedString(@"SEG_FAVORITE",@"")

#define REMOVE_ADS_MESSAGE NSLocalizedString(@"REMOVE_ADS_MESSAGE",@"")
#define BUTTON_FULL NSLocalizedString(@"BUTTON_FULL",@"")
#define PLACEHOLDER_GRAMMAR NSLocalizedString(@"PLACEHOLDER_GRAMMAR",@"")
#define PLACEHOLDER_KANJI NSLocalizedString(@"PLACEHOLDER_KANJI",@"")

#define CHAONHANVIEN(chucdanh,ten) [NSString stringWithFormat:NSLocalizedString(@"CHAONHANVIEN",@""),chucdanh,ten]
