//
//  GlobalDefine.h
//  VPMTFrame
//
//  Created by CHUONGPD2 on 6/18/14.
//  Copyright (c) 2014 CHUONGPD2. All rights reserved.
//

/*******************************Network Define***************************************/

typedef enum {
    TYPE_EV,
    TYPE_VE,
    TYPE_KANJI,
    TYPE_GRAMMAR
} TYPE_TABLE;
#pragma mark - Network define


#define DOMAIN_NAME @"https://10.60.15.105:8998/"
#define KEY_INSERT @"INSERT"
#define LOGIN_API MakeURL(@"UniAuthentication.svc/login")
#define SYNC_TABLE_API MakeURL(@"UniNotification.svc/getnotifications")
//Colore
#define COLOR_LEFT_BACKGROUND @"#2d213e"
#define COLOR_VE_TEXT @"016c63"
#define COLOR_EV_TEXT @"0f99c6"

#define PASS_ZIP_DATABASE @"tanduy@1991"

#define KEY_REVIEW @"dictionnary.review"
#define KEY_REVIEW_LATER @"dictionnary.review.later"
#define REVIEW_LATER @"reviewlater"
#define REVIEW_CANCEL @"denied"
#define REVIEWED @"deviewed"
//Define Ads StartApp
#define STARTAPP_APP_ID @"209351984"
#define STARTAPP_DEV_ID @"109488100"

#define GDA_ADDUNIT_ID_BANNER @"ca-app-pub-6472469337251368/4293121134"
#define GDA_ADDUNIT_ID_BANNER2 @"ca-app-pub-6472469337251368/1397141938"
#define GDA_ADD_FULLSCREEN @"ca-app-pub-6472469337251368/6630019134"

//id1056900140,id1059167740
#define ITUN_APP_ID @"id1063630371"
#define ITUN_MYAPP  [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/%@",ITUN_APP_ID]
#define MY_EMAIL @"hoangtanduy247@gmail.com"
#define SUBJECT_EMAIL @"[Japanese English Dictionnary Feedback]"
#define LINK_TRANSLATE @"https://translate.google.com/m/translate"
#define LINK_APP @"https://itunes.apple.com/app/"
#define ITUn_FULL_APP @"itms-apps://itunes.apple.com/app/id1059167740"

#define KEY_ConfigUserDefault @"ConfigUserDefault"