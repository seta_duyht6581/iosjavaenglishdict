//
//  DeviceUtils.m
//  ViettelPI
//
//  Created by Trinh Duy Nhan on 4/1/14.
//  Copyright (c) 2014 viettel. All rights reserved.
//

#import "DeviceUtils.h"
#include <sys/utsname.h>
#include <sys/types.h>
#include <sys/sysctl.h>
#import <ExternalAccessory/ExternalAccessory.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import <mach/mach.h>
#import <mach/mach_host.h>

#define MIB_SIZE 2

#define MB (double)(1024*1024)
#define GB (double)(MB*1024)
#define TB (double)(GB*1024)

@interface DeviceUtils()

+ (UIDevice *)deviceBattery;
+ (NSDictionary *)infoForDevice;

@end

@implementation DeviceUtils

//=============================================
//          Battery
//=============================================
#pragma mark -
#pragma mark - Battery

+ (UIDevice *)deviceBattery {
    [UIDevice currentDevice].batteryMonitoringEnabled = YES;
    return [UIDevice currentDevice];
}


+ (NSDictionary *)infoForDevice {
    NSString *device = [self platformType];
    NSDictionary *info = [[NSDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[device stringByReplacingOccurrencesOfString:@" " withString:@""] ofType:@"plist"]];
    return info;
}


+ (BOOL)batteryFullCharged
{
    if ([self batteryLevel] == 100) {
        return YES;
    }
    return NO;
}


+ (BOOL)batteryInCharge
{
    if ([self deviceBattery].batteryState == UIDeviceBatteryStateCharging ||
        [self deviceBattery].batteryState == UIDeviceBatteryStateFull) {
        return YES;
    } else {
        return NO;
    }
}


+ (BOOL)devicePluggedIntoPower
{
    if ([self deviceBattery].batteryState == UIDeviceBatteryStateUnplugged) {
        return NO;
    } else {
        return YES;
    }
}


+ (UIDeviceBatteryState)batteryState
{
    return [self deviceBattery].batteryState;
}


+ (CGFloat)batteryLevel
{
    CGFloat batteryLevel = 0.0f;
    CGFloat batteryCharge = [self deviceBattery].batteryLevel;
    if (batteryCharge > 0.0f)
        batteryLevel = batteryCharge * 100;
    else
        // Unable to find battery level
        return -1;
    
    return batteryLevel;
}


+ (NSString *)remainingHoursForStandby
{
    CGFloat batteryLevel = [self batteryLevel];
    NSDictionary *info = [self infoForDevice];
    NSInteger maxHours = 0;
    if ([info objectForKey:@"standby"]) {
        maxHours = [[info objectForKey:@"standby"] integerValue];
        CGFloat totMinutes = (maxHours - ((100 - batteryLevel) * maxHours / 100)) * 60; //remaining time in minutes
        NSInteger hours = totMinutes / 60; //get hours
        NSInteger minutes = totMinutes - (hours * 60); // get minutes
        if (hours < 0 || minutes < 0) {
            return [NSString stringWithFormat:@"ND"];
        }
        return [NSString stringWithFormat:@"%i:%02i",(int)hours,(int)minutes];
    } else
        return @"NS";
}


+ (NSString *)remainingHoursFor3gConversation
{
    CGFloat batteryLevel = [self batteryLevel];
    NSDictionary *info = [self infoForDevice];
    NSInteger maxHours = 0;
    if ([info objectForKey:@"conversation3g"]) {
        maxHours = [[info objectForKey:@"conversation3g"] integerValue];
        CGFloat totMinutes = (maxHours - ((100 - batteryLevel) * maxHours / 100)) * 60; //remaining time in minutes
        NSInteger hours = totMinutes / 60; //get hours
        NSInteger minutes = totMinutes - (hours * 60); // get minutes
        if (hours < 0 || minutes < 0) {
            return [NSString stringWithFormat:@"ND"];
        }
        return [NSString stringWithFormat:@"%i:%02i",(int)hours,(int)minutes];
    } else
        return @"NS";
}


+ (NSString *)remainingHoursForInternet3g
{
    CGFloat batteryLevel = [self batteryLevel];
    NSDictionary *info = [self infoForDevice];
    NSInteger maxHours = 0;
    if ([info objectForKey:@"internet3g"]) {
        maxHours = [[info objectForKey:@"internet3g"] integerValue];
        CGFloat totMinutes = (maxHours - ((100 - batteryLevel) * maxHours / 100)) * 60; //remaining time in minutes
        NSInteger hours = totMinutes / 60; //get hours
        NSInteger minutes = totMinutes - (hours * 60); // get minutes
        if (hours < 0 || minutes < 0) {
            return [NSString stringWithFormat:@"ND"];
        }
        return [NSString stringWithFormat:@"%i:%02i",(int)hours,(int)minutes];
    } else
        return @"NS";
}


+ (NSString *)remainingHoursForInternetWiFi
{
    CGFloat batteryLevel = [self batteryLevel];
    NSDictionary *info = [self infoForDevice];
    NSInteger maxHours = 0;
    if ([info objectForKey:@"internetwifi"]) {
        maxHours = [[info objectForKey:@"internetwifi"] integerValue];
        CGFloat totMinutes = (maxHours - ((100 - batteryLevel) * maxHours / 100)) * 60; //remaining time in minutes
        NSInteger hours = totMinutes / 60; //get hours
        NSInteger minutes = totMinutes - (hours * 60); // get minutes
        if (hours < 0 || minutes < 0) {
            return [NSString stringWithFormat:@"ND"];
        }
        return [NSString stringWithFormat:@"%i:%02i",(int)hours,(int)minutes];
    } else
        return @"NS";
}


+ (NSString *)remainingHoursForVideo
{
    CGFloat batteryLevel = [self batteryLevel];
    NSDictionary *info = [self infoForDevice];
    NSInteger maxHours = 0;
    if ([info objectForKey:@"video"]) {
        maxHours = [[info objectForKey:@"video"] integerValue];
        CGFloat totMinutes = (maxHours - ((100 - batteryLevel) * maxHours / 100)) * 60; //remaining time in minutes
        NSInteger hours = totMinutes / 60; // get hours
        NSInteger minutes = totMinutes - (hours *60); // get minutes
        if (hours < 0 || minutes < 0) {
            return [NSString stringWithFormat:@"ND"];
        }
        return [NSString stringWithFormat:@"%i:%02i",(int)hours,(int)minutes];
    } else
        return @"NS";
}


+ (NSString *)remainingHoursForAudio
{
    CGFloat batteryLevel = [self batteryLevel];
    NSDictionary *info = [self infoForDevice];
    NSInteger maxHours = 0;
    if ([info objectForKey:@"audio"]) {
        maxHours = [[info objectForKey:@"audio"] integerValue];
        CGFloat totMinutes = (maxHours - ((100 - batteryLevel) * maxHours / 100)) * 60; //remaining time in minutes
        NSInteger hours = totMinutes / 60; //get hours
        NSInteger minutes = totMinutes - (hours * 60); // get minutes
        if (hours < 0 || minutes < 0) {
            return [NSString stringWithFormat:@"ND"];
        }
        return [NSString stringWithFormat:@"%i:%02i",(int)hours,(int)minutes];
    } else
        return @"NS";
}


//=============================================
//          Hardware
//=============================================
#pragma mark -
#pragma mark - Hardware


+ (NSString *)deviceModel
{
    return [[UIDevice currentDevice] model];
}


+ (NSString *)deviceName
{
    return [[UIDevice currentDevice] name];
}


+ (NSString *)systemName
{
    return [[UIDevice currentDevice] systemName];
}


+ (NSString *)systemVersion
{
    return [[UIDevice currentDevice] systemVersion];
}


+ (NSInteger)screenWidth
{
    return [[UIScreen mainScreen] bounds].size.width;
}


+ (NSInteger)screenHeight
{
    return [[UIScreen mainScreen] bounds].size.height;
}


+ (CGFloat)screenBrightness
{
    return [[UIScreen mainScreen] brightness] * 100;
}


+ (NSString *)platformType
{
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *result = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    NSString *type;
    if ([result isEqualToString:@"i386"])           type = @"Simulator";
    if ([result isEqualToString:@"iPod3,1"])        type = @"iPod Touch 3";
    if ([result isEqualToString:@"iPod4,1"])        type = @"iPod Touch 4";
    if ([result isEqualToString:@"iPod5,1"])        type = @"iPod Touch 5";
    if ([result isEqualToString:@"iPhone2,1"])      type = @"iPhone 3Gs";
    if ([result isEqualToString:@"iPhone3,1"])      type = @"iPhone 4";
    if ([result isEqualToString:@"iPhone4,1"])      type = @"iPhone 4s";
    if ([result isEqualToString:@"iPhone5,1"]   ||
        [result isEqualToString:@"iPhone5,2"])      type = @"iPhone 5";
    if ([result isEqualToString:@"iPad2,1"]     ||
        [result isEqualToString:@"iPad2,2"]     ||
        [result isEqualToString:@"iPad2,3"])        type = @"iPad 2";
    if ([result isEqualToString:@"iPad3,1"]     ||
        [result isEqualToString:@"iPad3,2"]     ||
        [result isEqualToString:@"iPad3,3"])        type = @"iPad 3";
    if ([result isEqualToString:@"iPad3,4"]     ||
        [result isEqualToString:@"iPad3,5"]     ||
        [result isEqualToString:@"iPad3,6"])         type = @"iPad 4";
    if ([result isEqualToString:@"iPad4,1"]     ||
        [result isEqualToString:@"iPad4,2"])         type = @"iPad Air";
    if ([result isEqualToString:@"iPad2,5"]     ||
        [result isEqualToString:@"iPad2,6"]     ||
        [result isEqualToString:@"iPad2,7"])        type = @"iPad Mini";
    if ([result isEqualToString:@"iPad4,4"]     ||
        [result isEqualToString:@"iPad4,5"])        type = @"iPad Mini Retina";
    if ([result isEqualToString:@"iPhone6,1"]   ||
        [result isEqualToString:@"iPhone6,2"])      type = @"iPhone 5s";
    if ([result isEqualToString:@"iPhone5,3"]   ||
        [result isEqualToString:@"iPhone5,4"])      type = @"iPhone 5c";
    
    return type;
}


+ (NSString *)bootTime
{
    NSInteger ti = (NSInteger)[[NSProcessInfo processInfo] systemUptime];
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02i:%02i:%02i", (int)hours, (int)minutes, (int)seconds];
}


+ (BOOL)proximitySensor
{
    // Make a Bool for the proximity Sensor
    BOOL proximitySensor = NO;
    // Is the proximity sensor enabled?
    if ([[UIDevice currentDevice] respondsToSelector:@selector(setProximityMonitoringEnabled:)]) {
        // Create a UIDevice variable
        UIDevice *device = [UIDevice currentDevice];
        // Turn the sensor on, if not already on, and see if it works
        if (device.proximityMonitoringEnabled != YES) {
            // Sensor is off
            // Turn it on
            [device setProximityMonitoringEnabled:YES];
            // See if it turned on
            if (device.proximityMonitoringEnabled == YES) {
                // It turned on!  Turn it off
                [device setProximityMonitoringEnabled:NO];
                // It works
                proximitySensor = YES;
            } else {
                // Didn't turn on, no good
                proximitySensor = NO;
            }
        } else {
            // Sensor is already on
            proximitySensor = YES;
        }
    }
    // Return on or off
    return proximitySensor;
}


+ (BOOL)multitaskingEnabled
{
    // Is multitasking enabled?
    if ([[UIDevice currentDevice] respondsToSelector:@selector(isMultitaskingSupported)]) {
        // Create a bool
        BOOL multitaskingSupported = [UIDevice currentDevice].multitaskingSupported;
        // Return the value
        return multitaskingSupported;
    } else {
        // Doesn't respond to selector
        return NO;
    }
}


+ (NSString *)dimensions
{
    return [[self infoForDevice] objectForKey:@"dimensions"];
}


+ (NSString *)weight
{
    return [[self infoForDevice] objectForKey:@"weight"];
}


+ (NSString *)displayType
{
    return [[self infoForDevice] objectForKey:@"display-type"];
}


+ (NSString *)displayDensity
{
    return [[self infoForDevice] objectForKey:@"display-density"];
}


+ (NSString *)bluetoothVersion
{
    return [[self infoForDevice] objectForKey:@"bluetooth"];
}


+ (NSString *)cameraPrimaryDetail
{
    return [[self infoForDevice] objectForKey:@"camera-primary"];
}


+ (NSString *)cameraSecondaryDetail
{
    return [[self infoForDevice] objectForKey:@"camera-secondary"];
}


+ (NSString *)cpuDetail
{
    return [[self infoForDevice] objectForKey:@"cpu"];
}


+ (NSString *)gpuDetail
{
    return [[self infoForDevice] objectForKey:@"gpu"];
}


+ (BOOL)siri
{
    if ([[[self infoForDevice] objectForKey:@"siri"] isEqualToString:@"Yes"])
        return YES;
    else
        return NO;
}


+ (BOOL)touchID
{
    if ([[[self infoForDevice] objectForKey:@"touch-id"] isEqualToString:@"Yes"])
        return YES;
    else
        return NO;
}


+ (CGFloat)pixelToPoints:(CGFloat)px {
    CGFloat pointsPerInch = 72.0; // see: http://en.wikipedia.org/wiki/Point%5Fsize#Current%5FDTP%5Fpoint%5Fsystem
    CGFloat scale = [[UIScreen mainScreen] scale]; // We dont't use [[UIScreen mainScreen] scale] as we don't want the native pixel, we want pixels for UIFont - it does the retina scaling for us
    float pixelPerInch; // aka dpi
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        pixelPerInch = 132 * scale;
    } else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        pixelPerInch = 163 * scale;
    } else {
        pixelPerInch = 160 * scale;
    }
    CGFloat result = px * pointsPerInch / pixelPerInch;
    return result;
}


+ (CGFloat)pointsToPixel:(CGFloat)pt
{
    CGFloat pointsPerInch = 72.0; // see: http://en.wikipedia.org/wiki/Point%5Fsize#Current%5FDTP%5Fpoint%5Fsystem
    CGFloat scale = [[UIScreen mainScreen] scale]; // We dont't use [[UIScreen mainScreen] scale] as we don't want the native pixel, we want pixels for UIFont - it does the retina scaling for us
    float pixelPerInch; // aka dpi
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        pixelPerInch = 132 * scale;
    } else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        pixelPerInch = 163 * scale;
    } else {
        pixelPerInch = 160 * scale;
    }
    CGFloat result = pt * pixelPerInch / pointsPerInch;
    return result;
}


//=============================================
//          Disk
//=============================================
#pragma mark -
#pragma mark - Disk


+ (NSString *)memoryFormatter:(long long)diskSpace {
    NSString *formatted;
    double bytes = 1.0 * diskSpace;
    double megabytes = bytes / MB;
    double gigabytes = bytes / GB;
    double terabytes = bytes / TB;
    if (terabytes >= 1.0) {
        formatted = [NSString stringWithFormat:@"%.2f TB", terabytes];
    } else if (gigabytes >= 1.0)
        formatted = [NSString stringWithFormat:@"%.2f GB", gigabytes];
    else if (megabytes >= 1.0)
        formatted = [NSString stringWithFormat:@"%.2f MB", megabytes];
    else
        formatted = [NSString stringWithFormat:@"%.2f bytes", bytes];
    
    return formatted;
}


+ (NSString *)diskSpaceTotal
{
    long long space = [[[[NSFileManager defaultManager] attributesOfFileSystemForPath:NSHomeDirectory() error:nil] objectForKey:NSFileSystemSize] longLongValue];
    return [self memoryFormatter:space];
}


+ (NSString *)diskSpaceFree
{
    long long freeSpace = [[[[NSFileManager defaultManager] attributesOfFileSystemForPath:NSHomeDirectory() error:nil] objectForKey:NSFileSystemFreeSize] longLongValue];
    return [self memoryFormatter:freeSpace];
}


+ (NSString *)diskSpaceUse
{
    return [self memoryFormatter:[self diskSpaceUseInBytes]];
}


+ (CGFloat)diskSpaceTotalInBytes
{
    long long space = [[[[NSFileManager defaultManager] attributesOfFileSystemForPath:NSHomeDirectory() error:nil] objectForKey:NSFileSystemSize] longLongValue];
    return space;
}


+ (CGFloat)diskSpaceFreeInBytes
{
    long long freeSpace = [[[[NSFileManager defaultManager] attributesOfFileSystemForPath:NSHomeDirectory() error:nil] objectForKey:NSFileSystemFreeSize] longLongValue];
    return freeSpace;
}


+ (CGFloat)diskSpaceUseInBytes
{
    long long usedSpace = [self diskSpaceTotalInBytes] - [self diskSpaceFreeInBytes];
    return usedSpace;
}


//=============================================
//          Jailbeark
//=============================================
#pragma mark -
#pragma mark - Jailbeark


+ (BOOL)isJailbreak
{
    FILE *f = fopen("/bin/bash", "r");
    BOOL isJailbroken = NO;
    if (f != NULL)
        // Device is jailbroken
        isJailbroken = YES;
    else
        // Device isn't jailbroken
        isJailbroken = NO;
    
    fclose(f);
    
    return isJailbroken;
}


//=============================================
//          Accessory
//=============================================
#pragma mark -
#pragma mark - Accessory


+ (BOOL)isAccessoriesPluggedIn
{
    int accessoryCount = (int)[[[EAAccessoryManager sharedAccessoryManager] connectedAccessories] count];
    if (accessoryCount > 0)
        return YES;
    else
        return NO;
}


+ (NSInteger)numberOfAccessoriesPluggedIn {
    return [[[EAAccessoryManager sharedAccessoryManager] connectedAccessories] count];
}


+ (BOOL)isHeadphonesAttached
{
    AVAudioSessionRouteDescription *route = [[AVAudioSession sharedInstance] currentRoute];
    
    BOOL headphonesLocated = NO;
    for( AVAudioSessionPortDescription *portDescription in route.outputs )
    {
        headphonesLocated |= ( [portDescription.portType isEqualToString:AVAudioSessionPortHeadphones] );
    }
    return headphonesLocated;
}


//=============================================
//          Localization
//=============================================
#pragma mark -
#pragma mark - Localization


+ (NSString *)language {
    return [[NSLocale preferredLanguages] objectAtIndex:0];
}


+ (NSString *)timeZone {
    return [[NSTimeZone systemTimeZone] name];
}


+ (NSString *)currencySymbol {
    return [[NSLocale currentLocale] objectForKey:NSLocaleCurrencySymbol];
}


+ (NSString *)currencyCode {
    return [[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode];
}


+ (NSString *)country {
    return [[NSLocale currentLocale] localeIdentifier];
}


+ (NSString *)measurementSystem {
    return [[NSLocale currentLocale] objectForKey:NSLocaleMeasurementSystem];
}


//=============================================
//          Memory
//=============================================
#pragma mark -
#pragma mark - Memory


+ (NSInteger)memoryTotal
{
    int nearest = 256;
    int totalMemory = (int)([[NSProcessInfo processInfo] physicalMemory] / 1024 / 1024);
    int rem = (int)totalMemory % nearest;
    int tot = 0;
    if (rem >= nearest/2) {
        tot = ((int)totalMemory - rem)+256;
    } else {
        tot = ((int)totalMemory - rem);
    }
    
    return tot;
}


+ (CGFloat)memoryFree
{
    double totalMemory = 0.00;
    vm_statistics_data_t vmStats;
    mach_msg_type_number_t infoCount = HOST_VM_INFO_COUNT;
    kern_return_t kernReturn = host_statistics(mach_host_self(), HOST_VM_INFO, (host_info_t)&vmStats, &infoCount);
    if(kernReturn != KERN_SUCCESS) {
        return -1;
    }
    totalMemory = ((vm_page_size * vmStats.free_count) / 1024) / 1024;
    
    return totalMemory;
}


+ (CGFloat)memoryUsed
{
    double usedMemory = 0.00;
    vm_statistics_data_t vmStats;
    mach_msg_type_number_t infoCount = HOST_VM_INFO_COUNT;
    kern_return_t kernReturn = host_statistics(mach_host_self(), HOST_VM_INFO, (host_info_t)&vmStats, &infoCount);
    if(kernReturn != KERN_SUCCESS) {
        return -1;
    }
    usedMemory = ((vm_page_size * (vmStats.active_count + vmStats.inactive_count + vmStats.wire_count)) / 1024) / 1024;
    
    return usedMemory;
}


+ (CGFloat)memoryActived
{
    double activeMemory = 0.00;
    vm_statistics_data_t vmStats;
    mach_msg_type_number_t infoCount = HOST_VM_INFO_COUNT;
    kern_return_t kernReturn = host_statistics(mach_host_self(), HOST_VM_INFO, (host_info_t)&vmStats, &infoCount);
    if(kernReturn != KERN_SUCCESS) {
        return -1;
    }
    activeMemory = ((vm_page_size * vmStats.active_count) / 1024) / 1024;
    
    return activeMemory;
}


+ (CGFloat)memoryWired
{
    double wiredMemory = 0.00;
    vm_statistics_data_t vmStats;
    mach_msg_type_number_t infoCount = HOST_VM_INFO_COUNT;
    kern_return_t kernReturn = host_statistics(mach_host_self(), HOST_VM_INFO, (host_info_t)&vmStats, &infoCount);
    if(kernReturn != KERN_SUCCESS) {
        return -1;
    }
    wiredMemory = ((vm_page_size * vmStats.wire_count) / 1024) / 1024;
    
    return wiredMemory;
}


+ (CGFloat)memoryInactive
{
    double inactiveMemory = 0.00;
    vm_statistics_data_t vmStats;
    mach_msg_type_number_t infoCount = HOST_VM_INFO_COUNT;
    kern_return_t kernReturn = host_statistics(mach_host_self(), HOST_VM_INFO, (host_info_t)&vmStats, &infoCount);
    if(kernReturn != KERN_SUCCESS) {
        return -1;
    }
    inactiveMemory = ((vm_page_size * vmStats.inactive_count) / 1024) / 1024;
    
    return inactiveMemory;
}


//=============================================
//          Processor
//=============================================
#pragma mark -
#pragma mark - Processor


+ (NSInteger)processorsNumber
{
    return [[NSProcessInfo processInfo] processorCount];
}


+ (NSInteger)processorsActiveNumber
{
    return [[NSProcessInfo processInfo] activeProcessorCount];
}


+ (CGFloat)cpuUsageForApp
{
    kern_return_t kr;
    task_info_data_t tinfo;
    mach_msg_type_number_t task_info_count;
    
    task_info_count = TASK_INFO_MAX;
    kr = task_info(mach_task_self(), TASK_BASIC_INFO, (task_info_t)tinfo, &task_info_count);
    if (kr != KERN_SUCCESS)
        return -1;
    
    task_basic_info_t      basic_info;
    thread_array_t         thread_list;
    mach_msg_type_number_t thread_count;
    thread_info_data_t     thinfo;
    mach_msg_type_number_t thread_info_count;
    thread_basic_info_t basic_info_th;
    uint32_t stat_thread = 0; // Mach threads
    
    basic_info = (task_basic_info_t)tinfo;
    
    // get threads in the task
    kr = task_threads(mach_task_self(), &thread_list, &thread_count);
    if (kr != KERN_SUCCESS)
        return -1;
    if (thread_count > 0)
        stat_thread += thread_count;
    
    long tot_sec = 0;
    long tot_usec = 0;
    float tot_cpu = 0;
    int j;
    
    for (j = 0; j < thread_count; j++) {
        thread_info_count = THREAD_INFO_MAX;
        kr = thread_info(thread_list[j], THREAD_BASIC_INFO,
                         (thread_info_t)thinfo, &thread_info_count);
        if (kr != KERN_SUCCESS)
            return -1;
        
        basic_info_th = (thread_basic_info_t)thinfo;
        if (!(basic_info_th->flags & TH_FLAGS_IDLE)) {
            tot_sec = tot_sec + basic_info_th->user_time.seconds + basic_info_th->system_time.seconds;
            tot_usec = tot_usec + basic_info_th->system_time.microseconds + basic_info_th->system_time.microseconds;
            tot_cpu = tot_cpu + basic_info_th->cpu_usage / (float)TH_USAGE_SCALE;
        }
        
    } // for each thread
    
    return tot_cpu;
}


+ (NSArray *)processesActive
{
    int mib[4] = {CTL_KERN, KERN_PROC, KERN_PROC_ALL, 0};
    size_t miblen = 4;
    size_t size;
    int st = sysctl(mib, (u_int)miblen, NULL, &size, NULL, 0);
    struct kinfo_proc * process = NULL;
    struct kinfo_proc * newprocess = NULL;
    do {
        
        size += size / 10;
        newprocess = realloc(process, size);
        if (!newprocess) {
            if (process) {
                free(process);
            }
            return nil;
        }
        process = newprocess;
        st = sysctl(mib, (u_int)miblen, process, &size, NULL, 0);
        
    } while (st == -1 && errno == ENOMEM);
    
    if (st == 0) {
        if (size % sizeof(struct kinfo_proc) == 0){
            int nprocess = (int)size / sizeof(struct kinfo_proc);
            if (nprocess) {
                NSMutableArray * array = [[NSMutableArray alloc] init];
                for (int i = nprocess - 1; i >= 0; i--) {
                    NSString * processID = [[NSString alloc] initWithFormat:@"%d", process[i].kp_proc.p_pid];
                    NSString * processName = [[NSString alloc] initWithFormat:@"%s", process[i].kp_proc.p_comm];
                    NSDictionary * dict = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:processID, processName, nil]
                                                                        forKeys:[NSArray arrayWithObjects:@"ProcessID", @"ProcessName", nil]];
                    [array addObject:dict];
                }
                free(process);
                return array;
            }
        }
    }
    
    return nil;
}


+ (NSInteger)processesActiveNumber
{
    return [[self processesActive] count];
}

@end
