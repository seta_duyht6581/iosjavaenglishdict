//
//  UIDevice+extend
//
//  Created by viettel on 7/10/14.
//  Copyright (c) 2014 ChuongPD. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    GREATER_THAN_7,
    IOS7,
    IOS6,
    IOS5,
    LESS_THAN_5,
    UNKNOWN
} IOS_VERSION;

@interface UIDevice (extend)
- (NSString* ) getModel;
- (CGSize) getScreenSize;
- (BOOL) isPad;
- (IOS_VERSION) getIOSVersion;

@end
