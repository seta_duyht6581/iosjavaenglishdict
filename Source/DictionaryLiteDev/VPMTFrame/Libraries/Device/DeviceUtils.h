//
//  DeviceUtils.h
//  ViettelPI
//
//  Created by Trinh Duy Nhan on 4/1/14.
//  Copyright (c) 2014 viettel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeviceUtils : NSObject

//=============================================
//          Battery
//=============================================


/**Kiểm tra pin của thiết bị đã được nạp đầy hay không.
 @author    nhantd
 @return    BOOL YES nếu thiết bị đã được nạp đầy pin. No nếu ngược lại.
 */
+ (BOOL)batteryFullCharged;


/**Kiểm tra xem thiết bị có đang được nạp pin hay không.
 @author    nhantd
 @return    BOOL YES nếu thiết bị đang được nạp pin. No nếu ngược lại.
 */
+ (BOOL)batteryInCharge;


/**Kiểm tra xem thiết bị có đang được cắm vào nguồn hay không.
 @author    nhantd
 @return    BOOL YES nếu thiết bị đang được cắm vào nguồn. No nếu ngược lại.
 */
+ (BOOL)devicePluggedIntoPower;


/**Lấy trạng thái pin hiện tại của thiết bị.
 @author    nhantd
 @return    UIDeviceBatteryState Giá trị đại diện cho trạng thái của pin.
 */
+ (UIDeviceBatteryState)batteryState;


/**Lấy dung lượng của pin.
 @author    nhantd
 @return    CGFloat giá trị đại diện cho mức pin hiện tại.
 */
+ (CGFloat)batteryLevel;


/**Lấy số giờ sử dụng còn lại của thiết bị ở trạng thái bình thường.
 @author    nhantd
 @return    NSString giá trị đại diện số giờ còn lại.
 */
+ (NSString *)remainingHoursForStandby;


/**Lấy số giờ sử dụng còn lại của thiết bị ở trạng thái trò chuyện sử dụng 3G.
 @author    nhantd
 @return    NSString giá trị đại diện số giờ còn lại.
 */
+ (NSString *)remainingHoursFor3gConversation;


/**Lấy số giờ sử dụng còn lại của thiết bị ở trạng thái internet sử dụng 3G.
 @author    nhantd
 @return    NSString giá trị đại diện số giờ còn lại.
 */
+ (NSString *)remainingHoursForInternet3g;


/**Lấy số giờ sử dụng còn lại của thiết bị ở trạng thái internet sử dụng Wifi.
 @author    nhantd
 @return    NSString giá trị đại diện số giờ còn lại.
 */
+ (NSString *)remainingHoursForInternetWiFi;


/**Lấy số giờ sử dụng còn lại của thiết bị ở trạng thái xem video.
 @author    nhantd
 @return    NSString giá trị đại diện số giờ còn lại.
 */
+ (NSString *)remainingHoursForVideo;


/**Lấy số giờ sử dụng còn lại của thiết bị ở trạng thái nghe audio.
 @author    nhantd
 @return    NSString giá trị đại diện số giờ còn lại.
 */
+ (NSString *)remainingHoursForAudio;


//=============================================
//          Hardware
//=============================================


/**Kiểm tra device model.
 @author    nhantd
 @return    NSString device model.
 */
+ (NSString *)deviceModel;


/**Kiểm tra system name.
 @author    nhantd
 @return    NSString system name.
 */
+ (NSString *)systemName;


/**Kiểm tra system version.
 @author    nhantd
 @return    NSString system version.
 */
+ (NSString *)systemVersion;


/**Kiểm tra độ rộng của screen dưới dạng pixel.
 @author    nhantd
 @return    NSInteger Giá trị độ rộng của screen.
 */
+ (NSInteger)screenWidth;


/**Kiểm tra độ dài của screen dưới dạng pixel.
 @author    nhantd
 @return    NSInteger Giá trị độ dài của screen.
 */
+ (NSInteger)screenHeight;


/**Kiểm tra độ sáng của screen.
 @author    nhantd
 @return    CGFloat Giá trị độ sáng của screen.
 */
+ (CGFloat)screenBrightness;


/**Kiểm tra device type.
 @author    nhantd
 @return    NSString Giá trị thể hiện platform type (ex : iPhone 5).
 */
+ (NSString *)platformType;


/**Lấy thời gian boot time bằng giờ, phút, giây.
 @author    nhantd
 @return    NSDate Giá trị thể hiện boot time.
 */
+ (NSDate *)bootTime;


/**Kiểm tra thiết bị (cùng loại hoặc cùng hãng sản xuất Apple) hoạt động gần đấy.
 @author    nhantd
 @return    BOOL YES nếu có thiết bị hoạt động gần đấy. NO nếu ngược lại.
 */
+ (BOOL)proximitySensor;


/**Kiểm tra thiết bị có đang kích hoạt multitasking hay không.
 @author    nhantd
 @return    BOOL YES nếu thiết bị có kích hoạt multitasking. NO nếu ngược lại.
 */
+ (BOOL)multitaskingEnabled;


/**Lấy kích thước của thiết bị.
 @author    nhantd
 @return    NSString Giá trị thể hiện kích thước của thiết bị.
 */
+ (NSString *)dimensions;


/**Lấy cân nặng của thiết bị.
 @author    nhantd
 @return    NSString Giá trị thể hiện cân nặng của thiết bị.
 */
+ (NSString *)weight;


/**Lấy loại màn hình của thiết bị.
 @author    nhantd
 @return    NSString Giá trị thể hiện loại màn hình của thiết bị.
 */
+ (NSString *)displayType;


/**Lấy mật độ điểm ảnh màn hình của thiết bị.
 @author    nhantd
 @return    NSString Giá trị thể hiện mật độ điểm ảnh màn hình của thiết bị.
 */
+ (NSString *)displayDensity;


/**Lấy version bluetooth của thiết bị.
 @author    nhantd
 @return    NSString Giá trị thể hiện version bluetooth của thiết bị.
 */
+ (NSString *)bluetoothVersion;


/**Lấy thông tin về camera chính của thiết bị.
 @author    nhantd
 @return    NSString Giá trị thể hiện thông tin về camera chính của thiết bị.
 */
+ (NSString *)cameraPrimaryDetail;


/**Lấy thông tin về camera phụ của thiết bị.
 @author    nhantd
 @return    NSString Giá trị thể hiện thông tin về camera phụ của thiết bị.
 */
+ (NSString *)cameraSecondaryDetail;


/**Lấy thông tin về cpu của thiết bị.
 @author    nhantd
 @return    NSString Giá trị thể hiện thông tin về cpu của thiết bị.
 */
+ (NSString *)cpuDetail;


/**Lấy thông tin về gpu của thiết bị.
 @author    nhantd
 @return    NSString Giá trị thể hiện thông tin về gpu của thiết bị.
 */
+ (NSString *)gpuDetail;


/**Kiểm tra Siri trên thiết bị.
 @author    nhantd
 @return    BOOL YES nếu Siri có trên thiết bị. NO nếu ngược lại.
 */
+ (BOOL)siri;


/**Kiểm tra touchID trên thiết bị.
 @author    nhantd
 @return    BOOL YES nếu TouchID có trên thiết bị. NO nếu ngược lại.
 */
+ (BOOL)touchID;


/**Chuyển đổi pixel thành points.
 @author    nhantd
 @return    CGFloat points.
 */
+ (CGFloat)pixelToPoints:(CGFloat)px;


/**Chuyển đổi points thành pixel.
 @author    nhantd
 @return    CGFloat pixels.
 */
+ (CGFloat)pointsToPixel:(CGFloat)pt;


//=============================================
//          Disk
//=============================================


/**Lấy tổng dung lượng disk của thiết bị.
 @author    nhantd
 @return    NSString Giá trị thể hiện tổng dung lượng disk của thiết bị.
 */
+ (NSString *)diskSpaceTotal;


/**Lấy dung lượng disk còn trống của thiết bị.
 @author    nhantd
 @return    NSString Giá trị thể hiện dung lượng disk còn trống của thiết bị.
 */
+ (NSString *)diskSpaceFree;


/**Lấy dung lượng disk đang sử dụng của thiết bị.
 @author    nhantd
 @return    NSString Giá trị thể hiện dung lượng disk đang sử dụng của thiết bị.
 */
+ (NSString *)diskSpaceUse;


/**Lấy tổng dung lượng disk tính bằng bytes của thiết bị.
 @author    nhantd
 @return    CGFloat Giá trị thể hiện tổng dung lượng disk tính bằng bytes của thiết bị.
 */
+ (CGFloat)diskSpaceTotalInBytes;


/**Lấy dung lượng disk còn trống tính bằng bytes của thiết bị.
 @author    nhantd
 @return    long long Giá trị thể hiện dung lượng disk còn trống tính bằng bytes của thiết bị.
 */
+ (CGFloat)diskSpaceFreeInBytes;


/**Lấy dung lượng disk đang sử dụng tính bằng bytes của thiết bị.
 @author    nhantd
 @return    CGFloat Giá trị thể hiện dung lượng disk đang sử dụng tính bằng bytes của thiết bị.
 */
+ (CGFloat)diskSpaceUseInBytes;


//=============================================
//          Jailbeark
//=============================================


/**Kiểm tra thiết bị đã bị jailbroken hay chưa.
 @author    nhantd
 @return    YES nếu thiết bị đã bị jailbroken. NO nếu ngược lại.
 */
+ (BOOL)isJailbreak;


//=============================================
//          Accessory
//=============================================


/**Kiểm tra thiết bị có được cắm phụ kiện nào vào hay chưa.
 @author    nhantd
 @return    YES nếu thiết bị có cắm phụ kiện. NO nếu ngược lại.
 */
+ (BOOL)isAccessoriesPluggedIn;


/**Lấy số lượng phụ kiện cắm vào thiết bị.
 @author    nhantd
 @return    NSInteger Số lượng phụ kiện cắm vào thiết bị.
 */
+ (NSInteger)numberOfAccessoriesPluggedIn;


/**Kiểm tra thiết bị có cắm headphone hay không.
 @author    nhantd
 @return    YES nếu thiết bị có cắm headphone. NO nếu ngược lại.
 */
+ (BOOL)isHeadphonesAttached;


//=============================================
//          Localization
//=============================================


/**Lấy ngôn ngữ của hệ thống.
 @author    nhantd
 @return    NSString ngôn ngữ của hệ thống.
 */
+ (NSString *)language;


/**Lấy time zone của hệ thống.
 @author    nhantd
 @return    NSString time zone của hệ thống.
 */
+ (NSString *)timeZone;


/**Lấy ký hiệu tiền tệ của hệ thống.
 @author    nhantd
 @return    NSString ký hiệu tiền tệ của hệ thống.
 */
+ (NSString *)currencySymbol;


/**Lấy mã tiền tệ của hệ thống.
 @author    nhantd
 @return    NSString ký hiệu mã tiền tệ của hệ thống.
 */
+ (NSString *)currencyCode;


/**Lấy country của hệ thống.
 @author    nhantd
 @return    NSString country của hệ thống.
 */
+ (NSString *)country;


/**Lấy giá trị đo lường của hệ thống.
 @author    nhantd
 @return    NSString giá trị đọ lường của hệ thống.
 */
+ (NSString *)measurementSystem;


//=============================================
//          Memory
//=============================================


/**Kiểm tra tổng dung lượng bộ nhớ của thiết bị.
 @author    nhantd
 @return    NSInteger giá trị tổng dung lượng bộ nhớ của thiết bị.
 */
+ (NSInteger)memoryTotal;


/**Kiểm tra dung lượng bộ nhớ còn trống của thiết bị.
 @author    nhantd
 @return    CGFloat giá trị dung lượng bộ nhớ còn trống của thiết bị.
 */
+ (CGFloat)memoryFree;


/**Kiểm tra dung lượng bộ nhớ đang sử dụng của thiết bị.
 @author    nhantd
 @return    NSString giá trị dung lượng bộ nhớ đang sử dụng của thiết bị.
 */
+ (CGFloat)memoryUsed;


/**Kiểm tra dung lượng bộ nhớ active của thiết bị.
 @author    nhantd
 @return    CGFloat giá trị dung lượng bộ nhớ active của thiết bị.
 */
+ (CGFloat)memoryActived;


/**Kiểm tra dung lượng bộ nhớ wired của thiết bị.
 @author    nhantd
 @return    CGFloat giá trị dung lượng bộ nhớ wired của thiết bị.
 */
+ (CGFloat)memoryWired;


/**Kiểm tra dung lượng bộ nhớ inactive của thiết bị.
 @author    nhantd
 @return    CGFloat giá trị dung lượng bộ nhớ inactive của thiết bị.
 */
+ (CGFloat)memoryInactive;


//=============================================
//          Processor
//=============================================


/**Kiểm tra số lượng processors.
 @author    nhantd
 @return    NSInteger số lượng processor.
 */
+ (NSInteger)processorsNumber;


/**Kiểm tra số lượng processors active.
 @author    nhantd
 @return    NSInteger số lượng processor active.
 */
+ (NSInteger)processorsActiveNumber;


/**Kiểm tra cpu usage cho ứng dụng.
 @author    nhantd
 @return    CGFloat giá trị thể hiển cpu usage cho ứng dụng.
 */
+ (CGFloat)cpuUsageForApp;


/**Danh sách processes active.
 @author    nhantd
 @return    NSArray danh sách processes active.
 */
+ (NSArray *)processesActive;


/**Số lượng processes active.
 @author    nhantd
 @return    NSInteger số lượng processes active.
 */
+ (NSInteger)processesActiveNumber;


@end
