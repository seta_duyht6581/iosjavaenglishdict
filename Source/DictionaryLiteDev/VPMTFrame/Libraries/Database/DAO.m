//
//  DAO.m
//  Smart-i
//
//  Created by Tony Nguyen on 5/15/12.
//  Copyright (c) 2012 self. All rights reserved.
//

#import "DAO.h"
#import "SSZipArchive.h"
@implementation DAO

static sqlite3* __OttoDB = NULL;

+ (sqlite3*)OttoDB {
    return __OttoDB;
}

+ (BOOL)unzipDB
{
    //zip -er database.sqlite.zip database.sqlite terminal
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDir = [paths objectAtIndex:0];
    
    NSString *databasePath = [documentsDir stringByAppendingPathComponent:DATAbase_full_name];
    NSLog(@"databasePath:[%@] ",databasePath);
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    BOOL fexist = [filemgr fileExistsAtPath:databasePath];

    if (fexist)
    {
        //copied
        return true;
    }
    else
    {
        Boolean success=false;
        NSString* srcPath = [[NSBundle mainBundle]pathForResource:DATAbase_full_name   ofType:@"zip"];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *destPath = [paths objectAtIndex:0];
        
        NSError *error;
        success = [SSZipArchive unzipFileAtPath:srcPath toDestination:destPath overwrite:NO password:PASS_ZIP_DATABASE error:&error];
        
        return success;
        
    }
    return FALSE;
}
+(BOOL)openDB
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDir = [paths objectAtIndex:0];
    
    NSString *databasePath = [documentsDir stringByAppendingPathComponent:DATAbase_full_name];
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    BOOL fexist = [filemgr fileExistsAtPath:databasePath];
    if (fexist)
    {
        const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &__OttoDB) == SQLITE_OK)
        {
            return TRUE;
        }
        
    }
    else 
    {
        Boolean success=false;
        NSString* srcPath = [[NSBundle mainBundle]pathForResource:DATAbase_name ofType:DATAbase_type];
        NSArray* arrayPathComp = [NSArray arrayWithObjects:NSHomeDirectory(),@"Documents",DATAbase_full_name, nil];
        
        NSString* destPath = [NSString pathWithComponents:arrayPathComp];
      
        NSFileManager *manager = [NSFileManager defaultManager];
         NSError *error;
        if ([manager fileExistsAtPath:destPath]!=YES) {
           
            success = [manager copyItemAtPath:srcPath toPath:destPath error:&error];
            if (!success)
            {        NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
                return FALSE;
            }
            else
            {
                const char *dbpath = [databasePath UTF8String];
                if (sqlite3_open(dbpath, &__OttoDB) == SQLITE_OK)
                {
                    return TRUE;
                }
            }
        }
        
        
        
    }
    return FALSE;
}

+(BOOL) copyDB:(NSString *) databasePath fileManager: (NSFileManager *) filemgr
{
    @try 
    {

        NSError *error;
        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DATAbase_name];
        [filemgr removeItemAtPath:databasePath error:&error];
        BOOL success = [filemgr copyItemAtPath:defaultDBPath toPath:databasePath error:&error];
    
        if (!success) 
        {        
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
            return FALSE;
        }
        else
        {
            return TRUE;
        } 
    }

    @catch (NSException *exception) 
    {
        
    }

    return FALSE;
}

+(void)closeDB
{
    sqlite3_close(__OttoDB);
    __OttoDB = NULL;
}

@end
