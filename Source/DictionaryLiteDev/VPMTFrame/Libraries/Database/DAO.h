//
//  DAO.h
//  Smart-i
//
//  Created by Tuan Nguyen on 5/15/12.
//  Copyright (c) 2012 self. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"
#define DATAbase_name @"database"
#define DATAbase_type @"sqlite"
#define DATAbase_full_name @"database.sqlite"
#define DATAbase_full_name_zip @"database.sqlite.zip"
@interface DAO : NSObject {
}

+ (sqlite3*)OttoDB;

+(BOOL)openDB;
+(void)closeDB;
+(BOOL) copyDB:(NSString *) databasePath fileManager: (NSFileManager *) filemgr;
+ (BOOL)unzipDB;
@end
