//
//  NetworkHelper
//
//  Created by viettel on 7/10/14.
//  Copyright (c) 2014 ChuongPD. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface Network: NSObject
    @property (nonatomic, strong) Reachability *reachability;
    @property (readonly, nonatomic) BOOL NETWORK_AVAIABLE;

- (BOOL)isNetworkAvaiable;
- (NSString *)getIPAddress;
+ (instancetype)sharedManager;
@end
