#import "AFURLRequestSerialization.h"

@interface TimeoutAFHTTPRequestSerializer : AFJSONRequestSerializer

@property (nonatomic, assign) NSTimeInterval timeout;

- (id)initWithTimeout:(NSTimeInterval)timeout;

@end