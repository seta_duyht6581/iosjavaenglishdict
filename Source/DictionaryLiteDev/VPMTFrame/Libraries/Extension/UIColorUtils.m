//
//  UIColor+Utils.m
//  ViettelPI
//
//  Created by Trinh Duy Nhan on 4/4/14.
//  Copyright (c) 2014 viettel. All rights reserved.
//

#import "UIColorUtils.h"

@implementation UIColor (Utils)


+ (UIColor *) colorFromHexString:(NSString *)hexString {
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                       [cleanString substringWithRange:NSMakeRange(0, 1)],[cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],[cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)],[cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    
    float red = ((baseValue >> 24) & 0xFF)/255.0f;
    float green = ((baseValue >> 16) & 0xFF)/255.0f;
    float blue = ((baseValue >> 8) & 0xFF)/255.0f;
    float alpha = ((baseValue >> 0) & 0xFF)/255.0f;
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}


+ (instancetype)aliceBlueColor
{
    return [[self class] colorFromHexString:@"F0F8FF"];
}


+ (instancetype)antiqueWhiteColor
{
    return [[self class] colorFromHexString:@"FAEBD7"];
}


+ (instancetype)aquaColor
{
    return [[self class] colorFromHexString:@"00FFFF"];
}


+ (instancetype)aquamarineColor
{
    return [[self class] colorFromHexString:@"7FFFD4"];
}


+ (instancetype)azureColor
{
    return [[self class] colorFromHexString:@"F0FFFF"];
}


+ (instancetype)beigeColor
{
    return [[self class] colorFromHexString:@"F5F5DC"];
}


+ (instancetype)bisqueColor
{
    return [[self class] colorFromHexString:@"FFE4C4"];
}


+ (instancetype)blanchedAlmondColor
{
    return [[self class] colorFromHexString:@"FFEBDC"];
}


+ (instancetype)blueVioletColor
{
    return [[self class] colorFromHexString:@"8A2BE2"];
}


+ (instancetype)burlyWoodColor
{
    return [[self class] colorFromHexString:@"DEB887"];
}


+ (instancetype)cadetBlueColor
{
    return [[self class] colorFromHexString:@"5F9EA0"];
}


+ (instancetype)chartreuseColor
{
    return [[self class] colorFromHexString:@"7FFF00"];
}


+ (instancetype)chocolateColor
{
    return [[self class] colorFromHexString:@"D2691E"];
}


+ (instancetype)coralColor
{
    return [[self class] colorFromHexString:@"FF7F50"];
}


+ (instancetype)cornflowerBlueColor
{
    return [[self class] colorFromHexString:@"6495ED"];
}


+ (instancetype)cornsilkColor
{
    return [[self class] colorFromHexString:@"FFF8DC"];
}


+ (instancetype)crimsonColor
{
    return [[self class] colorFromHexString:@"DC143C"];
}


+ (instancetype)darkBlueColor
{
    return [[self class] colorFromHexString:@"00008B"];
}


+ (instancetype)darkCyanColor
{
    return [[self class] colorFromHexString:@"008B8B"];
}


+ (instancetype)darkGoldenRodColor
{
    return [[self class] colorFromHexString:@"B8860B"];
}


+ (instancetype)darkGreenColor
{
    return [[self class] colorFromHexString:@"006400"];
}


+ (instancetype)darkKhakiColor
{
    return [[self class] colorFromHexString:@"BDB76B"];
}


+ (instancetype)darkMagentaColor
{
    return [[self class] colorFromHexString:@"8B008B"];
}


+ (instancetype)darkOliveGreenColor
{
    return [[self class] colorFromHexString:@"556B2F"];
}


+ (instancetype)darkOrangeColor
{
    return [[self class] colorFromHexString:@"FF8C00"];
}


+ (instancetype)darkOrchidColor
{
    return [[self class] colorFromHexString:@"9932CC"];
}



+ (instancetype)darkRedColor
{
    return [[self class] colorFromHexString:@"8B0000"];
}


+ (instancetype)darkSalmonColor
{
    return [[self class] colorFromHexString:@"E9967A"];
}


+ (instancetype)darkSeaGreenColor
{
    return [[self class] colorFromHexString:@"8FBC8F"];
}


+ (instancetype)darkSlateBlueColor
{
    return [[self class] colorFromHexString:@"483D8B"];
}


+ (instancetype)darkSlateGrayColor
{
    return [[self class] colorFromHexString:@"2F4F4F"];
}


+ (instancetype)darkTurquoiseColor
{
    return [[self class] colorFromHexString:@"00CED1"];
}


+ (instancetype)darkVioletColor
{
    return [[self class] colorFromHexString:@"9400D3"];
}


+ (instancetype)deepPinkColor
{
    return [[self class] colorFromHexString:@"FF1493"];
}


+ (instancetype)deepSkyBlueColor
{
    return [[self class] colorFromHexString:@"00BFFF"];
}


+ (instancetype)dimGrayColor
{
    return [[self class] colorFromHexString:@"696969"];
}


+ (instancetype)dodgerBlueColor
{
    return [[self class] colorFromHexString:@"1E90FF"];
}


+ (instancetype)fireBrickColor
{
    return [[self class] colorFromHexString:@"B22222"];
}


+ (instancetype)floralWhiteColor
{
    return [[self class] colorFromHexString:@"FFFAF0"];
}


+ (instancetype)forestGreenColor
{
    return [[self class] colorFromHexString:@"228B22"];
}


+ (instancetype)fuchsiaColor
{
    return [[self class] colorFromHexString:@"FF00FF"];
}


+ (instancetype)gainsboroColor
{
    return [[self class] colorFromHexString:@"DCDCDC"];
}


+ (instancetype)ghostwhiteColor
{
    return [[self class] colorFromHexString:@"F8F8FF"];
}


+ (instancetype)goldColor
{
    return [[self class] colorFromHexString:@"FFD700"];
}


+ (instancetype)goldenRodColor
{
    return [[self class] colorFromHexString:@"DAA520"];
}


+ (instancetype)greenYellowColor
{
    return [[self class] colorFromHexString:@"ADFF2F"];
}


+ (instancetype)honeyDewColor
{
    return [[self class] colorFromHexString:@"F0FFF0"];
}


+ (instancetype)hotPinkColor
{
    return [[self class] colorFromHexString:@"FF69B4"];
}


+ (instancetype)indianRedColor
{
    return [[self class] colorFromHexString:@"CD5C5C"];
}


+ (instancetype)indigoColor
{
    return [[self class] colorFromHexString:@"4B0082"];
}


+ (instancetype)ivoryColor
{
    return [[self class] colorFromHexString:@"FFFFF0"];
}


+ (instancetype)khakiColor
{
    return [[self class] colorFromHexString:@"F0E68C"];
}


+ (instancetype)lavenderColor
{
    return [[self class] colorFromHexString:@"E6E6FA"];
}


+ (instancetype)lavenderBlushColor
{
    return [[self class] colorFromHexString:@"FFF0F5"];
}


+ (instancetype)lawnGreenColor
{
    return [[self class] colorFromHexString:@"7CFC00"];
}


+ (instancetype)lemonChiffonColor
{
    return [[self class] colorFromHexString:@"FFFACD"];
}


+ (instancetype)lightBlueColor
{
    return [[self class] colorFromHexString:@"ADD8E6"];
}


+ (instancetype)lightCoralColor
{
    return [[self class] colorFromHexString:@"F08080"];
}


+ (instancetype)lightCyanColor
{
    return [[self class] colorFromHexString:@"E0FFFF"];
}


+ (instancetype)lightGoldenRodYellowColor
{
    return [[self class] colorFromHexString:@"FAFAD2"];
}


+ (instancetype)lightGreenColor
{
    return [[self class] colorFromHexString:@"90EE90"];
}


+ (instancetype)lightPinkColor
{
    return [[self class] colorFromHexString:@"FFB6C1"];
}


+ (instancetype)lightSalmonColor
{
    return [[self class] colorFromHexString:@"FFA07A"];
}


+ (instancetype)lightSeaGreenColor
{
    return [[self class] colorFromHexString:@"20B2AA"];
}


+ (instancetype)lightSkyBlueColor
{
    return [[self class] colorFromHexString:@"87CEFA"];
}


+ (instancetype)lightSlateGrayColor
{
    return [[self class] colorFromHexString:@"778899"];
}


+ (instancetype)lightSteelBlueColor
{
    return [[self class] colorFromHexString:@"B0C4DE"];
}


+ (instancetype)lightYellowColor
{
    return [[self class] colorFromHexString:@"FFFFF0"];
}


+ (instancetype)limeColor
{
    return [[self class] colorFromHexString:@"00FF00"];
}


+ (instancetype)limeGreenColor
{
    return [[self class] colorFromHexString:@"32CD32"];
}


+ (instancetype)linenColor
{
    return [[self class] colorFromHexString:@"FAF0E6"];
}


+ (instancetype)maroonColor
{
    return [[self class] colorFromHexString:@"800000"];
}


+ (instancetype)mediumAquaMarineColor
{
    return [[self class] colorFromHexString:@"66CDAA"];
}


+ (instancetype)mediumBlueColor
{
    return [[self class] colorFromHexString:@"0000CD"];
}


+ (instancetype)mediumOrchidColor
{
    return [[self class] colorFromHexString:@"BA55D3"];
}


+ (instancetype)mediumPurpleColor
{
    return [[self class] colorFromHexString:@"9370DB"];
}


+ (instancetype)mediumSeaGreenColor
{
    return [[self class] colorFromHexString:@"3CB371"];
}


+ (instancetype)mediumSlateBlueColor
{
    return [[self class] colorFromHexString:@"7B68EE"];
}


+ (instancetype)mediumSpringGreenColor
{
    return [[self class] colorFromHexString:@"00FA9A"];
}


+ (instancetype)mediumTurquoiseColor
{
    return [[self class] colorFromHexString:@"48D1CC"];
}


+ (instancetype)mediumVioletRedColor
{
    return [[self class] colorFromHexString:@"C71585"];
}


+ (instancetype)midnightBlueColor
{
    return [[self class] colorFromHexString:@"191970"];
}


+ (instancetype)mintCreamColor
{
    return [[self class] colorFromHexString:@"F5FFFA"];
}


+ (instancetype)mistyRoseColor
{
    return [[self class] colorFromHexString:@"FFE4E1"];
}


+ (instancetype)moccasinColor
{
    return [[self class] colorFromHexString:@"FFE4B5"];
}


+ (instancetype)navajoWhiteColor
{
    return [[self class] colorFromHexString:@"FFDEAD"];
}


+ (instancetype)navyColor
{
    return [[self class] colorFromHexString:@"000080"];
}


+ (instancetype)oldLaceColor
{
    return [[self class] colorFromHexString:@"FDF5E6"];
}


+ (instancetype)oliveColor
{
    return [[self class] colorFromHexString:@"808000"];
}


+ (instancetype)oliveDrabColor
{
    return [[self class] colorFromHexString:@"6B8E23"];
}


+ (instancetype)orangeColor
{
    return [[self class] colorFromHexString:@"FFA500"];
}


+ (instancetype)orangeRedColor
{
    return [[self class] colorFromHexString:@"FF4500"];
}


+ (instancetype)orchidColor
{
    return [[self class] colorFromHexString:@"DA70D6"];
}


+ (instancetype)paleGoldenRodColor
{
    return [[self class] colorFromHexString:@"EEE8AA"];
}


+ (instancetype)paleGreenColor
{
    return [[self class] colorFromHexString:@"98FB98"];
}


+ (instancetype)paleTurquoiseColor
{
    return [[self class] colorFromHexString:@"AFEEEE"];
}


+ (instancetype)paleVioletRedColor
{
    return [[self class] colorFromHexString:@"DB7093"];
}


+ (instancetype)papayaWhipColor
{
    return [[self class] colorFromHexString:@"FFEFD5"];
}


+ (instancetype)peachPuffColor
{
    return [[self class] colorFromHexString:@"FFDAB9"];
}


+ (instancetype)peruColor
{
    return [[self class] colorFromHexString:@"CD853F"];
}


+ (instancetype)pinkColor
{
    return [[self class] colorFromHexString:@"FFC0CB"];
}


+ (instancetype)plumColor
{
    return [[self class] colorFromHexString:@"DDA0DD"];
}


+ (instancetype)powderBlueColor
{
    return [[self class] colorFromHexString:@"B0E0E6"];
}


+ (instancetype)rosyBrownColor
{
    return [[self class] colorFromHexString:@"BC8F8F"];
}


+ (instancetype)royalBlueColor
{
    return [[self class] colorFromHexString:@"4169E1"];
}


+ (instancetype)saddleBrownColor
{
    return [[self class] colorFromHexString:@"8B4513"];
}


+ (instancetype)salmonColor
{
    return [[self class] colorFromHexString:@"FA8072"];
}


+ (instancetype)seaGreenColor
{
    return [[self class] colorFromHexString:@"2E8B57"];
}


+ (instancetype)seaShellColor
{
    return [[self class] colorFromHexString:@"FFF5EE"];
}


+ (instancetype)siennaColor
{
    return [[self class] colorFromHexString:@"A0522D"];
}


+ (instancetype)silverColor
{
    return [[self class] colorFromHexString:@"C0C0C0"];
}


+ (instancetype)skyBlueColor
{
    return [[self class] colorFromHexString:@"87CEEB"];
}


+ (instancetype)slateBlueColor
{
    return [[self class] colorFromHexString:@"6A5ACD"];
}


+ (instancetype)slateGrayColor
{
    return [[self class] colorFromHexString:@"708090"];
}


+ (instancetype)snowColor
{
    return [[self class] colorFromHexString:@"FFFAFA"];
}


+ (instancetype)springGreenColor
{
    return [[self class] colorFromHexString:@"00FF7F"];
}


+ (instancetype)steelBlueColor
{
    return [[self class] colorFromHexString:@"4682B4"];
}


+ (instancetype)tanColor
{
    return [[self class] colorFromHexString:@"D2B48C"];
}


+ (instancetype)tealColor
{
    return [[self class] colorFromHexString:@"008080"];
}


+ (instancetype)thistleColor
{
    return [[self class] colorFromHexString:@"D8BFD8"];
}


+ (instancetype)tomatoColor
{
    return [[self class] colorFromHexString:@"FF6347"];
}


+ (instancetype)turquoiseColor
{
    return [[self class] colorFromHexString:@"40E0D0"];
}


+ (instancetype)violetColor
{
    return [[self class] colorFromHexString:@"EE82EE"];
}


+ (instancetype)wheatColor
{
    return [[self class] colorFromHexString:@"F5DEB3"];
}


+ (instancetype)whiteSmokeColor
{
    return [[self class] colorFromHexString:@"F5F5F5"];
}


+ (instancetype)yellowGreenColor
{
    return [[self class] colorFromHexString:@"9ACD32"];
}



@end
