//
//  NSDictionary+Utils.m
//  ViettelPI
//
//  Created by Trinh Duy Nhan on 3/31/14.
//  Copyright (c) 2014 viettel. All rights reserved.
//

#import "NSDictionaryUtils.h"

@implementation NSDictionary (Utils)

- (id)objectOrNilForKey:(id)key
{
    id object = [self objectForKey:key];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end
