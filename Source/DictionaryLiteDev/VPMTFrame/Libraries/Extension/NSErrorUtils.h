//
//  NSErrorUtils.h
//  SmartHospital
//
//  Created by Trinh Duy Nhan on 4/26/14.
//  Copyright (c) 2014 ViettelICT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSErrorUtils : NSObject

+ (NSString *)getMessageLocalizationFromErrorCode:(int)code;

@end
