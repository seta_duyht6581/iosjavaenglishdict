//
//  NSDictionary+Utils.h
//  ViettelPI
//
//  Created by Trinh Duy Nhan on 3/31/14.
//  Copyright (c) 2014 viettel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (BVJSONString)
-(NSString*) toJSON:(BOOL) prettyPrint;
@end

@implementation NSDictionary (BVJSONString)

-(NSString*) toJSON:(BOOL) prettyPrint {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:(NSJSONWritingOptions)    (prettyPrint ? NSJSONWritingPrettyPrinted : 0)
                                                         error:&error];
    
    if (! jsonData) {
        //NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"{}";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}
@end

@interface NSDictionary (Utils)


/**Trả về object xác định bởi key, nếu nó thuộc dictionary, ngược lại trả về nil. Sử dụng đối với webservice để kiểm tra trường hợp trả về [NSNull null] nếu giá trị là 'null' trong chuỗi JSON của webservice trả về.
 @author    nhantd
 @param     key
 Khoá sử dụng
 @return    Object hoặc nil, nếu object không được thiết lập trong dictionary hoặc là NSNULL.
 */
- (id)objectOrNilForKey:(id)key;

@end
