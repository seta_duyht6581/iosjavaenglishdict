//
//  NSDate+Utils.m
//  ViettelPI
//
//  Created by Trinh Duy Nhan on 3/27/14.
//  Copyright (c) 2014 viettel. All rights reserved.
//

#define DATE_COMPONENTS (NSYearCalendarUnit| NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekCalendarUnit |  NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSWeekdayCalendarUnit | NSWeekdayOrdinalCalendarUnit)
#define CURRENT_CALENDAR [NSCalendar currentCalendar]

#import "NSDateUtils.h"

@implementation NSDate (Utils)


+ (BOOL)isEarlierThanDate:(NSDate *)date
{
    NSDate* today = [NSDate date];
	return ([today earlierDate:date] == today);
}


+ (BOOL)isLaterThanDate:(NSDate *)date
{
    NSDate* today = [NSDate date];
	return ([today laterDate:date] == today);
}


+ (NSString *)getDayOfTheWeek:(NSDate *)date
{
    NSDateFormatter *weekday = [[NSDateFormatter alloc] init];
    [weekday setDateFormat: @"EEEE"];
    
    return [[weekday stringFromDate:date] uppercaseString];
}


+ (NSString *)getStringFromDate:(NSDate *)date withDateFormatter:(NSString *)formatDate
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormat setLocale:[NSLocale currentLocale]];
    [dateFormat setDateFormat:formatDate];
    NSString *conversationDateString = [dateFormat stringFromDate:date];
    return conversationDateString;
}


+ (NSString *)getStringVietNameseStandardFromDate:(NSDate *)date
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"vi_VN"]];
    [dateFormat setDateFormat:@"EEEE"];
    NSString *conversationDateString = [NSString stringWithFormat:@"%@, ngày %@ tháng %@ năm %@",[dateFormat stringFromDate:date]
                                        ,[NSDate getStringFromDate:date withDateFormatter:@"dd"]
                                        ,[NSDate getStringFromDate:date withDateFormatter:@"MM"]
                                        ,[NSDate getStringFromDate:date withDateFormatter:@"yyyy"]];
    return conversationDateString;
}


+ (NSString *)getStringFromStringDate:(NSString *)date withStringDateFormat:(NSString *)formatStringDate withDateFormat:(NSString *)formatDate
{
    NSDate *dateConvert = [self getDateFromString:date withDateFormatter:formatStringDate];
    return [self getStringFromDate:dateConvert withDateFormatter:formatDate];
}


+ (NSDate *)getDateFromString:(NSString *)date withDateFormatter:(NSString *)formatDate
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormat setLocale:[NSLocale currentLocale]];
    [dateFormat setDateFormat:formatDate];
    NSDate *conversationStringDate = [dateFormat dateFromString:date];
    return conversationStringDate;
}


+ (NSDate *)getDateFromDate:(NSDate *)date withDateFormatter:(NSString *)formatDate
{
    NSString *str_date = [self getStringFromDate:date withDateFormatter:formatDate];
    return [self getDateFromString:str_date withDateFormatter:formatDate];
}


+ (NSComparisonResult)compareTwoDateWithoutTimeWithDateFirst:(NSDate *)datefirst andDateSecond:(NSDate *)datesecond
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger comps = (NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit);
    
    NSDateComponents *date1Components = [calendar components:comps
                                                    fromDate: datefirst];
    NSDateComponents *date2Components = [calendar components:comps
                                                    fromDate: datesecond];
    
    NSDate *date1 = [calendar dateFromComponents:date1Components];
    NSDate *date2 = [calendar dateFromComponents:date2Components];
    
    NSComparisonResult result = [date1 compare:date2];
    return result;
}


+ (NSInteger)getNumberDaysBetweenDateBegin:(NSDate *)dateBegin andDateEnd:(NSDate *)dateEnd withFormatter:(NSString *)dateFormatter
{
    NSDate *dateStart = [self getDateFromDate:dateBegin withDateFormatter:dateFormatter];
    NSDate *dateStop = [self getDateFromDate:dateEnd withDateFormatter:dateFormatter];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSDayCalendarUnit fromDate:dateStart toDate:dateStop options:0];
    return components.day;
}


+ (NSInteger)getNumberDaysBetweenStringDateBegin:(NSString *)dateBegin andStringDateEnd:(NSString *)dateEnd withFormatter:(NSString *)dateFormatter
{
    NSDate *dateStart = [self getDateFromString:dateBegin withDateFormatter:dateFormatter];
    NSDate *dateStop = [self getDateFromString:dateEnd withDateFormatter:dateFormatter];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSDayCalendarUnit fromDate:dateStart toDate:dateStop options:0];
    return components.day;
}


+ (NSInteger)getMinuteByDate:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit fromDate:date];
    return components.hour;
}


+ (NSInteger)getHourByDate:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit fromDate:date];
    return components.minute;
}


+ (NSInteger)getDayByDate:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    return components.day;
}


+ (NSInteger)getMonthByDate:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    return components.month;
}


+ (NSInteger)getYearByDate:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    return components.year;
}


+ (NSInteger)getNumberDaysInMonthByDate:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSRange range = [calendar rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:date];
    return range.length;
}


+ (NSString *)determineDateInWeek:(NSDate *)date
{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    [calendar setTimeZone:[NSTimeZone localTimeZone]];
    NSDate* now = [NSDate date];
    NSDate *newDate1 = [date dateByAddingTimeInterval:[[NSTimeZone localTimeZone] secondsFromGMT]];
    NSDate *newDate2 = [now dateByAddingTimeInterval:[[NSTimeZone localTimeZone] secondsFromGMT]];
    int differenceInDays = (int)([calendar ordinalityOfUnit:NSDayCalendarUnit inUnit:NSEraCalendarUnit forDate:newDate1] -
                                 [calendar ordinalityOfUnit:NSDayCalendarUnit inUnit:NSEraCalendarUnit forDate:newDate2]);
    NSString* dayString;
    
    switch (differenceInDays)
    {
        case -1:
            dayString = @"Yesterday";
            break;
        case 0:
            dayString = @"Today";
            break;
        case 1:
            dayString = @"Tomorrow";
            break;
        default:
            dayString = [self getDayOfTheWeek:date];
            
            break;
    }
    
    return dayString;
}

+ (NSString *)determineDateWithDate:(NSDate *)date WithFormat:(NSString *)formatter
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar setTimeZone:[NSTimeZone localTimeZone]];
    NSDate* now = [NSDate date];
    NSDate *newDate1 = [date dateByAddingTimeInterval:[[NSTimeZone localTimeZone] secondsFromGMT]];
    NSDate *newDate2 = [now dateByAddingTimeInterval:[[NSTimeZone localTimeZone] secondsFromGMT]];
    int differenceInDays = (int)([calendar ordinalityOfUnit:NSDayCalendarUnit inUnit:NSEraCalendarUnit forDate:newDate1] -
                                 [calendar ordinalityOfUnit:NSDayCalendarUnit inUnit:NSEraCalendarUnit forDate:newDate2]);
    NSString* dayString;
    
    switch (differenceInDays)
    {
        case -1:
            dayString = @"Hôm qua";
            break;
        case 0:
            dayString = @"Hôm nay";
            break;
        case 1:
            dayString = @"Ngày mai";
            break;
        default:
            dayString = [self getStringFromDate:date withDateFormatter:formatter];
            
            break;
    }
    
    return dayString;
}


+ (double)timeIntervalSince1970:(NSString *)date withFormatter:(NSString *)formatter
{
    NSDate *dateReturn = [NSDate getDateFromString:date withDateFormatter:formatter];
    NSTimeInterval interval = [dateReturn timeIntervalSince1970];
    return interval;
}


+ (NSString *)changeTime24HFrom12H:(NSString *)time
{
    NSArray *array = [time componentsSeparatedByString:@" "];
    NSArray *array2 = [[array objectAtIndex:0] componentsSeparatedByString:@":"];
    int hour =  (int)[[array2 objectAtIndex:0] integerValue] ;
    if([[array objectAtIndex:1] isEqualToString:@"PM"]){
        hour = hour+12;
        if(hour ==24){
            hour = 0;
        }
    }else if(hour == 12 && [[array objectAtIndex:1] isEqualToString:@"AM"]){
        hour = 0;
    }
    NSString *str = (hour >=10) ? [NSString stringWithFormat:@"%d:%@",hour,[array2 objectAtIndex:1]]
    : [NSString stringWithFormat:@"0%d:%@",hour,[array2 objectAtIndex:1]];
    return str;
}


+ (NSString *)changeTime12HFrom24H:(NSString *)time
{
    NSArray *array2 = [time componentsSeparatedByString:@":"];
    int hour =  (int)[[array2 objectAtIndex:0] integerValue] ;
    NSString *str = @"AM";
    if(hour>12){
        hour = hour-12;
        str= @"PM";
    }else if(hour==0){
        hour = 12;
        str= @"AM";
    }else if(hour == 12){
        hour = 12;
        str = @"PM";
    }
    NSString *strF = (hour>=10) ? [NSString stringWithFormat:@"%d:%@ %@",hour,[array2 objectAtIndex:1],str] : [NSString stringWithFormat:@"0%d:%@ %@",hour,[array2 objectAtIndex:1],str];
    return strF;
}


+ (NSDate *)dateLocalGMTSystem:(NSDate *)date
{
    NSDate* sourceDate = date;
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    NSDate* dateCv = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
    return dateCv;
}


- (NSDate *)dateLocalGMTSystem
{
    return [NSDate dateLocalGMTSystem:self];
}


+ (NSUInteger)getAgeFromBirthday:(NSDate *)birthday
{
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSYearCalendarUnit
                                       fromDate:birthday
                                       toDate:[NSDate date]
                                       options:0];
    return [ageComponents year];
}


+ (BOOL)isLeapYear:(NSInteger)year
{
    return ((( year%100 != 0) && (year%4 == 0)) || year%400 == 0);
}


+ (BOOL)isLeapYearByDate:(NSDate *)date
{
    NSInteger year = [self getYearByDate:date];
    return [self isLeapYear:year];
}


+ (NSDate *)beginningOfDay:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:date];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [components setTimeZone:timeZone];
    
    return [calendar dateFromComponents:components];
}


+ (NSDate *)endOfDay:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [components setTimeZone:timeZone];
    [components setDay:1];
    
    return [[calendar dateByAddingComponents:components toDate:[NSDate beginningOfDay:date] options:0] dateByAddingTimeInterval:-1];
}


+ (NSDate *)beginningOfWeek:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekdayCalendarUnit | NSDayCalendarUnit fromDate:date];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [components setTimeZone:timeZone];
    
    NSUInteger offset = ([components weekday] == [calendar firstWeekday]) ? 6 : [components weekday] - 2;
    [components setDay:[components day] - offset];
    
    return [calendar dateFromComponents:components];
}


+ (NSDate *)endOfWeek:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [components setTimeZone:timeZone];
    [components setWeek:1];
    
    return [[calendar dateByAddingComponents:components toDate:[NSDate beginningOfWeek:date] options:0] dateByAddingTimeInterval:-1];
}


+ (NSDate *)beginningOfMonth:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit fromDate:date];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [components setTimeZone:timeZone];
    
    return [calendar dateFromComponents:components];
}


+ (NSDate *)endOfMonth:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [components setTimeZone:timeZone];
    [components setMonth:1];
    
    return [[calendar dateByAddingComponents:components toDate:[NSDate beginningOfMonth:date] options:0] dateByAddingTimeInterval:-1];
}


+ (NSDate *)beginningOfYear:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit fromDate:date];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [components setTimeZone:timeZone];
    
    return [calendar dateFromComponents:components];
}


+ (NSDate *)endOfYear:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [components setTimeZone:timeZone];
    [components setYear:1];
    
    return [[calendar dateByAddingComponents:components toDate:[NSDate beginningOfYear:date] options:0] dateByAddingTimeInterval:-1];
}


- (NSDate *)beginningOfDay
{
    return [NSDate beginningOfDay:self];
}


- (NSDate *)endOfDay
{
    return [NSDate endOfDay:self];
}


- (NSDate *)beginningOfWeek
{
    return [NSDate beginningOfWeek:self];
}


- (NSDate *)endOfWeek
{
    return [NSDate endOfWeek:self];
}


- (NSDate *)beginningOfMonth
{
    return [NSDate beginningOfMonth:self];
}


- (NSDate *)endOfMonth
{
    return [NSDate endOfMonth:self];
}


- (NSDate *)beginningOfYear
{
    return [NSDate beginningOfYear:self];
}


- (NSDate *)endOfYear
{
    return [NSDate endOfYear:self];
}


+ (NSDate *)getYesterday:(NSDate *)date
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:-1];
    return [cal dateByAddingComponents:comps toDate:date options:0];
}


+ (NSArray *)getArrayDateWithDateBegin:(NSDate *)dateBegin andDateEnd:(NSDate *)dateEnd
{
    NSComparisonResult result = [self compareTwoDateWithoutTimeWithDateFirst:dateBegin andDateSecond:dateEnd];
    if (result == NSOrderedAscending || result == NSOrderedSame) {
        NSMutableArray *dates = [NSMutableArray array];
        if (result != NSOrderedSame) {
            [dates addObject:dateBegin];
        }
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                            fromDate:dateBegin
                                                              toDate:dateEnd
                                                             options:0];
        
        for (int i = 1; i < components.day; ++i) {
            NSDateComponents *newComponents = [NSDateComponents new];
            newComponents.day = i;
            
            NSDate *date = [gregorianCalendar dateByAddingComponents:newComponents
                                                              toDate:dateBegin
                                                             options:0];
            [dates addObject:date];
        }
        
        [dates addObject:dateEnd];
        return dates;
    }
    return nil;
}

-(NSString*)toString{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    return [dateFormatter stringFromDate:self];
}

-(NSString*)toStringWithFormat:(NSString*)dateFormat{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = dateFormat;
    return [dateFormatter stringFromDate:self];
}
@end
