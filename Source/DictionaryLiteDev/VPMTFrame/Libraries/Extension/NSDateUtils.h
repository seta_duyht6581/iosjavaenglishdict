//
//  NSDate+Utils.h
//  ViettelPI
//
//  Created by Trinh Duy Nhan on 3/27/14.
//  Copyright (c) 2014 viettel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Utils)

/**Kiểm tra xem ngày hiện tại có ở trước (sớm hơn) ngày nhập vào hay không.
 @author    nhantd
 @param     date
 Ngày nhập vào
 @return    YES nếu ngày hiện tại sớm hơn ngày nhập vào. NO nếu ngược lại.
 */
+ (BOOL)isEarlierThanDate:(NSDate *)date;


/**Kiểm tra xem ngày hiện tại có ở sau (trễ hơn) ngày nhập vào hay không.
 @author    nhantd
 @param     date
 Ngày nhập vào
 @return    YES nếu ngày hiện tại trễ hơn ngày nhập vào. NO nếu ngược lại.
 */
+ (BOOL)isLaterThanDate:(NSDate *)date;


/**Lấy ngày (thứ) của tuần xác định bởi ngày nhập vào. Ex : Monday, Tuesday, Wednesday, Thursday...
 @author    nhantd
 @param     date
 Ngày nhập vào
 @return    NSString chứa ngày (thứ) của tuần.
 */
+ (NSString *)getDayOfTheWeek:(NSDate *)date;


/**Lấy chuỗi NSString xác định bởi ngày và định dạng ngày nhập vào. Định dạng ngày Ex : MM/dd/yyyy
 @author    nhantd
 @code
 NSString *result = [NSDate getStringFromDate:[NSDate date]
 withDateFormatter:@"MM/dd/yyyy"];
 @endcode
 @param     date
 Ngày nhập vào
 @param     formatDate
 Định dạng ngày nhập vào
 @return    NSString chứa ngày muốn lấy.
 */
+ (NSString *)getStringFromDate:(NSDate *)date withDateFormatter:(NSString *)formatDate;


/**Lấy chuỗi NSString theo chuẩn Việt Nam xác định bởi ngày nhập vào. Định dạng chuẩn Ex : Thứ 6, ngày 16 tháng 05 năm 2014
 @author    nhantd
 @code
 NSString *result = [NSDate getStringVietNameseStandardFromDate:date];
 @endcode
 @param     date
 Ngày nhập vào
 @return    NSString chứa ngày muốn lấy.
 */
+ (NSString *)getStringVietNameseStandardFromDate:(NSDate *)date;


/**Lấy chuỗi NSString xác định bởi chuỗi ngày và định dạng ngày nhập vào. Định dạng ngày Ex : MM/dd/yyyy
 @author    nhantd
 @param     date
 Ngày nhập vào
 @param     formatStringDate
 Định dạng chuỗi ngày nhập vào
 @param     formatDate
 Định dạng chuỗi ngày xuất ra.
 @return    NSString chứa ngày muốn lấy.
 */
+ (NSString *)getStringFromStringDate:(NSString *)date withStringDateFormat:(NSString *)formatStringDate withDateFormat:(NSString *)formatDate;


/**Lấy ngày xác định bởi chuỗi NSString thể hiện cho ngày và định dạng ngày nhập vào. Định dạng ngày
 Ex : MM/dd/yyyy
 @author    nhantd
 @code
 NSDate *result = [NSDate getDateFromString:@"12/03/2014 11:15:00 AM"
 withDateFormatter:@"MM/dd/yyyy"];
 @endcode
 @param     date
 Ngày nhập vào
 @param     formatDate
 Định dạng ngày nhập vào
 @return    NSDate chứa ngày muốn lấy.
 */
+ (NSDate *)getDateFromString:(NSString *)date withDateFormatter:(NSString *)formatDate;


/**Lấy ngày xác định bởi ngày và định dạng ngày nhập vào (Thay đổi định dạng của ngày nhập vào).
 @author    nhantd
 @code
 NSDate *result = [NSDate getDateFromDate:[NSDate date]
 withDateFormatter:@"MM/dd/yyyy"];
 @endcode
 @param     date
 Ngày nhập vào
 @param     formatDate
 Định dạng ngày nhập vào
 @return    NSDate chứa ngày muốn lấy.
 */
+ (NSDate *)getDateFromDate:(NSDate *)date withDateFormatter:(NSString *)formatDate;


/**So sánh 2 ngày nhập vào (Bỏ qua time).
 @author    nhantd
 @param     datefirst
 Ngày thứ nhất
 @param     datesecond
 Ngày thứ hai
 @return    NSComparisonResult.
 Nếu datefirst < datesecond : return NSOrderedAscending.
 Nếu datefirst > datesecond : return NSOrderedDescending.
 Nếu datefirst = datesecond : return NSOrderedSame.
 */
+ (NSComparisonResult)compareTwoDateWithoutTimeWithDateFirst:(NSDate *)datefirst andDateSecond:(NSDate *)datesecond;


/**Lấy số ngày giữa 2 ngày nhập vào.
 @author    nhantd
 @param     dateBegin
 Ngày bắt đầu
 @param     dateEnd
 Ngày kết thúc
 @param     dateFormatter
 Định dạng ngày
 @return    NSInteger số ngày giữa 2 ngày nhập vào.
 NSInteger < 0 : Ngày bắt đầu lớn hơn ngày kết thúc.
 NSINteger = 0 : Ngày bắt đầu bằng ngày kết thúc.
 NSInteger > 0 : Ngày bắt đầu nhỏ hơn ngày kết thúc.
 */
+ (NSInteger)getNumberDaysBetweenDateBegin:(NSDate *)dateBegin andDateEnd:(NSDate *)dateEnd withFormatter:(NSString *)dateFormatter;

/**Lấy số ngày giữa 2 ngày nhập vào. Lưu ý 2 ngày nhập vào phải cùng kiểu dateFormatter.
 @author    nhantd
 @param     dateBegin
 Chuỗi thể hiện ngày bắt đầu
 @param     dateEnd
 Chuỗi thể hiện ngày kết thúc
 @param     dateFormatter
 Định dạng ngày
 @return    NSInteger số ngày giữa 2 ngày nhập vào.
 NSInteger < 0 : Ngày bắt đầu lớn hơn ngày kết thúc.
 NSINteger = 0 : Ngày bắt đầu bằng ngày kết thúc.
 NSInteger > 0 : Ngày bắt đầu nhỏ hơn ngày kết thúc.
 */
+ (NSInteger)getNumberDaysBetweenStringDateBegin:(NSString *)dateBegin andStringDateEnd:(NSString *)dateEnd withFormatter:(NSString *)dateFormatter;


/**Lấy về phút (Minute) xác định bởi ngày nhập vào.
 @author    nhantd
 @param     date
 Ngày nhập vào
 @return    NSInteger phút xác định bởi date.
 */
+ (NSInteger)getMinuteByDate:(NSDate *)date;


/**Lấy về giờ (Hour) xác định bởi ngày nhập vào.
 @author    nhantd
 @param     date
 Ngày nhập vào
 @return    NSInteger giờ xác định bởi date.
 */
+ (NSInteger)getHourByDate:(NSDate *)date;


/**Lấy về ngày (Day) xác định bởi ngày nhập vào.
 @author    nhantd
 @param     date
 Ngày nhập vào
 @return    NSInteger ngày xác định bởi date.
 */
+ (NSInteger)getDayByDate:(NSDate *)date;


/**Lấy về tháng (Month) xác định bởi ngày nhập vào.
 @author    nhantd
 @param     date
 Ngày nhập vào
 @return    NSInteger tháng xác định bởi date.
 */
+ (NSInteger)getMonthByDate:(NSDate *)date;


/**Lấy về năm (Year) xác định bởi ngày nhập vào.
 @author    nhantd
 @param     date
 Ngày nhập vào
 @return    NSInteger năm xác định bởi date.
 */
+ (NSInteger)getYearByDate:(NSDate *)date;


/**Lấy số ngày trong tháng xác định bởi ngày nhập vào.
 @author    nhantd
 @param     date
 Ngày nhập vào
 @return    NSInteger số ngày trong tháng.
 */
+ (NSInteger)getNumberDaysInMonthByDate:(NSDate *)date;


/**Lấy ngày (thứ) trong tuần xác định bởi ngày nhập vào. Có xác định hôm qua, hôm nay, và ngày mai. Sử dụng trong ứng dụng Note.
 @author    nhantd
 @param     date
 Ngày nhập vào
 @return    NSString ngày (thứ) trong tuần xác định bởi ngày nhập vào.
 Nếu ngày nhập vào bằng với ngày hiện tại thì trả về "Today".
 Nếu ngày nhập vào nhỏ hơn ngày hiện tại 1 ngày thì trả về "Yesterday".
 Nếu ngày nhập vào lớn hơn ngày hiện tại 1 ngày thì trả về "Tomorrow".
 Các trường hợp còn lại trả về thứ trong tuần xác định bởi ngày nhập vào.
 */
+ (NSString *)determineDateInWeek:(NSDate *)date;


/**Lấy ngày xác định bởi ngày nhập vào. Có xác định hôm qua, hôm nay, và ngày mai. Sử dụng trong ứng dụng Note.
 @author    nhantd
 @param     date
 Ngày nhập vào
 @param     formatter
 Định dạng ngày
 @return    NSString ngày trong tuần xác định bởi ngày nhập vào.
 Nếu ngày nhập vào bằng với ngày hiện tại thì trả về "Hôm nay".
 Nếu ngày nhập vào nhỏ hơn ngày hiện tại 1 ngày thì trả về "Hôm qua".
 Nếu ngày nhập vào lớn hơn ngày hiện tại 1 ngày thì trả về "Ngày mai".
 Các trường hợp còn lại trả về ngày xác định bởi ngày và định dạng nhập vào.
 */
+ (NSString *)determineDateWithDate:(NSDate *)date WithFormat:(NSString *)formatter;


/**Lấy thời gian trả về tính từ năm 1970 đến ngày nhập vào xác định bởi định dạng ngày nhập vào.
 @author    nhantd
 @param     date
 Ngày nhập vào
 @param     formatter
 Định dạng ngày nhập vào
 @return    double thời gian tính bằng milliseconds.
 */
+ (double)timeIntervalSince1970:(NSString *)date withFormatter:(NSString *)formatter;


/**Đổi thời gian 12H thành 24H.
 @author    nhantd
 @param     time
 Thời gian nhập vào
 @return    NSString chuỗi thời gian 12H đổi từ thời gian 24H nhập vào.
 */
+ (NSString *)changeTime24HFrom12H:(NSString *)time;


/**Đổi thời gian 24H thành 12H.
 @author    nhantd
 @param     time
 Thời gian nhập vào
 @return    NSString chuỗi thời gian 12H đổi từ thời gian 24H nhập vào.
 */
+ (NSString *)changeTime12HFrom24H:(NSString *)time;


/**Lấy ngày cục bộ của hệ thống theo giờ GMT xác định bởi ngày nhập vào.
 @author    nhantd
 @param     date
 Ngày nhập vào
 @return    NSDate ngày cục bộ của hệ thống theo giờ GMT.
 */
+ (NSDate *)dateLocalGMTSystem:(NSDate *)date;

/**Lấy ngày cục bộ của hệ thống theo giờ GMT.
 @author    nhantd
 @return    NSDate ngày cục bộ của hệ thống theo giờ GMT.
 */
- (NSDate *)dateLocalGMTSystem;


/**Lấy tuổi xác định bởi ngày sinh nhập vào.
 @author    nhantd
 @param     birthday
 Ngày sinh nhập vào
 @return    NSUInteger tuổi xác định bởi ngày sinh nhập vào.
 */
+ (NSUInteger)getAgeFromBirthday:(NSDate *)birthday;


/**Kiểm tra năm nhập vào có phải là năm nhuận hay không. Năm nhuận là năm mà tháng 2 có 29 ngày.
 @author    nhantd
 @param     year
 Năm nhập vào
 @return    YES nếu năm nhập vào là năm nhuận. NO nếu ngược lại.
 */
+ (BOOL)isLeapYear:(NSInteger)year;


/**Kiểm tra năm chứa ngày nhập vào có phải là năm nhuận hay không. Năm nhuận là năm mà tháng 2 có 29 ngày.
 @author    nhantd
 @param     date
 Ngày nhập vào
 @return    YES nếu năm nhập vào là năm nhuận. NO nếu ngược lại.
 */
+ (BOOL)isLeapYearByDate:(NSDate *)date;


/**Lấy thời gian bắt đầu xác định bởi ngày nhập vào.
 @author    nhantd
 @param     date
 Ngày nhập vào
 @return    NSDate thời gian bắt đầu xác định bởi ngày nhập vào.
 */
+ (NSDate *)beginningOfDay:(NSDate *)date;


/**Lấy thời gian kết thúc xác định xác định bởi ngày nhập vào.
 @author    nhantd
 @param     date
 Ngày nhập vào
 @return    NSDate thời gian kết thúc xác định bởi ngày nhập vào.
 */
+ (NSDate *)endOfDay:(NSDate *)date;


/**Lấy ngày đầu tiên của tuần xác định bởi ngày nhập vào.
 @author    nhantd
 @param     date
 Ngày nhập vào
 @return    NSDate ngày đầu tiên của tuần.
 */
+ (NSDate *)beginningOfWeek:(NSDate *)date;


/**Lấy ngày cuối cùng của tuần xác định bởi ngày nhập vào.
 @author    nhantd
 @param     date
 Ngày nhập vào
 @return    NSDate ngày cuối cùng của tuần.
 */
+ (NSDate *)endOfWeek:(NSDate *)date;


/**Lấy ngày đầu tiên của tháng xác định bởi ngày nhập vào.
 @author    nhantd
 @param     date
 Ngày nhập vào
 @return    NSDate ngày đầu tiên của tháng.
 */
+ (NSDate *)beginningOfMonth:(NSDate *)date;


/**Lấy ngày cuối cùng của tháng xác định bởi ngày nhập vào.
 @author    nhantd
 @param     date
 Ngày nhập vào
 @return    NSDate ngày cuối cùng của tháng.
 */
+ (NSDate *)endOfMonth:(NSDate *)date;


/**Lấy ngày đầu tiên của năm xác định bởi ngày nhập vào.
 @author    nhantd
 @param     date
 Ngày nhập vào
 @return    NSDate ngày đầu tiên của tháng.
 */
+ (NSDate *)beginningOfYear:(NSDate *)date;


/**Lấy ngày cuối cùng của năm xác định bởi ngày nhập vào.
 @author    nhantd
 @param     date
 Ngày nhập vào
 @return    NSDate ngày cuối cùng của năm.
 */
+ (NSDate *)endOfYear:(NSDate *)date;


/**Lấy thời gian bắt đầu của ngày.
 @author    nhantd
 @return    NSDate thời gian bắt đầu của ngày.
 */
- (NSDate *)beginningOfDay;


/**Lấy thời gian kết thúc của ngày.
 @author    nhantd
 @return    NSDate thời gian kết thúc của ngày.
 */
- (NSDate *)endOfDay;


/**Lấy ngày đầu tiên trong tuần.
 @author    nhantd
 @return    NSDate ngày đầu tiên trong tuần.
 */
- (NSDate *)beginningOfWeek;


/**Lấy ngày kết thúc trong tuần.
 @author    nhantd
 @return    NSDate ngày cuối cùng trong tuần.
 */
- (NSDate *)endOfWeek;


/**Lấy ngày đầu tiên trong tháng.
 @author    nhantd
 @return    NSDate ngày đầu tiên trong tháng.
 */
- (NSDate *)beginningOfMonth;


/**Lấy ngày kết thúc trong tháng.
 @author    nhantd
 @return    NSDate ngày cuối cùng trong tháng.
 */
- (NSDate *)endOfMonth;


/**Lấy ngày đầu tiên trong năm.
 @author    nhantd
 @return    NSDate ngày đầu tiên trong năm.
 */
- (NSDate *)beginningOfYear;


/**Lấy ngày cuối cùng trong năm.
 @author    nhantd
 @return    NSDate ngày cuối cùng trong năm.
 */
- (NSDate *)endOfYear;

/**Chuyển NSDate sang NSString.
 @author    chuongpd
 @param     NSDate
 @return    NSString.
 */
-(NSString*)toString;
-(NSString*)toStringWithFormat:(NSString*)dateFormat;

/**Lấy ngày hôm qua được xác định bởi ngày nhập vào.
 @author    nhantd
 @param     date
 Ngày nhập vào
 @return    NSDate ngày hôm qua.
 */
+ (NSDate *)getYesterday:(NSDate *)date;


/**Lấy danh sách các ngày từ ngày bắt đầu đến ngày kết thúc (Bao gồm cả ngày bắt đầu và ngày kết thúc).
 @author    nhantd
 @param     dateBegin
 Ngày bắt đầu
 @param     dateEnd
 Ngày kết thúc
 @return    NSArray danh sách ngày trả về.
 */
+ (NSArray *)getArrayDateWithDateBegin:(NSDate *)dateBegin andDateEnd:(NSDate *)dateEnd;
@end
