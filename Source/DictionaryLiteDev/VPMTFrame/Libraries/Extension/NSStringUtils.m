//
//  NSString+Utils.m
//  ViettelPI
//
//  Created by Trinh Duy Nhan on 3/31/14.
//  Copyright (c) 2014 viettel. All rights reserved.
//

#import "NSStringUtils.h"

@implementation NSString (Utils)
-(NSString*) toJSON:(BOOL) prettyPrint {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:(NSJSONWritingOptions)    (prettyPrint ? NSJSONWritingPrettyPrinted : 0)
                                                         error:&error];
    
    if (! jsonData) {
        //NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"{}";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

- (NSString *)stringByReplacingOccurrencesOfStringArray:(NSArray *)targetarray withStringArray:(NSArray *)replacementarray
{
    NSAssert((targetarray || replacementarray || targetarray.count == replacementarray.count),@"ERROR::targetarray or replacementarray is nil or targetarray.cout != replacementarray.count, they must be not nil and have same number objects.");
    NSString *str_convert = self;
    for (int i = 0; i < targetarray.count; i++) {
        str_convert = [str_convert stringByReplacingOccurrencesOfString:targetarray[i] withString:replacementarray[i]];
    }
    return str_convert;
}


- (BOOL)containsString:(NSString *)string
{
    return [self containsString:string ignoringCase:NO];
}

-(NSMutableString*)appendTo:(NSMutableString*)source{
    int size = self.length;
    for (int i = 0; i < size; i+=100) {
        if (size - i > 100) {
            [source appendString:[self substringWithRange:NSMakeRange(i, 100)]];
        }else{
            [source appendString:[self substringWithRange:NSMakeRange(i, size-i)]];
        }
    }
    
    return source;
}

- (BOOL)containsString:(NSString *)string ignoringCase:(BOOL)flag
{
    unsigned mask = (flag ? NSCaseInsensitiveSearch : 0);
    NSRange range = [self rangeOfString:string options:mask];
    return (range.length > 0);
}


- (NSInteger)countNumberWordsInString
{
    __block NSInteger wordCount = 0;
    [self enumerateSubstringsInRange:NSMakeRange(0, [self length]) options:NSStringEnumerationByWords usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
        wordCount++;
    }];
    return wordCount;
}


- (NSString *)trims
{
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}


- (void)sendToPasteboard
{
    UIPasteboard *pasteboard = [UIPasteboard pasteboardWithUniqueName];
    pasteboard.persistent = YES;
    [pasteboard setString:self];
}


- (NSInteger)countOccurrencesOfSubString:(NSString *)subString
{
    NSInteger count = 0;
    NSInteger length = [self length];
    NSRange range = NSMakeRange(0, length);
    while (range.location != NSNotFound) {
        range = [self rangeOfString:subString options:0 range:range];
        if (range.location != NSNotFound) {
            range = NSMakeRange(range.location + range.length, length - (range.location + range.length));
            count++;
        }
    }
    return count;
}


- (NSString *)convertToCurrencyWithCurrency:(NSString *)currencySymbol
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setAlwaysShowsDecimalSeparator:FALSE];
    NSDecimalNumber *number = (NSDecimalNumber *)self;
    if (number < 0) {
        [numberFormatter setPositiveFormat:@"-#,##0.###"];
    } else {
        [numberFormatter setPositiveFormat:@"#,##0.###"];
    }
    NSString *numberCurrency = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:[number doubleValue]]];
    return [NSString stringWithFormat:@"%@ %@",numberCurrency,currencySymbol];
}


+ (NSString *)convertToCurrencyWithNumber:(NSNumber *)number withCurrency:(NSString *)currencySymbol
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setAlwaysShowsDecimalSeparator:FALSE];
    if (number < 0) {
        [numberFormatter setPositiveFormat:@"-#,##0.###"];
    } else {
        [numberFormatter setPositiveFormat:@"#,##0.###"];
    }
    NSString *numberCurrency = [numberFormatter stringFromNumber:number];
    return [NSString stringWithFormat:@"%@%@",numberCurrency,currencySymbol];
}


+ (NSComparisonResult)compareStringsUnicode:(NSString *)unicodeString withString:(NSString *)string
{
    NSComparisonResult compareResult;
    unicodeString = [unicodeString stringByReplacingOccurrencesOfString:@"Đ" withString:@"D"];
    return compareResult = [string compare:unicodeString options:NSCaseInsensitiveSearch|NSLiteralSearch|NSDiacriticInsensitiveSearch range:NSMakeRange(0, unicodeString.length)];
}


- (NSString *)getStringOrEmpty
{
    if (self.length < 1 || [self isEqual:[NSNull null]] || self == nil) {
        return @"";
    }
    return self;
}


+ (NSString *)convertASCIIStringFromVietnameseString:(NSString *)string
{
    string = [string stringByReplacingOccurrencesOfString:@"đ" withString:@"d"];
    string = [string stringByReplacingOccurrencesOfString:@"Đ" withString:@"D"];
    NSData *data = [string dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *stringconvert = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
    return stringconvert;
}


- (NSString *)urlEncode
{
    NSString* encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                (CFStringRef)self,
                                                NULL,
                                                (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding)));
	return encodedString;
}


- (NSString *)substringFrom:(NSInteger)start to:(NSInteger)end
{
    NSRange r;
	r.location = start;
	r.length = end - start;
	return [self substringWithRange:r];
}


- (NSString *)reverse
{
    NSMutableString *reversed = [NSMutableString string];
    NSInteger charIndex = [self length];
    while (charIndex > 0) {
        charIndex--;
        NSRange subRange = NSMakeRange(charIndex, 1);
        [reversed appendString:[self substringWithRange:subRange]];
    }
    return reversed;
}


- (BOOL)isSymmetric
{
    return [NSString isSymmetric:self];
}

- (NSDate*)toDate{
    if ([self rangeOfString:@"01/01/0001"].length > 0) {
        return nil;
    }
    
    NSDateFormatter *dateFormatter=[NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd/MM/yyyy'T'HH:mm:ss'ZZZ'"];
    
    NSDate *date =  [dateFormatter dateFromString:self];
    
    if (date == nil) {
        [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
        date =  [dateFormatter dateFromString:self];
    }
    
    if (date == nil) {
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'ZZZ'"];
        date =  [dateFormatter dateFromString:self];
    }
    
    if (date == nil) {
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'ZZZ'"];
        date =   [dateFormatter dateFromString:self];
    }
    
    if (date == nil) {
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'ZZ'"];
        date =   [dateFormatter dateFromString:self];
    }
    
    if (date == nil) {
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
        date =   [dateFormatter dateFromString:self];
    }
    
    if (date == nil) {
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.'ZZZ'"];
        date =   [dateFormatter dateFromString:self];
    }
    
    if (date == nil) {
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.'ZZ'"];
        return [dateFormatter dateFromString:self];
    }
    
    return nil;
}

-(NSDictionary*)toDictionary{
    return [NSJSONSerialization JSONObjectWithData: [self dataUsingEncoding:NSUTF8StringEncoding] options: NSJSONReadingMutableContainers                                error: nil];
}

+ (BOOL)isSymmetric:(NSString *)string
{
    return [NSString isSymmetric:string startAt:0];
}


+ (BOOL)isSymmetric:(NSString *)string startAt:(NSInteger)position
{
    NSString *_string = [NSString stringWithString:string];
	NSInteger _position = position;
    
	if (! _string) {
		return NO;
	}
    
	NSInteger stringLength = [_string length];
	NSString *firstChar = [[_string substringToIndex:_position] substringToIndex:1];
	NSString *lastChar = [[_string substringToIndex:(stringLength - 1 - _position)] substringToIndex:1];
    
	if (_position > (stringLength / 2)) {
		return YES;
	}
    
	if (! [firstChar isEqualToString:lastChar]) {
		return NO;
	}
    
	return [NSString isSymmetric:_string startAt:(_position + 1)];
}

@end
