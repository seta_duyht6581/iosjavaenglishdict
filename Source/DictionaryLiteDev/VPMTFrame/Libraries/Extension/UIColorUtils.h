//
//  UIColor+Utils.h
//  ViettelPI
//
//  Created by Trinh Duy Nhan on 4/4/14.
//  Copyright (c) 2014 viettel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Utils)


/**Lấy color từ chuỗi hex string.
 @author    nhantd
 @param     hexString
 Chuỗi hexString color. Ex : @"FFFFFF"
 @return    UIColor color convert từ chuỗi hexString
 */
+ (UIColor *) colorFromHexString:(NSString *)hexString;

+ (instancetype)aliceBlueColor;
+ (instancetype)antiqueWhiteColor;
+ (instancetype)aquaColor;
+ (instancetype)aquamarineColor;
+ (instancetype)azureColor;
+ (instancetype)beigeColor;
+ (instancetype)bisqueColor;
+ (instancetype)blanchedAlmondColor;
+ (instancetype)blueVioletColor;
+ (instancetype)burlyWoodColor;
+ (instancetype)cadetBlueColor;
+ (instancetype)chartreuseColor;
+ (instancetype)chocolateColor;
+ (instancetype)coralColor;
+ (instancetype)cornflowerBlueColor;
+ (instancetype)cornsilkColor;
+ (instancetype)crimsonColor;
+ (instancetype)darkBlueColor;
+ (instancetype)darkCyanColor;
+ (instancetype)darkGoldenRodColor;
+ (instancetype)darkGreenColor;
+ (instancetype)darkKhakiColor;
+ (instancetype)darkMagentaColor;
+ (instancetype)darkOliveGreenColor;
+ (instancetype)darkOrangeColor;
+ (instancetype)darkOrchidColor;
+ (instancetype)darkRedColor;
+ (instancetype)darkSalmonColor;
+ (instancetype)darkSeaGreenColor;
+ (instancetype)darkSlateBlueColor;
+ (instancetype)darkSlateGrayColor;
+ (instancetype)darkTurquoiseColor;
+ (instancetype)darkVioletColor;
+ (instancetype)deepPinkColor;
+ (instancetype)deepSkyBlueColor;
+ (instancetype)dimGrayColor;
+ (instancetype)dodgerBlueColor;
+ (instancetype)fireBrickColor;
+ (instancetype)floralWhiteColor;
+ (instancetype)forestGreenColor;
+ (instancetype)fuchsiaColor;
+ (instancetype)gainsboroColor;
+ (instancetype)ghostwhiteColor;
+ (instancetype)goldColor;
+ (instancetype)goldenRodColor;
+ (instancetype)greenYellowColor;
+ (instancetype)honeyDewColor;
+ (instancetype)hotPinkColor;
+ (instancetype)indianRedColor;
+ (instancetype)indigoColor;
+ (instancetype)ivoryColor;
+ (instancetype)khakiColor;
+ (instancetype)lavenderColor;
+ (instancetype)lavenderBlushColor;
+ (instancetype)lawnGreenColor;
+ (instancetype)lemonChiffonColor;
+ (instancetype)lightBlueColor;
+ (instancetype)lightCoralColor;
+ (instancetype)lightCyanColor;
+ (instancetype)lightGoldenRodYellowColor;
+ (instancetype)lightGreenColor;
+ (instancetype)lightPinkColor;
+ (instancetype)lightSalmonColor;
+ (instancetype)lightSeaGreenColor;
+ (instancetype)lightSkyBlueColor;
+ (instancetype)lightSlateGrayColor;
+ (instancetype)lightSteelBlueColor;
+ (instancetype)lightYellowColor;
+ (instancetype)limeColor;
+ (instancetype)limeGreenColor;
+ (instancetype)linenColor;
+ (instancetype)maroonColor;
+ (instancetype)mediumAquaMarineColor;
+ (instancetype)mediumBlueColor;
+ (instancetype)mediumOrchidColor;
+ (instancetype)mediumPurpleColor;
+ (instancetype)mediumSeaGreenColor;
+ (instancetype)mediumSlateBlueColor;
+ (instancetype)mediumSpringGreenColor;
+ (instancetype)mediumTurquoiseColor;
+ (instancetype)mediumVioletRedColor;
+ (instancetype)midnightBlueColor;
+ (instancetype)mintCreamColor;
+ (instancetype)mistyRoseColor;
+ (instancetype)moccasinColor;
+ (instancetype)navajoWhiteColor;
+ (instancetype)navyColor;
+ (instancetype)oldLaceColor;
+ (instancetype)oliveColor;
+ (instancetype)oliveDrabColor;
+ (instancetype)orangeColor;
+ (instancetype)orangeRedColor;
+ (instancetype)orchidColor;
+ (instancetype)paleGoldenRodColor;
+ (instancetype)paleGreenColor;
+ (instancetype)paleTurquoiseColor;
+ (instancetype)paleVioletRedColor;
+ (instancetype)papayaWhipColor;
+ (instancetype)peachPuffColor;
+ (instancetype)peruColor;
+ (instancetype)pinkColor;
+ (instancetype)plumColor;
+ (instancetype)powderBlueColor;
+ (instancetype)rosyBrownColor;
+ (instancetype)royalBlueColor;
+ (instancetype)saddleBrownColor;
+ (instancetype)salmonColor;
+ (instancetype)seaGreenColor;
+ (instancetype)seaShellColor;
+ (instancetype)siennaColor;
+ (instancetype)silverColor;
+ (instancetype)skyBlueColor;
+ (instancetype)slateBlueColor;
+ (instancetype)slateGrayColor;
+ (instancetype)snowColor;
+ (instancetype)springGreenColor;
+ (instancetype)steelBlueColor;
+ (instancetype)tanColor;
+ (instancetype)tealColor;
+ (instancetype)thistleColor;
+ (instancetype)tomatoColor;
+ (instancetype)turquoiseColor;
+ (instancetype)violetColor;
+ (instancetype)wheatColor;
+ (instancetype)whiteSmokeColor;
+ (instancetype)yellowGreenColor;

@end
