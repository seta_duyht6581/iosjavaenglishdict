//
//  NSString+Utils.h
//  ViettelPI
//
//  Created by Trinh Duy Nhan on 3/31/14.
//  Copyright (c) 2014 viettel. All rights reserved.
//

#import <Foundation/Foundation.h>
@implementation NSNull (NSNull_nilIfNull)

+ (id)nilIfNull:(id)object {
    if (object == [self null]) {
        return nil;
    }
    return object;
}

@end

@interface NSString (Utils)

/**Convert String to Json.
 @author    chuongpd
 */
-(NSString*) toJSON:(BOOL) prettyPrint;

/**Thay đổi chuỗi string chứa các ký tự nằm trong mảng targetarray bằng các ký tự nằm trong mảng replacementarray. Lưu ý 2 mảng array có số độ lớn bằng nhau.
 @author    nhantd
 @code
 text = [text stringByReplacingOccurrencesOfStringArray:[NSArray arrayWithObjects:@"+",@"/",@"=",@"%", nil] withStringArray:[NSArray arrayWithObjects:@"!",@",",@"]",@"@", nil]];
 @endcode
 @param     targetarray
 Mảng chứa các ký tự muốn thay đổi
 @param     replacementarray
 Mảng chứa các ký tự thay thế
 @return    NSString chuỗi đã thay thế các ký tự xác định.
 */
- (NSString *)stringByReplacingOccurrencesOfStringArray:(NSArray *)targetarray withStringArray:(NSArray *)replacementarray;


/**Kiểm tra xem chuỗi string có tồn tại trong chuỗi có sẵn hay không. Không phân biệt HOA, thường.
 @author    nhantd
 @param     string
 Chuỗi string muốn kiểm tra
 @return    YES nếu chuỗi string có nằm trong chuỗi có sẵn. NO nếu ngược lại.
 */
- (BOOL)containsString:(NSString *)string;

-(NSMutableString*)appendTo:(NSMutableString*)source;

/**Kiểm tra xem chuỗi string có tồn tại trong chuỗi có sẵn hay không có xác định HOA, thường.
 @author    nhantd
 @param     string
 Chuỗi string muốn kiểm tra
 @param     flag
 Cờ kiểm tra viết HOA viết thường
 @return    YES nếu chuỗi string có nằm trong chuỗi có sẵn. NO nếu ngược lại.
 */
- (BOOL)containsString:(NSString *)string ignoringCase:(BOOL)flag;


/**Đếm số từ trong chuỗi có sẵn.
 @author    nhantd
 @return    NSInteger Số từ có trong chuỗi string.
 */
- (NSInteger)countNumberWordsInString;


/**Xoá khoảng trắng và xuống dòng trong chuỗi có sẵn.
 @author    nhantd
 @return    NSString chuỗi sau khi xoá bỏ khoảng trắng và xuống dòng.
 */
- (NSString *)trims;


/**Gửi chuỗi đến pasteboard.
 @author    nhantd
 @return    void.
 */
- (void)sendToPasteboard;


/**Đếm số lần xuất hiện của chuỗi subString trong chuỗi có sẵn.
 @author    nhantd
 @param     subString
 Chuỗi subString muốn đếm số lần xuất hiện
 @return    NSInteger Số lần xuất hiện của subString trong chuỗi có sẵn.
 */
- (NSInteger)countOccurrencesOfSubString:(NSString *)subString;


/**Chuyển định dạng chuỗi string sang chuỗi định dạng currency.
 @author    nhantd
 @param     currentSymbol
 Định dạng tiền tệ. Ex : VND, $
 @return    NSString chuỗi đã chuyển sang định dạng currency. Ex : 1,000,000.000.
 */
- (NSString *)convertToCurrencyWithCurrency:(NSString *)currencySymbol;


/**Chuyển định dạng number sang chuỗi định dạng currency.
 @author    nhantd
 @param     number
 Số muốn chuyển đổi định dạng currency
 @param     currentSymbol
 Định dạng tiền tệ. Ex : VND, $
 @return    NSString chuỗi đã chuyển sang định dạng currency. Ex : 1,000,000.000.
 */
+ (NSString *)convertToCurrencyWithNumber:(NSNumber *)number withCurrency:(NSString *)currencySymbol;


/**So sánh chuỗi unicode với chuỗi string không unicode.
 @author    nhantd
 @param     unicodeString
 Chuỗi unicode
 @param     string
 Chuỗi không unicode
 @return    NSComparisonResult NSOrderedSame nếu 2 chuỗi tương đương nhau. Các trường hợp còn lại là 2 chuỗi khác nhau.
 */
+ (NSComparisonResult)compareStringsUnicode:(NSString *)unicodeString withString:(NSString *)string;


/**Trả về chuỗi string hoặc rỗng nếu chuỗi là null.
 @author    nhantd
 @return    NSString thay đổi chuỗi nếu chuỗi là null.
 */
- (NSString *)getStringOrEmpty;


/**Chuyển đổi chuỗi vietnamese thành chuỗi ascii.
 @author    nhantd
 @param     string
 Chuỗi vietnamese
 @return    NSString chuỗi sau khi đã chuyển đổi.
 */
+ (NSString *)convertASCIIStringFromVietnameseString:(NSString *)string;


/**Encode URL.
 @author    nhantd
 @return    NSString chuỗi URL sau khi đã encode.
 */
- (NSString *)urlEncode;


/**Lấy ra chuỗi con của chuỗi xác định trước bắt đầu từ vị trí start đến vị trí end.
 @author    nhantd
 @param     start
 Vị trí bắt đầu
 @param     end
 Vị trí kết thúc
 @return    NSString chuỗi đã tách.
 */
- (NSString *)substringFrom:(NSInteger)start to:(NSInteger)end;


/**Đảo ngược chuỗi xác định trước.
 @author    nhantd
 @param     string
 Chuỗi vietnamese
 @return    NSString chuỗi sau khi đã chuyển đổi.
 */
- (NSString *)reverse;


/**Kiểm tra xem chuỗi nhập vào có phải là chuỗi đối xứng hay không.
 @author    nhantd
 @return    YES nếu chuỗi nhập vào là chuỗi đối xứng.
 NO nếu ngược lại.
 */
- (BOOL)isSymmetric;

#pragma mark - Chuyển String sang NSDate
/**Kiểm tra xem chuỗi bắt đầu từ vị trí position của chuỗi nhập vào có phải là chuỗi đối xứng hay không.
 @author    chuongpd
 @param     string
 Chuỗi nhập vào
 @return    NSDate nếu chuỗi nhập hợp lệ.
 Nil nếu chuỗi nhập không hợp lệ.
 */
- (NSDate*)toDate;

#pragma mark - Chuyển String sang NSDictionary
/**Chuyển chuỗi có cấu trúc Json sang Dictionnary.
 @author    chuongpd
 @param     string
 Chuỗi nhập vào
 @return    NSDictionary nếu chuỗi nhập hợp lệ.
 Nil nếu chuỗi nhập không hợp lệ.
 */
-(NSDictionary*)toDictionary;

/**Kiểm tra xem chuỗi nhập vào có phải là chuỗi đối xứng hay không.
 @author    nhantd
 @param     string
 Chuỗi nhập vào
 @return    YES nếu chuỗi nhập vào là chuỗi đối xứng.
 NO nếu ngược lại.
 */
+ (BOOL)isSymmetric:(NSString *)string;

#pragma mark - Class Private

/**Kiểm tra xem chuỗi bắt đầu từ vị trí position của chuỗi nhập vào có phải là chuỗi đối xứng hay không.
 @author    nhantd
 @param     string
 Chuỗi nhập vào
 @param     position
 Vị trí bắt đầu
 @return    YES nếu chuỗi nhập vào là chuỗi đối xứng.
 NO nếu ngược lại.
 */
+ (BOOL)isSymmetric:(NSString *)string startAt:(NSInteger)position;

@end
