//
//  NSErrorUtils.m
//  SmartHospital
//
//  Created by Trinh Duy Nhan on 4/26/14.
//  Copyright (c) 2014 ViettelICT. All rights reserved.
//

#import "NSErrorUtils.h"

@implementation NSErrorUtils

+ (NSString *)getMessageLocalizationFromErrorCode:(int)code
{
    NSString *message = @"";
    switch (code) {
        case -1:
            message = @"";
            break;
            
        default:
            break;
    }
    return message;
}

@end
