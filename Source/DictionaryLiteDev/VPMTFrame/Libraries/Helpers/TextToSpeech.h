//
//  TextToSpeech.h
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/16/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//
#define Arabic_Saudi_Arabia @"ar-SA"
#define Chinese_China @"zh-CN"
#define Chinese_Hong_Kong_SAR_China @"zh-HK"
#define Chinese_Taiwan @"zh-TW"
#define Czech_Czech_Republic @"cs-CZ"
#define Danish_Denmark @"da-DK"
#define Dutch_Belgium @"nl-BE"
#define Dutch_Netherlands @"nl-NL"
#define English_Australia @"en-AU"
#define English_Ireland @"en-IE"
#define English_South_Africa @"en-ZA"
#define English_United_Kingdom @"en-GB"
#define English_United_States @"en-US"
#define Finnish_Finland @"fi-FI"
#define French_Canada @"fr-CA"
#define French_France @"fr-FR"
#define German_Germany @"de-DE"
#define Greek_Greece @"el-GR"
#define Hindi_India @"hi-IN"
#define Hungarian_Hungary @"hu-HU"
#define Indonesian_Indonesia @"id-ID"
#define Italian_Italy @"it-IT"
#define Japanese_Japan @"ja-JP"
#define Korean_South_Korea @"ko-KR"
#define Norwegian_Norway @"no-NO"
#define Polish_Poland @"pl-PL"
#define Portuguese_Brazil @"pt-BR"
#define Portuguese_Portugal @"pt-PT"
#define Romanian_Romania @"ro-RO"
#define Russian_Russia @"ru-RU"
#define Slovak_Slovakia @"sk-SK"
#define Spanish_Mexico @"es-MX"
#define Spanish_Spain @"es-ES"
#define Swedish_Sweden @"sv-SE"
#define Thai_Thailand @"th-TH"
#define Turkish_Turkey @"tr-TR"
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
@interface TextToSpeech : NSObject <AVSpeechSynthesizerDelegate>
@property (nonatomic, strong) AVSpeechSynthesizer *synthesizer;
@property (nonatomic, strong) AVSpeechUtterance *utterance;
+ (id)sharedManager ;
- (void)speakText:(NSString*)text andLangue:(NSString*)languge;
@end
