//
//  CryptUtils.h
//  ViettelPI
//
//  Created by Trinh Duy Nhan on 3/28/14.
//  Copyright (c) 2014 viettel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CryptUtils : NSObject

/**Mã hoá AES256 chuỗi text nhập vào xác định bởi key mã hoá và dữ liệu vector khởi tạo để sắp xếp ngẫu nhiên chuỗi mã hoá.
 @author    nhantd
 @param     text
 Chuỗi nhập vào
 @param     key
 Key mã hoá
 @param     ivdata
 Vector khởi tạo (có thể là NULL)
 @return    NSString chuỗi đã mã hoá AES256.
 */
+ (NSString *)encryptAES256WithString:(NSString *)text withKey:(NSString *)key withIV:(NSData *)ivdata;


/**Giải mã AES256 chuỗi text nhập vào xác định bởi key mã hoá và dữ liệu vector khởi tạo để sắp xếp ngẫu nhiên chuỗi mã hoá.
 @author    nhantd
 @param     text
 Chuỗi nhập vào
 @param     key
 Key mã hoá
 @param     ivdata
 Vector khởi tạo (có thể là NULL)
 @return    NSString chuỗi đã giải mã AES256.
 */
+ (NSString *)decryptAES256WithString:(NSString *)text withKey:(NSString *)key withIV:(NSData *)ivdata;


/**Mã hoá AES128 chuỗi text nhập vào xác định bởi key mã hoá và dữ liệu vector khởi tạo để sắp xếp ngẫu nhiên chuỗi mã hoá.
 @author    nhantd
 @param     text
 Chuỗi nhập vào
 @param     key
 Key mã hoá
 @param     ivdata
 Vector khởi tạo (có thể là NULL)
 @return    NSString chuỗi đã mã hoá AES128.
 */
+ (NSString *)encryptAES128WithString:(NSString *)text withKey:(NSString *)key withIV:(NSData *)ivdata;


/**Giải mã AES128 chuỗi text nhập vào xác định bởi key mã hoá và dữ liệu vector khởi tạo để sắp xếp ngẫu nhiên chuỗi mã hoá.
 @author    nhantd
 @param     text
 Chuỗi nhập vào
 @param     key
 Key mã hoá
 @param     ivdata
 Vector khởi tạo (có thể là NULL)
 @return    NSString chuỗi đã giải mã AES128.
 */
+ (NSString *)decryptAES128WithString:(NSString *)text withKey:(NSString *)key withIV:(NSData *)ivdata;


/**Mã hoá DES chuỗi text nhập vào xác định bởi key mã hoá và dữ liệu vector khởi tạo để sắp xếp ngẫu nhiên chuỗi mã hoá.
 @author    nhantd
 @param     text
 Chuỗi nhập vào
 @param     key
 Key mã hoá
 @param     ivdata
 Vector khởi tạo (có thể là NULL)
 @return    NSString chuỗi đã mã hoá DES.
 */
+ (NSString *)encryptDESWithString:(NSString *)text withKey:(NSString *)key withIV:(NSData *)ivdata;


/**Giải mã DES chuỗi text nhập vào xác định bởi key mã hoá và dữ liệu vector khởi tạo để sắp xếp ngẫu nhiên chuỗi mã hoá.
 @author    nhantd
 @param     text
 Chuỗi nhập vào
 @param     key
 Key mã hoá
 @param     ivdata
 Vector khởi tạo (có thể là NULL)
 @return    NSString chuỗi đã giải mã DES.
 */
+ (NSString *)decryptDESWithString:(NSString *)text withKey:(NSString *)key withIV:(NSData *)ivdata;


/**Mã hoá 3DES chuỗi text nhập vào xác định bởi key mã hoá và dữ liệu vector khởi tạo để sắp xếp ngẫu nhiên chuỗi mã hoá.
 @author    nhantd
 @param     text
 Chuỗi nhập vào
 @param     key
 Key mã hoá
 @param     ivdata
 Vector khởi tạo (có thể là NULL)
 @return    NSString chuỗi đã mã hoá 3DES.
 */
+ (NSString *)encryptTripleDESWithString:(NSString *)text withKey:(NSString *)key withIV:(NSData *)ivdata;


/**Giải mã 3DES chuỗi text nhập vào xác định bởi key mã hoá và dữ liệu vector khởi tạo để sắp xếp ngẫu nhiên chuỗi mã hoá.
 @author    nhantd
 @param     text
 Chuỗi nhập vào
 @param     key
 Key mã hoá
 @param     ivdata
 Vector khởi tạo (có thể là NULL)
 @return    NSString chuỗi đã giải mã 3DES.
 */
+ (NSString *)decryptTripleDESWithString:(NSString *)text withKey:(NSString *)key withIV:(NSData *)ivdata;


/**Mã hoá AES256 dữ liệu đưa vào xác định bởi key mã hoá và dữ liệu vector khởi tạo để sắp xếp ngẫu nhiên chuỗi mã hoá.
 @author    nhantd
 @param     data
 Dữ liệu đưa vào
 @param     key
 Key mã hoá
 @param     ivdata
 Vector khởi tạo (có thể là NULL)
 @return    NSData dữ liệu đã mã hoá AES256.
 */
+ (NSData *)encryptAES256WithData:(NSData *)data withKey:(NSString *)key withIV:(NSData *)ivdata;


/**Giải mã AES256 dữ liệu đưa vào xác định bởi key mã hoá và dữ liệu vector khởi tạo để sắp xếp ngẫu nhiên chuỗi mã hoá.
 @author    nhantd
 @param     data
 Dữ liệu đưa vào
 @param     key
 Key mã hoá
 @param     ivdata
 Vector khởi tạo (có thể là NULL)
 @return    NSData dữ liệu đã giải mã AES256.
 */
+ (NSData *)decryptAES256WithData:(NSData *)data withKey:(NSString *)key withIV:(NSData *)ivdata;


/**Mã hoá AES128 dữ liệu đưa vào xác định bởi key mã hoá và dữ liệu vector khởi tạo để sắp xếp ngẫu nhiên chuỗi mã hoá.
 @author    nhantd
 @param     data
 Dữ liệu đưa vào
 @param     key
 Key mã hoá
 @param     ivdata
 Vector khởi tạo (có thể là NULL)
 @return    NSData dữ liệu đã mã hoá AES128.
 */
+ (NSData *)encryptAES128WithData:(NSData *)data withKey:(NSString *)key withIV:(NSData *)ivdata;


/**Giải mã AES128 dữ liệu đưa vào xác định bởi key mã hoá và dữ liệu vector khởi tạo để sắp xếp ngẫu nhiên chuỗi mã hoá.
 @author    nhantd
 @param     data
 Dữ liệu đưa vào
 @param     key
 Key mã hoá
 @param     ivdata
 Vector khởi tạo (có thể là NULL)
 @return    NSData dữ liệu đã giải mã AES128.
 */
+ (NSData *)decryptAES128WithData:(NSData *)data withKey:(NSString *)key withIV:(NSData *)ivdata;


/**Mã hoá DES dữ liệu đưa vào xác định bởi key mã hoá và dữ liệu vector khởi tạo để sắp xếp ngẫu nhiên chuỗi mã hoá.
 @author    nhantd
 @param     data
 Dữ liệu đưa vào
 @param     key
 Key mã hoá
 @param     ivdata
 Vector khởi tạo (có thể là NULL)
 @return    NSData dữ liệu đã mã hoá DES.
 */
+ (NSData *)encryptDESWithData:(NSData *)data withKey:(NSString *)key withIV:(NSData *)ivdata;


/**Giải mã DES dữ liệu đưa vào xác định bởi key mã hoá và dữ liệu vector khởi tạo để sắp xếp ngẫu nhiên chuỗi mã hoá.
 @author    nhantd
 @param     data
 Dữ liệu đưa vào
 @param     key
 Key mã hoá
 @param     ivdata
 Vector khởi tạo (có thể là NULL)
 @return    NSData dữ liệu đã giải mã DES.
 */
+ (NSData *)decryptDESWithData:(NSData *)data withKey:(NSString *)key withIV:(NSData *)ivdata;


/**Mã hoá 3DES dữ liệu đưa vào xác định bởi key mã hoá và dữ liệu vector khởi tạo để sắp xếp ngẫu nhiên chuỗi mã hoá.
 @author    nhantd
 @param     data
 Dữ liệu đưa vào
 @param     key
 Key mã hoá
 @param     ivdata
 Vector khởi tạo (có thể là NULL)
 @return    NSData dữ liệu đã mã hoá 3DES.
 */
+ (NSData *)encryptTripleDESWithData:(NSData *)data withKey:(NSString *)key withIV:(NSData *)ivdata;


/**Giải mã 3DES dữ liệu đưa vào xác định bởi key mã hoá và dữ liệu vector khởi tạo để sắp xếp ngẫu nhiên chuỗi mã hoá.
 @author    nhantd
 @param     data
 Dữ liệu đưa vào
 @param     key
 Key mã hoá
 @param     ivdata
 Vector khởi tạo (có thể là NULL)
 @return    NSData dữ liệu đã giải mã 3DES.
 */
+ (NSData *)decryptTripleDESWithData:(NSData *)data withKey:(NSString *)key withIV:(NSData *)ivdata;


/**Mã hoá Base64 dữ liệu đưa vào.
 @author    nhantd
 @param     data
 Dữ liệu đưa vào
 @return    NSString chuỗi đã mã hoá Base64.
 */
+ (NSString *)encodeBase64WithData:(NSData *)data;


/**Mã hoá Base64 dữ liệu đưa vào xác định thêm tham số lineLength.
 @author    nhantd
 @param     data
 Dữ liệu đưa vào
 @param     lineLength
 Độ dài mỗi line của dữ liệu mã hoá.
 @return    NSString chuỗi đã mã hoá Base64.
 */
+ (NSString *)encodeBase64WithData:(NSData *)data withLineLength:(NSUInteger)lineLength;


/**Mã hoá Base64 chuỗi text nhập vào.
 @author    nhantd
 @param     text
 Chuỗi nhập vào
 @return    NSData dữ liệu đã mã hoá Base64.
 */
+ (NSData *)encodeBase64WithString:(NSString *)text;


/**Mã hoá Hex chuỗi text nhập vào.
 @author    nhantd
 @param     text
 Chuỗi nhập vào
 @return    NSString chuỗi đã mã hoá Hex.
 */
+ (NSString *)encodeHexStringWithString:(NSString *)text;


/**Giải mã Hex chuỗi text (dạng Hex) nhập vào.
 @author    nhantd
 @param     text
 Chuỗi nhập vào
 @return    NSString chuỗi đã giải mã Hex.
 */
+ (NSString *)decodeHexStringWithString:(NSString *)hexText;


/**Mã hoá XOR chuỗi text nhập vào với key xác định trước.
 @author    nhantd
 @param     text
 Chuỗi nhập vào
 @param     key
 Khoá để mã hoá
 @return    NSString chuỗi đã mã hoá XOR.
 */
+ (NSString *)encodeStringXOR:(NSString *)text withKey:(NSString *)key;


/**Mã hoá XOR data đưa vào với key xác định trước.
 @author    nhantd
 @param     data
 Mảng dữ liệu đưa vào
 @param     key
 Khoá để mã hoá
 @return    NSData mảng dữ liệu đã mã hoá XOR.
 */
+ (NSData *)encodeDataXOR:(NSData *)data withKey:(NSString *)key;


/**Mã hoá MD4 chuỗi text nhập vào.
 @author    nhantd
 @param     text
 Chuỗi nhập vào
 @return    NSString chuỗi đã mã hoá MD4.
 */
+ (NSString *)encryptMD4WithString:(NSString *)text;


/**Mã hoá MD5 chuỗi text nhập vào.
 @author    nhantd
 @param     text
 Chuỗi nhập vào
 @return    NSString chuỗi đã mã hoá MD5.
 */
+ (NSString *)encryptMD5WithString:(NSString *)text;


/**Mã hoá MD5 chuỗi text nhập vào xác định bởi key mã hoá.
 @author    nhantd
 @param     text
 Chuỗi nhập vào
 @param     key
 Key mã hoá
 @return    NSString chuỗi đã mã hoá MD5.
 */
+ (NSString *)encryptMD5WithString:(NSString *)text withKey:(NSString *)key;


/**Mã hoá SHA1 chuỗi text nhập vào.
 @author    nhantd
 @param     text
 Chuỗi nhập vào
 @return    NSString chuỗi đã mã hoá SHA1.
 */
+ (NSString *)encryptSHA1WithString:(NSString *)text;


/**Mã hoá SHA1 chuỗi text nhập vào xác định bởi key mã hoá.
 @author    nhantd
 @param     text
 Chuỗi nhập vào
 @param     key
 Key mã hoá
 @return    NSString chuỗi đã mã hoá SHA1.
 */
+ (NSString *)encryptSHA1WithString:(NSString *)text withKey:(NSString *)key;


/**Mã hoá SHA256 chuỗi text nhập vào.
 @author    nhantd
 @param     text
 Chuỗi nhập vào
 @return    NSString chuỗi đã mã hoá SHA256.
 */
+ (NSString *)encryptSHA256WithString:(NSString *)text;


/**Mã hoá SHA256 chuỗi text nhập vào xác định bởi key mã hoá.
 @author    nhantd
 @param     text
 Chuỗi nhập vào
 @param     key
 Key mã hoá
 @return    NSString chuỗi đã mã hoá SHA256.
 */
+ (NSString *)encryptSHA256WithString:(NSString *)text withKey:(NSString *)key;


/**Mã hoá SHA512 chuỗi text nhập vào.
 @author    nhantd
 @param     text
 Chuỗi nhập vào
 @return    NSString chuỗi đã mã hoá SHA512.
 */
+ (NSString *)encryptSHA512WithString:(NSString *)text;


/**Mã hoá SHA512 chuỗi text nhập vào xác định bởi key mã hoá.
 @author    nhantd
 @param     text
 Chuỗi nhập vào
 @param     key
 Key mã hoá
 @return    NSString chuỗi đã mã hoá SHA512.
 */
+ (NSString *)encryptSHA512WithString:(NSString *)text withKey:(NSString *)key;


/**Tạo instance vector cho mã hoá AES128 và mã hoá AES256 (16 bit).
 @author    nhantd
 @return    NSData dữ liệu instance vector 16 bit mã hoá AES.
 */
+ (NSData *)generateDataInstanceVectorForAES;


/**Tạo instance vector cho mã hoá AES128 và mã hoá AES256 (16 bit).
 @author    nhantd
 @return    NSString chuỗi instance vector 16 bit mã hoá AES.
 */
+ (NSString *)generateStringInstanceVectorForAES;


/**Tạo instance vector cho mã hoá DES và mã hoá 3DES (8 bit).
 @author    nhantd
 @return    NSData dữ liệu instance vector 16 bit mã hoá DES.
 */
+ (NSData *)generateDataInstanceVectorForDES;


/**Tạo instance vector cho mã hoá DES và mã hoá 3DES (8 bit).
 @author    nhantd
 @return    NSString chuỗi instance vector 8 bit mã hoá DES.
 */
+ (NSString *)generateStringInstanceVectorForDES;

@end
