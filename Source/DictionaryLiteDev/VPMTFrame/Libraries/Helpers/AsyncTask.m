//
//  SqliteHelper.m
//  VPQH_VietLaw
//
//  Created by viettel on 7/24/13.
//  Copyright (c) 2013 Tran Hoang Hiep. All rights reserved.
//

#import "AsyncTask.h"
#import "MBProgressHUD.h"

@implementation AsyncTask

static AsyncTask* sharedManager = nil;

+ (instancetype)sharedManager {
    @synchronized(self)
    {
        if (sharedManager && [sharedManager isKindOfClass:[self class]]){
            return sharedManager;
        }else{
            sharedManager = [[self alloc] init];
            return sharedManager;
        }
    }
}

+ (void)execute:(AsynTaskResultBlock)doInBackground
       onFinishWithResult:(AsynTaskParamBlock)finish
{
    dispatch_queue_t dispatch_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(dispatch_queue, ^{
        if (doInBackground) {
            NSArray *result =  doInBackground();
            dispatch_async(dispatch_get_main_queue(), ^{
                if (finish) {
                    finish(result);
                }
            });
        }
    });
}
+ (void)execute:(AsynTaskBlock)doInBackground
      onFinish:(AsynTaskBlock)finish
{
    dispatch_queue_t dispatch_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(dispatch_queue, ^{
        if (doInBackground) {
            doInBackground();
            dispatch_async(dispatch_get_main_queue(), ^{
                if (finish) {
                    finish();
                }
            });
        }
    });
}

+ (void)execute:(AsynTaskBlock)doInBackground
       onFinish:(AsynTaskBlock)finish parent:(UIView*)view showLoadingBox:(BOOL)showLoadingBox
{
    dispatch_queue_t dispatch_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(dispatch_queue, ^{
        if (doInBackground) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (showLoadingBox) {
                    [MBProgressHUD showHUDAddedTo:view animated:YES];
                }
            });
            doInBackground();
            dispatch_async(dispatch_get_main_queue(), ^{
                if (finish) {
                    finish();
                    [MBProgressHUD hideHUDForView:view animated:YES];
                }
            });
        }
    });
}

+ (void)execute:(AsynTaskBlock)doInBackground
        onFinish:(AsynTaskBlock)finish parent:(UIView*)view
        showLoadingBox:(BOOL)showLoadingBox
        loadingText:(NSString*)text
{
    dispatch_queue_t dispatch_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(dispatch_queue, ^{
        if (doInBackground) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (showLoadingBox) {
                    [MBProgressHUD showHUDWithString:text addedTo:view animated:YES];
                }
            });
            doInBackground();
            dispatch_async(dispatch_get_main_queue(), ^{
                if (finish) {
                    finish();
                    [MBProgressHUD hideHUDForView:view animated:YES];
                }
            });
        }
    });
}

+ (void)execute:(AsynTaskBlock)doInBackground
       onFinish:(AsynTaskBlock)finish afterDelay:(NSTimeInterval)interval
{
    dispatch_queue_t dispatch_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(dispatch_queue, ^{
        if (doInBackground) {
            sleep(interval);
            doInBackground();
            dispatch_async(dispatch_get_main_queue(), ^{
                if (finish) {
                    finish();
                }
            });
        }
    });
}

+ (void)execute:(AsynTaskBlock)doInBackground
       onFinish:(AsynTaskBlock)finish afterDelay:(NSTimeInterval)interval repeatTime:(NSTimeInterval)repeatTime taskName:(NSString*)taskName;
{
    if (doInBackground) {
        [self execute:^{
            if ([UserDefault getObjectWithKey:taskName] == nil) {
                //Allow task repeat
                NSString *allowRepeat = [NSString stringWithFormat:@"%@AllowRepeat",taskName];
                [UserDefault addBoolForKey:YES key:allowRepeat];
                
                [UserDefault addObjectForKey:[NSNumber numberWithInt:1] key:taskName];
                
                double count = 0;
                while (count < interval) {
                    count+=0.01;
                    
                    //Stop task when Update or Cancel
                    NSString *allowRepeat = [NSString stringWithFormat:@"%@AllowRepeat",taskName];
                    if ([UserDefault getBoolWithKey:allowRepeat] == NO) {
                        [UserDefault removeObjectForKey:allowRepeat];
                        [UserDefault removeObjectForKey:taskName];
                        return;
                    }
                    
                    [NSThread sleepForTimeInterval:0.01];
                }
                count = 0;
            }
            
            if ([[UserDefault getObjectWithKey:taskName] intValue] == 1) {
                doInBackground();
            }
        } onFinish:^{
                if (finish) {
                    finish();
                }
                if ([[UserDefault getObjectWithKey:taskName] intValue] == 1) {
                [self execute:^{
                    if (repeatTime > 0) {
                        //waiting repeat
                        [UserDefault addObjectForKey:[NSNumber numberWithInt:2] key:taskName];
                        
                        double count = 0;
                        while (count < repeatTime) {
                            count+=0.01;
                           
                            //Stop task when Update or Cancel
                            NSString *allowRepeat = [NSString stringWithFormat:@"%@AllowRepeat",taskName];
                            if ([UserDefault getBoolWithKey:allowRepeat] == NO) {
                                [UserDefault removeObjectForKey:allowRepeat];
                                [UserDefault removeObjectForKey:taskName];
                                return;
                            }
                            
                            [NSThread sleepForTimeInterval:0.01];
                        }
                        count = 0;
                        
                        //fire task
                        [UserDefault addObjectForKey:[NSNumber numberWithInt:1] key:taskName];
                        [self execute:doInBackground onFinish:finish afterDelay:0 repeatTime:repeatTime taskName:taskName];
                    }
                } onFinish:^{
                    
                }];
            }
        }];
    }
}


+ (void)cancelTask:(NSString*)taskName{
    if ([UserDefault getObjectWithKey:taskName] != nil) {
       [UserDefault removeObjectForKey:taskName];
        NSString *allowRepeat = [NSString stringWithFormat:@"%@AllowRepeat",taskName];
        [UserDefault addBoolForKey:NO key:allowRepeat];
        [UserDefault removeObjectForKey:allowRepeat];
    }
}

+ (void)cancelAllTask{
    
}
@end
