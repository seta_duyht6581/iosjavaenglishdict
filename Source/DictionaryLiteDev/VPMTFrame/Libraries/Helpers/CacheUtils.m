//
//  CacheUtils.m
//
//  Created by Chuongpd on 17/6/2014.
//  Copyright (c) 2014 viettel. All rights reserved.
//

#import "CacheUtils.h"

@implementation CacheUtils

NSString *imageFolder = @"IMAGE_FOLDER";
NSString *videoFolder = @"VIDEO_FOLDER";

static CacheUtils* sharedManager = nil;
+ (instancetype)sharedManager {
    @synchronized(self)
    {
        if (sharedManager && [sharedManager isKindOfClass:[self class]]){
            return sharedManager;
        }else{
            sharedManager = [[self alloc] init];
            return sharedManager;
        }
    }
}

-(void)clearAllCache
{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    //delete disk cache
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *myPath    = [myPathList  objectAtIndex:0];
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    if ([fileManager fileExistsAtPath:myPath])
    {
        NSString *imageCachePath = [NSString stringWithFormat:@"%@/%@",myPath, imageFolder];
        [fileManager removeItemAtPath:imageCachePath error:NULL];
        
        NSString *videoCachePath = [NSString stringWithFormat:@"%@/%@",myPath, videoFolder];
        [fileManager removeItemAtPath:imageCachePath error:NULL];
        
        videoCachePath = nil;
        imageCachePath = nil;
    }
}

-(void)removeVideoCache:(NSString*)videoURL{
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *myPath    = [myPathList  objectAtIndex:0];
    
    NSString *filename = [CryptUtils encryptMD5WithString:videoURL];
    
    // create full file path
    NSString *filePath = [myPath stringByAppendingString:[NSString stringWithFormat:@"/%@/%@",videoFolder,filename]];
    
    [FileUtils removeFile:filePath];
}

-(void)removeImageCache:(NSString*)imageURL{
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *myPath    = [myPathList  objectAtIndex:0];
    
    NSString *filename = [CryptUtils encryptMD5WithString:imageURL];
    
    // create full file path
    NSString *filePath = [myPath stringByAppendingString:[NSString stringWithFormat:@"/%@/%@",imageFolder,filename]];
    
    [FileUtils removeFile:filePath];
}

-(void)removeAllImage{
    NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *imageCachePath = [[myPathList  objectAtIndex:0] stringByAppendingString:[NSString stringWithFormat:@"/%@",imageFolder]];
    
    [FileUtils removeFile:imageCachePath];
}

/*
 -(void)setImage1:(UIImageView*)imageView url:(NSString*)url defaultImage:(UIImage*)defaultImage{
 if (imageView != nil) {
 __weak UIImageView *weakImageView = imageView;
 
 [imageView setImageWithURLRequest:[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]]
 placeholderImage:defaultImage
 success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image){
 [weakImageView setImage:image];
 [weakImageView setNeedsLayout];
 }
 failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
 }];
 }
 }
 */

- (void)getImageThumbWithCache:(NSString *)imageURL onSuccess:(void (^)(UIImage *))success
{
    
    __block UIImage *imageCache;
    [AsyncTask execute:^{
        @autoreleasepool {
            NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
            NSString *myPath    = [myPathList  objectAtIndex:0];
            
            NSString *filename = [CryptUtils encryptMD5WithString:imageURL];
            
            // create full file path
            NSString *filePath = [myPath stringByAppendingString:[NSString stringWithFormat:@"/%@/%@",imageFolder,filename]];
            
            // check is file in cache
            NSData *data = [[NSData alloc] initWithContentsOfFile:filePath];
            
            if (!data) {
                // if not in cache, we just load from URL
                data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:imageURL]];
                
                if (data) {
                    UIImage *originalImage = [[UIImage alloc] initWithData: data];
                    CGSize destinationSize = CGSizeMake(100, 100);
                    UIGraphicsBeginImageContext(destinationSize);
                    [originalImage drawInRect:CGRectMake(0,0,destinationSize.width,destinationSize.height)];
                    imageCache = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    originalImage = nil;
                    
                    data = UIImagePNGRepresentation(imageCache);
                    
                    // and save to disk
                    [FileUtils createSubCacheFolder:imageFolder];
                    [data writeToFile:filePath atomically:NO];
                }
            }else{
                imageCache = [[UIImage alloc] initWithData: data];
            }
            
            data = nil;
            filePath = nil;
            myPath = nil;
            myPathList = nil;
        }
    } onFinish:^{
        @autoreleasepool {
            if(imageCache && success){
                success(imageCache);
            }
        }
    }];
}

- (void)getImageThumbNoCache:(NSString *)imageURL onSuccess:(void (^)(UIImage *))success
{
    
    __block UIImage *imageCache;
    [AsyncTask execute:^{
        @autoreleasepool {
            NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:imageURL]];
            
            if (data) {
                UIImage *originalImage = [[UIImage alloc] initWithData: data];
                CGSize destinationSize = CGSizeMake(100, 100);
                UIGraphicsBeginImageContext(destinationSize);
                [originalImage drawInRect:CGRectMake(0,0,destinationSize.width,destinationSize.height)];
                imageCache = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                originalImage = nil;
            }
            
            data = nil;
        }
    } onFinish:^{
        @autoreleasepool {
            if(imageCache && success){
                success(imageCache);
            }
        }
    }];
}

- (void)getImageNoCache:(NSString *)imageURL onSuccess:(void(^)(UIImage* image))success
{
    __block UIImage *image;
    [AsyncTask execute:^{
        @autoreleasepool {
            NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:imageURL]];
            
            if (data) {
                UIImage *originalImage = [[UIImage alloc] initWithData: data];
                CGSize destinationSize = originalImage.size;
                UIGraphicsBeginImageContext(destinationSize);
                [originalImage drawInRect:CGRectMake(0,0,destinationSize.width,destinationSize.height)];
                image = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                originalImage = nil;
            }
            data = nil;
        }
    } onFinish:^{
        @autoreleasepool {
            if(image && success){
                success(image);
            }
        }
    }];
}

- (void)getImageWithCache:(NSString *)imageURL onSuccess:(void(^)(UIImage* image))success
{
    __block UIImage *imageCache;
    [AsyncTask execute:^{
        @autoreleasepool {
            NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
            NSString *myPath    = [myPathList  objectAtIndex:0];
            
            NSString *filename = [CryptUtils encryptMD5WithString:imageURL];
            
            // create full file path
            NSString *filePath = [myPath stringByAppendingString:[NSString stringWithFormat:@"/%@/%@",imageFolder,filename]];
            
            // check is file in cache
            NSData *data = [[NSData alloc] initWithContentsOfFile:filePath];
            
            if (!data) {
                // if not in cache, we just load from URL
                data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:imageURL]];
                
                UIImage *originalImage = [[UIImage alloc] initWithData: data];
                CGSize destinationSize = originalImage.size;
                UIGraphicsBeginImageContext(destinationSize);
                [originalImage drawInRect:CGRectMake(0,0,destinationSize.width,destinationSize.height)];
                imageCache = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                originalImage = nil;
                
                data = UIImagePNGRepresentation(imageCache);
                
                if (data) {
                    // and save to disk
                    [FileUtils createSubCacheFolder:imageFolder];
                    [data writeToFile:filePath atomically:NO];
                }
            }else{
                imageCache = [[UIImage alloc] initWithData: data];
            }
            
            data = nil;
            filePath = nil;
            myPath = nil;
            myPathList = nil;
        }
    } onFinish:^{
        @autoreleasepool {
            if(imageCache && success){
                success(imageCache);
            }
        }
    }];
}

- (void)getVideo:(NSString *)videoURL onSuccess:(void(^)(NSData* videoData))success
{
    __block NSData *videoCache;
    [AsyncTask execute:^{
        NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *myPath    = [myPathList  objectAtIndex:0];
        
        NSString *filename = [CryptUtils encryptMD5WithString:videoURL];
        
        // create full file path
        NSString *filePath = [myPath stringByAppendingString:[NSString stringWithFormat:@"/%@/%@",videoFolder,filename]];
        
        // check is file in cache
        videoCache = [[NSData alloc] initWithContentsOfFile:filePath];
        
        if (!videoCache) {
            // if not in cache, we just load from URL
            videoCache = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:videoURL]];
            
            if (videoCache) {
                [FileUtils createSubCacheFolder:videoFolder];
                // and save to disk
                [videoCache writeToFile:filePath atomically:NO];
            }
        }
    } onFinish:^{
        if(videoCache && success){
            success(videoCache);
        }
    }];
}
/*
 - (UIImage*)getImage:(NSString *)imageURL
 {
 NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
 NSString *myPath    = [myPathList  objectAtIndex:0];
 
 NSString *filename = [CryptUtils encryptMD5WithString:imageURL];
 
 // create full file path
 NSString *filePath = [myPath stringByAppendingString:[NSString stringWithFormat:@"/%@/%@",imageFolder,filename]];
 
 // check is file in cache
 NSData *data = [[NSData alloc] initWithContentsOfFile:filePath];
 
 if (!data) {
 // if not in cache, we just load from URL
 data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:imageURL]];
 
 if (data) {
 // and save to disk
 [data writeToFile:filePath atomically:NO];
 }
 }
 
 if (data) {
 return [[UIImage alloc] initWithData: data];
 }
 
 return nil;
 }
 
 - (NSData*)getVideo:(NSString *)videoURL
 {
 NSArray *myPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
 NSString *myPath    = [myPathList  objectAtIndex:0];
 
 NSString *filename = [CryptUtils encryptMD5WithString:videoURL];
 
 // create full file path
 NSString *filePath = [myPath stringByAppendingString:[NSString stringWithFormat:@"/%@/%@",imageFolder,filename]];
 
 // check is file in cache
 NSData *data = [[NSData alloc] initWithContentsOfFile:filePath];
 
 if (!data) {
 // if not in cache, we just load from URL
 data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:videoURL]];
 
 if (data) {
 // and save to disk
 [data writeToFile:filePath atomically:NO];
 }
 }
 
 if (data) {
 return data;
 }
 
 return nil;
 }
 */
@end
