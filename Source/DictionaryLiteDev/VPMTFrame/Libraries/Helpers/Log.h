//
//  ApiHelper.h
//
//  Created by viettel on 6/14/13.
//  Copyright (c) 2014 ChuongPD. All rights reserved.
//

#import <Foundation/Foundation.h>

#define LogMethod NSLog(@"%s-%d", __PRETTY_FUNCTION__, __LINE__);

#define LogMethodArgs(format, ... )  NSLog(@"%s: %@", __PRETTY_FUNCTION__, [NSString stringWithFormat:format, ##__VA_ARGS__] );

@interface Log : NSObject
+ (instancetype)sharedManager;
+(NSArray*)getIOSSystemLog;
@end
