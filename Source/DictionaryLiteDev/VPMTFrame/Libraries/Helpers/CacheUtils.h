//
//  CacheUtils.m
//
//  Created by ChuongpDn on 17/6/2014.
//  Copyright (c) 2014 viettel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CacheUtils : NSObject

+ (instancetype)sharedManager;

- (void)clearAllCache;

- (void)getImageThumbWithCache:(NSString *)imageURL onSuccess:(void (^)(UIImage *))success;
- (void)getImageThumbNoCache:(NSString *)imageURL onSuccess:(void (^)(UIImage *))success;
- (void)getImageNoCache:(NSString *)imageURL onSuccess:(void(^)(UIImage* image))success;
- (void)getImageWithCache:(NSString *)imageURL onSuccess:(void(^)(UIImage* image))success;
- (void)getVideo:(NSString *)videoURL onSuccess:(void(^)(NSData* videoData))success;

-(void)removeAllImage;
-(void)removeVideoCache:(NSString*)videoURL;
-(void)removeImageCache:(NSString*)imageURL;

@end
