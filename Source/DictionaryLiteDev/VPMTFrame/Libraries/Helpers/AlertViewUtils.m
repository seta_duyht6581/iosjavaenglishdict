//
//  AlertViewUtils.m
//  ViettelPI
//
//  Created by Trinh Duy Nhan on 3/27/14.
//  Copyright (c) 2014 viettel. All rights reserved.
//

#import "AlertViewUtils.h"

@implementation AlertViewUtils

UIAlertView *alertView;
UIAlertController *alertController;

+ (void)showAlertViewWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate withTag:(NSInteger)tag withTitleButtonCancel:(NSString *)titleButtonCancel withTitleButtonOther:(NSString *)otherButtontitle
{

    if (NSClassFromString(@"UIAlertController") != nil) {
        if (alertController) {
            [alertController dismissViewControllerAnimated:YES completion:nil];
            alertController = nil;
        }
        alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];

        

        
    }
    else {
        
        if (alertView) {
            [alertView dismissWithClickedButtonIndex:-1 animated:YES];
            [alertView removeFromSuperview];
            alertView = nil;
        }
        alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:titleButtonCancel otherButtonTitles:otherButtontitle, nil];
        alertView.tag = tag;
        [alertView setNeedsDisplay];
        [alertView show];
    }

}

+ (void)dissmis{
    if (alertView) {
        [alertView dismissWithClickedButtonIndex:-1 animated:YES];
        [alertView removeFromSuperview];
        alertView = nil;
    }
}

+ (void)showAlertViewTextInputWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate withTag:(NSInteger)tag withTitleButtonCancel:(NSString *)titleButtonCancel withTitleButtonOther:(NSString *)otherButtontitle
{
    if (alertView) {
        [alertView dismissWithClickedButtonIndex:-1 animated:YES];
        [alertView removeFromSuperview];
        alertView = nil;
    }
    alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:titleButtonCancel otherButtonTitles:otherButtontitle, nil];
    alertView.tag = tag;
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alertView setNeedsDisplay];
    [alertView show];
}


+ (void)showAlertViewTextInputWithTitle:(NSString *)title inputText:(NSString *)inputText message:(NSString *)message delegate:(id)delegate withTag:(NSInteger)tag withTitleButtonCancel:(NSString *)titleButtonCancel withTitleButtonOther:(NSString *)otherButtontitle
{
    if (alertView) {
        [alertView dismissWithClickedButtonIndex:-1 animated:YES];
        [alertView removeFromSuperview];
        alertView = nil;
    }
    alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:titleButtonCancel otherButtonTitles:otherButtontitle, nil];
    alertView.tag = tag;
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField *tf = [alertView textFieldAtIndex:0];
    [tf setText:inputText];
    [alertView setNeedsDisplay];
    [alertView show];
}


+ (void)showAlertViewPasswordInputWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate withTag:(NSInteger)tag withTitleButtonCancel:(NSString *)titleButtonCancel withTitleButtonOther:(NSString *)otherButtontitle
{
    if (alertView) {
        [alertView dismissWithClickedButtonIndex:-1 animated:YES];
        [alertView removeFromSuperview];
        alertView = nil;
    }
    alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:titleButtonCancel otherButtonTitles:otherButtontitle, nil];
    alertView.tag = tag;
    alertView.alertViewStyle = UIAlertViewStyleSecureTextInput;
    [alertView setNeedsDisplay];
    [alertView show];
}


+ (void)showAlertViewUserPasswordWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate withTag:(NSInteger)tag withTitleButtonCancel:(NSString *)titleButtonCancel withTitleButtonOther:(NSString *)otherButtontitle
{
    if (alertView) {
        [alertView dismissWithClickedButtonIndex:-1 animated:YES];
        [alertView removeFromSuperview];
        alertView = nil;
    }
    alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:titleButtonCancel otherButtonTitles:otherButtontitle, nil];
    alertView.tag = tag;
    alertView.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    [alertView setNeedsDisplay];
    [alertView show];
}
@end
