//
//  TextToSpeech.m
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/16/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import "TextToSpeech.h"


@implementation TextToSpeech
+ (id)sharedManager {
    static TextToSpeech *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        self.synthesizer = [[AVSpeechSynthesizer alloc]init];
    }
    return self;
}
- (void)speakText:(NSString*)text andLangue:(NSString*)languge
{
    
    self.utterance = [AVSpeechUtterance speechUtteranceWithString:text];
    [self.utterance setRate:0.3f];
    self.utterance.voice = [AVSpeechSynthesisVoice voiceWithLanguage:languge];
    [self.synthesizer speakUtterance:self.utterance];

}
#pragma mark - AVSpeechSynthesizerDelegate
- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didStartSpeechUtterance:(AVSpeechUtterance *)utterance
{
    
}
- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance
{
    
}
@end
