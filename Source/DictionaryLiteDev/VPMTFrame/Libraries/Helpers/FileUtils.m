//
//  DocFileHelper.m
//  VPQH_VietLaw
//
//  Created by Tran Hoang Hiep on 7/24/13.
//  Copyright (c) 2013 Tran Hoang Hiep. All rights reserved.
//

#import "FileUtils.h"

@implementation FileUtils

+(NSString*) getPathFromDocumentFolder : (NSString*)fileName{
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* docFilePath = [documentsPath stringByAppendingPathComponent:fileName];
    return docFilePath;
}

+(NSString*) copyFileFromBundleToDocumentFolder: (NSString*)fileName{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* docFilePath = [documentsPath stringByAppendingPathComponent:fileName];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:docFilePath];
    if(!fileExists){
        //bat dau copy
        NSError *error;
        NSString* docBundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:fileName];
        [fileManager copyItemAtPath:docBundlePath toPath:docFilePath error:&error];
//        NSLog(@"%@",error);
    }
    return docFilePath;
}

+(NSString*) getPathFromBundle:(NSString *)fileName{
    NSString* docBundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:fileName];
    return docBundlePath;
}

+(UIImage*) getImageFromBundle:(NSString *)fileName ofType:(NSString*)type{
    return [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:fileName ofType:type]];
}

+(void)createFolder:(NSString *)folderPath{
    id error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:folderPath];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
}

+(void)createSubCacheFolder:(NSString *)folderPath{
    id error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:folderPath];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
}

+(void)removeFile:(NSString *)path{
    id error;
    [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
}

+(BOOL)existFile:(NSString *)filePath{
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* docFilePath = [documentsPath stringByAppendingPathComponent:filePath];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:docFilePath];
    return fileExists;
}

+(long long)getFileSize:(NSString *)filePath{
    long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil] fileSize];
    return fileSize;
}
@end
