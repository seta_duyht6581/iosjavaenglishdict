//
//  ValidateUtils.m
//  ViettelPI
//
//  Created by Trinh Duy Nhan on 3/27/14.
//  Copyright (c) 2014 viettel. All rights reserved.
//

#import "ValidateUtils.h"

@implementation ValidateUtils


+ (BOOL)isForMatchString:(NSString *)string withRegex:(NSString *)regex
{
    NSPredicate *regexTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [regexTest evaluateWithObject:string];
}


+ (BOOL)validateEmailAddress:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL isValid = [emailTest evaluateWithObject:email];
    return isValid;
}


+ (BOOL)validateString:(NSString *)text withConditionMin:(NSInteger)minLength
{
    NSString *string = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (string.length <= minLength) {
        return NO;
    }
    return YES;
}


+ (BOOL)validateString:(NSString *)text withConditionMax:(NSInteger)maxLength
{
    NSString *string = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (string.length > maxLength) {
        return NO;
    }
    return YES;
}


+ (BOOL)validateTextFirst:(NSString *)text1 andTextSecond:(NSString *)text2
{
    NSString *string1 = [text1 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *string2 = [text2 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if([string1 isEqualToString:string2]){
        return YES;
    }
    return NO;
}


+ (BOOL)validateURL:(NSString *)url
{
    NSString *urlRegEx=
	@"((https?|ftp|gopher|telnet|file|notes|ms-help):((//)|(\\\\\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
    
    return [self isForMatchString:url withRegex:urlRegEx];
}


+ (BOOL)validateNumberic:(NSString *)number
{
    NSString *numberRegEx = @"(-|\\+)?[0-9]+(\\.[0-9]+)?";
    
	return [self isForMatchString:number withRegex:numberRegEx];
}


+ (BOOL)validateIntegerNumberic:(NSString *)number
{
    NSString *numberRegEx = @"[0-9]+([0-9]+)?";
    
	return [self isForMatchString:number withRegex:numberRegEx];
}


+ (BOOL)validateContainNumberic:(NSString *)number
{
    NSString *numberRegEx = @"(-|\\+)?[0-9]+(\\.)?+([0-9]+)?";
    
	return [self isForMatchString:number withRegex:numberRegEx];
}


+ (BOOL)validateContainPositiveNumberic:(NSString *)number
{
    NSString *numberRegEx = @"[0-9]+(\\.)?+([0-9]+)?";
    
	return [self isForMatchString:number withRegex:numberRegEx];
}


+ (BOOL)validatePhoneNumber:(NSString *)phoneNumber
{
    NSString *phoneRegex = @"([-,0-9,(,), ,.]*)?";
    
    return [self isForMatchString:phoneNumber withRegex:phoneRegex];
}


+ (BOOL)validateViettelPhoneNumber:(NSString *)phoneNumber
{
    NSString *vtPhoneRegex = @"84(98|97|96|163|164|165|166|167|168|169)[0-9]*$";
    
    return [self isForMatchString:phoneNumber withRegex:vtPhoneRegex];
}


+ (BOOL)validateStrongPassword:(NSString *)password withMinLength:(int)length
{
    
    NSMutableString *passRegex = [[NSMutableString alloc] initWithString:@"((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*])."];
    [passRegex appendString:[NSString stringWithFormat:@"{%d,})",length]];
    
    return  [self isForMatchString:password withRegex:[passRegex mutableCopy]];
}


+ (BOOL)validateStrongPasswordVietnamese:(NSString *)password withMinLength:(int)length
{
    password = [NSString convertASCIIStringFromVietnameseString:password];
    NSMutableString *passRegex = [[NSMutableString alloc] initWithString:@"((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*])."];
    [passRegex appendString:[NSString stringWithFormat:@"{%d,})",length]];
    
    return  [self isForMatchString:password withRegex:[passRegex mutableCopy]];
}


+ (BOOL)validateHexadecimalNumber:(NSString *)number
{
    if ([number isEqualToString:@"0"] ||
        [number isEqualToString:@"1"] ||
        [number isEqualToString:@"2"] ||
        [number isEqualToString:@"3"] ||
        [number isEqualToString:@"4"] ||
        [number isEqualToString:@"5"] ||
        [number isEqualToString:@"6"] ||
        [number isEqualToString:@"7"] ||
        [number isEqualToString:@"8"] ||
        [number isEqualToString:@"9"] ||
        [[number uppercaseString] isEqualToString:@"A"] ||
        [[number uppercaseString] isEqualToString:@"B"] ||
        [[number uppercaseString] isEqualToString:@"C"] ||
        [[number uppercaseString] isEqualToString:@"D"] ||
        [[number uppercaseString] isEqualToString:@"E"] ||
        [[number uppercaseString] isEqualToString:@"F"] )
    {
        return YES;
    } else {
        return NO;
    }
}


+ (BOOL)validateMACAddress:(NSString *)address
{
    // Must be 17 chars.
    NSInteger len = [address length];
    if (len != 17) {
        return NO;
    }
    
    // Let's valide all characters.
    NSString *tempStr;
    if (len == 17) {
        
        // Loop by couples plus separator (eg: "C6:").
        for (NSInteger i=0; i<=len; i+=3) {
            
            // Position 0: hex char.
            tempStr = [address substringWithRange:NSMakeRange(i+0, 1)];
            if (![self validateHexadecimalNumber:tempStr]) {
                return NO;
            }
            
            // Position 1: hex char.
            tempStr = [address substringWithRange:NSMakeRange(i+1, 1)];
            if (![self validateHexadecimalNumber:tempStr]) {
                return NO;
            }
            
            // Position 2: separator.
            if (i+2 < len) // Warning: last separator does not exist.
            {
                tempStr = [address substringWithRange:NSMakeRange(i+2, 1)];
                if (![tempStr isEqualToString:@":"]) {
                    return NO;
                }
            }
        }
    }
    
    return YES;
}


+ (BOOL)validateIPAddress:(NSString *)address
{
    const char *utf8 = [address UTF8String];
    
    // Check valid IPv4.
    struct in_addr dst;
    int success = inet_pton(AF_INET, utf8, &(dst.s_addr));
    if (success != 1) {
        // Check valid IPv6.
        struct in6_addr dst6;
        success = inet_pton(AF_INET6, utf8, &dst6);
    }
    return (success == 1);
}

@end
