
//
//  ApiHelper.h
//
//  Created by viettel on 6/14/13.
//  Copyright (c) 2014 ChuongPD. All rights reserved.
//
/*
Level 0 – “Emergency”
Level 1 – “Alert”
Level 2 – “Critical”
Level 3 – “Error”
Level 4 – “Warning”
Level 5 – “Notice”
Level 6 – “Info”
Level 7 – “Debug”
*/

#import "Log.h"
//#import "asl.h"

@implementation Log

static Log* sharedManager = nil;

+ (instancetype)sharedManager {
    @synchronized(self)
    {
        if (sharedManager && [sharedManager isKindOfClass:[self class]]){
            return sharedManager;
        }else{
            sharedManager = [[self alloc] init];
            return sharedManager;
        }
    }
}

+(NSArray*)getIOSSystemLog{
    NSMutableArray *consoleLog = [NSMutableArray array];
        
    return consoleLog;
}


/*
 +(NSArray*)getOXSystemLog{
 NSMutableArray *consoleLog = [NSMutableArray array];
 
 aslmsg q, m;
 int i;
 const char *key, *val;
 
 q = asl_new(ASL_TYPE_QUERY);
 
 id projectName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
 const char *str  = [[NSString stringWithFormat:@"%@",projectName] UTF8String];
 
 asl_set_query(q, ASL_KEY_SENDER, str, ASL_QUERY_OP_EQUAL);
 asl_set_query(q, ASL_KEY_LEVEL, "7",  ASL_QUERY_OP_EQUAL);
 
 aslresponse r = asl_search(NULL, q);
 
 while (NULL != (m = aslresponse_next(r)))
 {
 NSMutableDictionary *tmpDict = [NSMutableDictionary dictionary];
 
 for (i = 0; (NULL != (key = asl_key(m, i))); i++)
 {
 NSString *keyString = [NSString stringWithUTF8String:(char *)key];
 
 val = asl_get(m, key);
 
 NSString *string = val?[NSString stringWithUTF8String:val]:@"";
 [tmpDict setObject:string forKey:keyString];
 }
 
 LogMethodArgs(@"%@", tmpDict);
 }
 aslresponse_free(r);
 
 return consoleLog;
 }
 */
@end
