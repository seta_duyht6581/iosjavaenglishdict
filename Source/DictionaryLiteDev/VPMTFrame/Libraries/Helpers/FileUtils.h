//
//  DocFileHelper.h
//  VPQH_VietLaw
//
//  Created by Tran Hoang Hiep on 7/24/13.
//  Copyright (c) 2013 Tran Hoang Hiep. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileUtils : NSObject
+(NSString*) getPathFromDocumentFolder : (NSString*)fileName;
+(NSString*) copyFileFromBundleToDocumentFolder: (NSString*)fileName;
+(NSString*) getPathFromBundle: (NSString*)fileName;
+(UIImage*) getImageFromBundle:(NSString *)fileName ofType:(NSString*)type;

+(void) createFolder :(NSString*)folderPath;
+(void)createSubCacheFolder:(NSString *)folderPath;
+(void)removeFile:(NSString *)path;
+(BOOL) existFile:(NSString*)filePath;

+(long long)getFileSize:(NSString*)filePath;
@end
