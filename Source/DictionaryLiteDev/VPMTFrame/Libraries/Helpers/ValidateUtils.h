//
//  ValidateUtils.h
//  ViettelPI
//
//  Created by Trinh Duy Nhan on 3/27/14.
//  Copyright (c) 2014 viettel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ValidateUtils : NSObject


/**Kiểm tra string phù hợp với chuỗi regex.
 @author    nhantd
 @param     string
 Chuỗi nhập vào
 @param     regex
 Chuỗi regex
 @return    YES nếu chuỗi string phù hợp với regex.
 NO nếu ngược lại.
 */
+ (BOOL)isForMatchString:(NSString *)string withRegex:(NSString *)regex;


/**Kiểm tra tính hợp lệ của địa chỉ email.
 @author    nhantd
 @param     email
 Địa chỉ email nhập vào
 @return    YES nếu hợp lệ.
 NO nếu ngược lại.
 */
+ (BOOL)validateEmailAddress:(NSString *)email;


/**Kiểm tra độ dài tối thiểu của chuỗi string nhập vào.
 @author    nhantd
 @param     text
 Chuỗi nhập vào
 @param     minLength
 Độ dài tối thiểu
 @return    NO nếu chuỗi string có độ dài nhỏ hơn hoặc bằng độ dài tối thiểu.
 YES nếu ngược lại.
 */
+ (BOOL)validateString:(NSString *)text withConditionMin:(NSInteger)minLength;


/**Kiểm tra độ dài tối đa của chuỗi string nhập vào.
 @author    nhantd
 @param     text
 Chuỗi nhập vào
 @param     maxLength
 Độ dài tối đa
 @return    NO nếu chuỗi string có độ dài lớn hơn độ dài tối đa.
 YES nếu ngược lại.
 */
+ (BOOL)validateString:(NSString *)text withConditionMax:(NSInteger)maxLength;


/**Kiểm tra 2 chuỗi nhập vào có trùng khớp với nhau không. Sử dụng đối với password và confirm password.
 @author    nhantd
 @param     text1
 Chuỗi thứ nhất
 @param     text2
 Chuỗi thứ hai
 @return    YES nếu 2 chuỗi là trùng khớp.
 NO nếu ngược lại.
 */
+ (BOOL)validateTextFirst:(NSString *)text1 andTextSecond:(NSString *)text2;


/**Kiểm tra tính hợp lệ của địa chỉ url.
 @author    nhantd
 @param     url
 Địa chỉ url nhập vào
 @return    YES nếu hợp lệ.
 NO nếu ngược lại.
 */
+ (BOOL)validateURL:(NSString *)url;


/**Kiểm tra xem chuỗi nhập vào có phải là numberic hay không.
 @author    nhantd
 @param     number
 Chuỗi nhập vào
 @return    YES nếu chuỗi nhập vào là numberic.
 NO nếu ngược lại.
 */
+ (BOOL)validateNumberic:(NSString *)number;


/**Kiểm tra xem chuỗi nhập vào có phải là integer numberic hay không.
 @author    nhantd
 @param     number
 Chuỗi nhập vào
 @return    YES nếu chuỗi nhập vào là integer numberic.
 NO nếu ngược lại.
 */
+ (BOOL)validateIntegerNumberic:(NSString *)number;


/**Kiểm tra xem chuỗi nhập vào có phải chứa numberic hay không. Ex: 120.;12;12.00;...
 @author    nhantd
 @param     number
 Chuỗi nhập vào
 @return    YES nếu chuỗi nhập vào là numberic.
 NO nếu ngược lại.
 */
+ (BOOL)validateContainNumberic:(NSString *)number;


/**Kiểm tra xem chuỗi nhập vào có phải chứa positive numberic (số dương) hay không. Ex: 120.;12;12.00;...
 @author    nhantd
 @param     number
 Chuỗi nhập vào
 @return    YES nếu chuỗi nhập vào là positive numberic.
 NO nếu ngược lại.
 */
+ (BOOL)validateContainPositiveNumberic:(NSString *)number;


/**Kiểm tra xem chuỗi nhập vào có phải là phonenumber hay không.
 @author    nhantd
 @param     phoneNumber
 Chuỗi nhập vào
 @return    YES nếu chuỗi nhập vào là phonenumber.
 NO nếu ngược lại.
 */
+ (BOOL)validatePhoneNumber:(NSString *)phoneNumber;


/**Kiểm tra xem chuỗi nhập vào có phải là Viettel phonenumber hay không.
 @author    nhantd
 @param     phoneNumber
 Chuỗi nhập vào
 @return    YES nếu chuỗi nhập vào là Viettel phonenumber.
 NO nếu ngược lại.
 */
+ (BOOL)validateViettelPhoneNumber:(NSString *)phoneNumber;


/**Kiểm tra xem mật khẩu nhập vào có phải là mật khẩu mạnh hay không. Mật khẩu mạnh là mật khẩu có tối thiểu length ký tự, có chữ cái thường, chữ cái hoa, chữ số và ký tự đặc biệt !@#$%^&*.
 @author    nhantd
 @param     password
 Mật khẩu nhập vào
 @param     length
 Số ký tự tối thiểu
 @return    YES nếu mật khẩu nhập vào là mật khẩu mạnh.
 NO nếu ngược lại.
 */
+ (BOOL)validateStrongPassword:(NSString *)password withMinLength:(int)length;


/**Kiểm tra xem mật khẩu nhập vào có phải là mật khẩu mạnh hay không, cho phép có tiếng Việt. Mật khẩu mạnh là mật khẩu có tối thiểu length ký tự, có chữ cái thường, chữ cái hoa, chữ số và ký tự đặc biệt !@#$%^&*.
 @author    nhantd
 @param     password
 Mật khẩu nhập vào
 @param     length
 Số ký tự tối thiểu
 @return    YES nếu mật khẩu nhập vào là mật khẩu mạnh.
 NO nếu ngược lại.
 */
+ (BOOL)validateStrongPasswordVietnamese:(NSString *)password withMinLength:(int)length;


/**Kiểm tra xem chuỗi number nhập vào có phải là một giá trị Hex.
 @author    nhantd
 @param     number
 Chuỗi nhập vào
 @return    YES nếu chuỗi nhập vào là một giá trị Hex.
 NO nếu ngược lại.
 */
+ (BOOL)validateHexadecimalNumber:(NSString *)number;


/**Kiểm tra xem chuỗi address nhập vào có phải là địa chỉ MAC hay không. Ex : 12:34:56:78:9A:BC.
 @author    nhantd
 @param     address
 Chuỗi nhập vào
 @return    YES nếu chuỗi nhập vào là địa chỉ MAC.
 NO nếu ngược lại.
 */
+ (BOOL)validateMACAddress:(NSString *)address;


/**Kiểm tra xem chuỗi address nhập vào có phải là địa chỉ IP (IPv4 hoặc IPv6) hay không. Ex : 192.168.1.1.
 @author    nhantd
 @param     address
 Chuỗi nhập vào
 @return    YES nếu chuỗi nhập vào là địa chỉ IP.
 NO nếu ngược lại.
 */
+ (BOOL)validateIPAddress:(NSString *)address;

@end
