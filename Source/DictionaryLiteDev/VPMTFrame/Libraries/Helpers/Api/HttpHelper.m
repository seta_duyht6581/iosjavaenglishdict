//
//  Api.h
//
//  Created by viettel on 7/10/14.
//  Copyright (c) 2014 ChuongPD. All rights reserved.
//

#import "HttpHelper.h"


@implementation HttpHelper


static int httpRequestType = REQUEST_TYPE_POST;
static NSMutableDictionary *defaultHeader = nil;

static HttpHelper* sharedManager = nil;

+ (instancetype)sharedManager {
    @synchronized(self)
    {
        if (sharedManager && [sharedManager isKindOfClass:[self class]]){
            return sharedManager;
        }else{
            sharedManager = [[self alloc] init];
            return sharedManager;
        }
    }
}

+(void)setRequestType:(int)requestType{
    httpRequestType = requestType;
}

+(void)reset:(int)requestType{
    httpRequestType = REQUEST_TYPE_POST;
}

+(void)setDefaultHeader:(NSString*)key value:(NSString*)value{
    if (defaultHeader == nil) {
        defaultHeader = [[NSMutableDictionary alloc] init];
    }
    [defaultHeader setValue:value forKey:key];
}

+(void)removeDefaultHeader:(NSString*)key value:(NSString*)value{
    if (defaultHeader) {
        [defaultHeader removeObjectForKey:key];
        
        if ([[self sharedManager] oManager]) {
            [[[self sharedManager] oManager].requestSerializer removeHTTPHeaderFieldForKey:key];
        }
    }
}

+(void)removeAllDefaultHeader{
    if (defaultHeader) {
        [defaultHeader removeAllObjects];
        defaultHeader = nil;
        
        if ([[self sharedManager] oManager]) {
            [[[self sharedManager] oManager].requestSerializer removeAllHTTPHeaderField];
        }
    }
}


#pragma mark Api requestor
- (void)httpRequest:(NSString*)url
             params:(NSDictionary*)params
          onSuccess:(void (^)(NSDictionary *responseDict, int statusCode))sucessBlock
            onError:(void (^)(NSError *error))failedBlock{
    [self httpRequest:url params:params headers:nil onSuccess:sucessBlock onError:failedBlock];
}


- (void)httpRequest:(NSString*)url
            params:(NSDictionary*)params
            headers:(NSDictionary*)headers
            onSuccess:(void (^)(NSDictionary *responseDict, int statusCode))sucessBlock
            onError:(void (^)(NSError *error))failedBlock
{
    [self httpsRequest:url params:params headers:headers onSuccess:sucessBlock onError:failedBlock];
}


- (void)cancelAllRequest{
    [[self.oManager operationQueue] cancelAllOperations];
}

#pragma mark https

- (void)httpsRequest:(NSString*)url
              params:(NSDictionary*)params
           onSuccess:(void (^)(NSDictionary *responseDict, int statusCode))sucessBlock
             onError:(void (^)(NSError *error))failedBlock{
    [self httpsRequest:url params:params headers:nil onSuccess:sucessBlock onError:failedBlock];
}

- (void)httpsRequest:(NSString*)url
             params:(NSDictionary*)params
             headers:(NSDictionary*)headers
          onSuccess:(void (^)(NSDictionary *responseDict, int statusCode))sucessBlock
            onError:(void (^)(NSError *error))failedBlock
{
    if (self.oManager == nil) {
        self.oManager = [AFHTTPRequestOperationManager manager];
        self.oManager.securityPolicy.allowInvalidCertificates = YES;
        
        #ifdef HTTP_REQUEST_TIME_OUT
        
        self.oManager.requestSerializer = [[TimeoutAFHTTPRequestSerializer alloc] initWithTimeout:HTTP_REQUEST_TIME_OUT];
        
        #else
        
        self.oManager.requestSerializer = [AFJSONRequestSerializer serializer];
        ////[self.oManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        //[self.oManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
        #endif
        
        self.oManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",@"application/x-www-form-urlencoded", nil];
    }
    
    if (headers) {
        for( NSString *key in headers )
        {
            [self.oManager.requestSerializer setValue:[headers valueForKey:key] forHTTPHeaderField:key];
        }
    }
    
    if (defaultHeader) {
        NSArray *allKeys = [defaultHeader allKeys];
        for( NSString *key in allKeys)
        {
            [self.oManager.requestSerializer setValue:[defaultHeader valueForKey:key] forHTTPHeaderField:key];
        }
    }
    
    switch (httpRequestType) {
        case REQUEST_TYPE_GET:
        {
            [self.oManager GET:url
                     parameters:params
                        success:^(AFHTTPRequestOperation *operation, id responseObject) {
                            if (sucessBlock) {
                                sucessBlock(responseObject, (int)[operation.response statusCode]);
                            }
                        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                            if (failedBlock) {
                                failedBlock(error);
                            }
                        }];
        }
            break;
        case REQUEST_TYPE_POST:
        {
            [self.oManager POST:url
                    parameters:params
                       success:^(AFHTTPRequestOperation *operation, id responseObject) {
                           if (sucessBlock) {
                               sucessBlock(responseObject, (int)[operation.response statusCode]);
                           }
                       } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                           if (failedBlock) {
                               failedBlock(error);
                           }
                       }];
        }
            break;
        case REQUEST_TYPE_HEAD:{
            [self.oManager HEAD:url
                     parameters:params
                        success:^(AFHTTPRequestOperation *operation) {
                            if (sucessBlock) {
                                sucessBlock(nil, (int)[operation.response statusCode]);
                            }
                        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                            if (failedBlock) {
                                failedBlock(error);
                            }
                        }];
        }
            break;
        case REQUEST_TYPE_PUT:
        {
            [self.oManager PUT:url
                    parameters:params
                       success:^(AFHTTPRequestOperation *operation, id responseObject) {
                           if (sucessBlock) {
                               sucessBlock(responseObject, (int)[operation.response statusCode]);
                           }
                       } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                           if (failedBlock) {
                               failedBlock(error);
                           }
                       }];
        }
            break;
        case REQUEST_TYPE_PATCH:
        {
            [self.oManager PATCH:url
                      parameters:params
                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                             if (sucessBlock) {
                                 sucessBlock(responseObject, (int)[operation.response statusCode]);
                             }
                         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                             if (failedBlock) {
                                 failedBlock(error);
                             }
                         }];
        }
            break;
        case REQUEST_TYPE_DELETE:
        {
            [self.oManager DELETE:url
                       parameters:params
                          success:^(AFHTTPRequestOperation *operation, id responseObject) {
                              if (sucessBlock) {
                                  sucessBlock(responseObject, (int)[operation.response statusCode]);
                              }
                          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                              if (failedBlock) {
                                  failedBlock(error);
                                }
                          }];
        }
            break;
            
        default:
            break;
    }
}

-(AFHTTPRequestOperation*)httpUploadFile:(NSString*)url
                 filePath:(NSString*)filePath
             serverKey:(NSString*)serverKey
             onSuccess:(void (^)(int statusCode))sucessBlock
               onError:(void (^)(NSError *error))failedBlock
          onProcessing:(void (^)(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite))processingBlock
{
 
    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json", nil];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    AFHTTPRequestOperation *operation = [manager POST:url parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //do not put image inside parameters dictionary as I did, but append it!
        [formData appendPartWithFileData:fileData name:serverKey fileName:[[NSURL URLWithString:filePath] lastPathComponent] mimeType:@"application/octet-stream"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        sucessBlock(200);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failedBlock(error);
    }];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        //float processing = [[NSString stringWithFormat:@"%.2f", (float)totalBytesWritten/totalBytesExpectedToWrite] floatValue];
        processingBlock(bytesWritten, totalBytesWritten, totalBytesExpectedToWrite);
    }];
    
    return operation;
}

-(AFHTTPRequestOperation*)httpUploadImage:(NSString*)url
                 image:(UIImage*)image
             serverKey:(NSString*)serverKey
              fileName:(NSString*)fileName
             onSuccess:(void (^)(int statusCode))sucessBlock
               onError:(void (^)(NSError *error))failedBlock
               onProcessing:(void (^)(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite))processingBlock
{
    
    NSData *imageData = UIImagePNGRepresentation(image);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json", nil];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.securityPolicy.allowInvalidCertificates = YES;
    AFHTTPRequestOperation *operation = [manager POST:url parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //do not put image inside parameters dictionary as I did, but append it!
        [formData appendPartWithFileData:imageData name:serverKey fileName:fileName mimeType:@"application/octet-stream"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        sucessBlock(200);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failedBlock(error);
    }];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        //float processing = [[NSString stringWithFormat:@"%.2f", (float)totalBytesWritten/totalBytesExpectedToWrite] floatValue];
        processingBlock(bytesWritten, totalBytesWritten, totalBytesExpectedToWrite);
    }];
    
    return operation;
}

-(AFHTTPRequestOperation*)httpDownload:(NSString*)url
             onSuccess:(void (^)(NSData *data))sucessBlock
               onError:(void (^)(NSError *error))failedBlock
          onProcessing:(void (^)(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite))processingBlock
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    AFHTTPRequestOperation *downloadRequest = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [downloadRequest setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSData *data = [[NSData alloc] initWithData:responseObject];
        sucessBlock(data);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failedBlock(error);
    }];
    
    [downloadRequest setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        processingBlock(bytesRead, totalBytesRead, totalBytesExpectedToRead);
    }];
    
    [downloadRequest start];
    
    return downloadRequest;
}

-(void)httpSoapRequest:(NSString *)url
           soapMessage:(NSString*)soapMessage
             onSuccess:(void (^)(NSXMLParser *responseDict, int statusCode))sucessBlock
               onError:(void (^)(NSError *error))failedBlock{
    /*
    NSString *soapMessage = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                             "<Envelope xmlns=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                             "<Body>"
                             "<requestCommand xmlns=\"http://mobilegateway.viettel.com/\">"
                             "<arg0 xmlns=\"\">8990db4bc2b2777cb770ae98006d8fa5a9d3e3cdf82b89b64aa3ee29fca30812</arg0>"
                             "<arg1 xmlns=\"\">1961</arg1>"
                             "<arg2 xmlns=\"\">341</arg2>"
                             "<arg3 xmlns=\"\">1</arg3>"
                             "<arg4 xmlns=\"\"></arg4>"
                             "</requestCommand>"
                             "</Body>"
                             "</Envelope>"];
    
    
    NSURL *url1 = [NSURL URLWithString:@"https://10.60.15.196:9101/mgw/mobilegw"];
     */
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: @"requestCommand" forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:theRequest];
    operation.responseSerializer = [AFXMLParserResponseSerializer serializer];
    operation.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/xml",@"application/json",@"application/x-www-form-urlencoded",@"application/xml", nil];
    operation.securityPolicy.allowInvalidCertificates = YES;
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //parse NSXMLParser object here if request successfull
        if ([responseObject isKindOfClass:[NSXMLParser class]]) {
            sucessBlock(responseObject,200);
            
            /*
             NSXMLParser *xmlParser = (NSXMLParser *)responseObject;
           
            [xmlParser setDelegate: self];
            
            [xmlParser setShouldResolveExternalEntities: NO];
            [xmlParser setShouldProcessNamespaces:NO];
            [xmlParser setShouldReportNamespacePrefixes:NO];
            [xmlParser setShouldResolveExternalEntities:NO];
            [xmlParser parse];
             */
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failedBlock(error);
    }];
    
    [[NSOperationQueue mainQueue] addOperation:operation];
}

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *) namespaceURI qualifiedName:(NSString *)qName

   attributes: (NSDictionary *)attributeDict

{
    //NSLog(@"%@",elementName);
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName

{
    //NSLog(@"%@",elementName);
}

-(void)parserDidEndDocument:(NSXMLParser *)parser

{
    
}

@end
