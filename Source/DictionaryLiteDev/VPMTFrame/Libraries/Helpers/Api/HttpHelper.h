//
//  ApiHelper.h
//
//  Created by viettel on 7/10/14.
//  Copyright (c) 2014 ChuongPD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Network.h"

#define REQUEST_TYPE_GET 1
#define REQUEST_TYPE_HEAD 3
#define REQUEST_TYPE_POST 5
#define REQUEST_TYPE_PUT 7
#define REQUEST_TYPE_PATCH 9
#define REQUEST_TYPE_DELETE 11

@interface HttpHelper:Network<NSXMLParserDelegate>

    @property(nonatomic,strong) AFHTTPRequestOperationManager* oManager;

#pragma mark http
+(void)setRequestType:(int)requestType;
+(void)reset:(int)requestType;
+(void)setDefaultHeader:(NSString*)key value:(NSString*)value;
+(void)removeDefaultHeader:(NSString*)key value:(NSString*)value;
+(void)removeAllDefaultHeader;

- (void)httpRequest:(NSString*)url
             params:(NSDictionary*)params
          onSuccess:(void (^)(NSDictionary *responseDict, int statusCode))sucessBlock
            onError:(void (^)(NSError *error))failedBlock;

- (void)httpRequest:(NSString*)url
             params:(NSDictionary*)params
            headers:(NSDictionary*)headers
          onSuccess:(void (^)(NSDictionary *responseDict, int statusCode))sucessBlock
            onError:(void (^)(NSError *error))failedBlock;

- (void)cancelAllRequest;

#pragma mark https
- (void)httpsRequest:(NSString*)url
              params:(NSDictionary*)params
           onSuccess:(void (^)(NSDictionary *responseDict, int statusCode))sucessBlock
             onError:(void (^)(NSError *error))failedBlock;
- (void)httpsRequest:(NSString*)url
             params:(NSDictionary*)params
            headers:(NSDictionary*)headers
          onSuccess:(void (^)(NSDictionary *responseDict, int statusCode))sucessBlock
            onError:(void (^)(NSError *error))failedBlock;

-(AFHTTPRequestOperation*)httpUploadFile:(NSString*)url
                                filePath:(NSString*)filePath
                               serverKey:(NSString*)serverKey
                               onSuccess:(void (^)(int statusCode))sucessBlock
                                 onError:(void (^)(NSError *error))failedBlock
                            onProcessing:(void (^)(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite))processingBlock;

-(AFHTTPRequestOperation*)httpUploadImage:(NSString*)url
                                    image:(UIImage*)image
                                serverKey:(NSString*)serverKey
                                 fileName:(NSString*)fileName
                                onSuccess:(void (^)(int statusCode))sucessBlock
                                  onError:(void (^)(NSError *error))failedBlock
                             onProcessing:(void (^)(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite))processingBlock;

-(AFHTTPRequestOperation*)httpDownload:(NSString*)url
                             onSuccess:(void (^)(NSData *data))sucessBlock
                               onError:(void (^)(NSError *error))failedBlock
                          onProcessing:(void (^)(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite))processingBlock;

-(void)httpSoapRequest:(NSString *)url
           soapMessage:(NSString*)soapMessage
             onSuccess:(void (^)(NSXMLParser *responseDict, int statusCode))sucessBlock
               onError:(void (^)(NSError *error))failedBlock;
@end
