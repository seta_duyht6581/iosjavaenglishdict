//
//  ApiHelper.h
//
//  Created by viettel on 6/14/13.
//  Copyright (c) 2014 ChuongPD. All rights reserved.
//

@interface AsyncTask : NSObject 

typedef void (^AsynTaskBlock)(void);
typedef id (^AsynTaskResultBlock)(void);
typedef void (^AsynTaskParamBlock)(id result);
+ (instancetype)sharedManager;

+ (void)execute:(AsynTaskBlock)doInBackground
       onFinish:(AsynTaskBlock)finish;

+ (void)execute:(AsynTaskBlock)doInBackground
       onFinish:(AsynTaskBlock)finish parent:(UIView*)view showLoadingBox:(BOOL)showLoadingBox;

+ (void)execute:(AsynTaskBlock)doInBackground
       onFinish:(AsynTaskBlock)finish parent:(UIView*)view
 showLoadingBox:(BOOL)showLoadingBox
    loadingText:(NSString*)text;

+ (void)execute:(AsynTaskBlock)doInBackground
       onFinish:(AsynTaskBlock)finish afterDelay:(NSTimeInterval)interval;

+ (void)execute:(AsynTaskBlock)doInBackground
       onFinish:(AsynTaskBlock)finish afterDelay:(NSTimeInterval)interval repeatTime:(NSTimeInterval)repeatTime taskName:(NSString*)taskName;

+ (void)execute:(AsynTaskResultBlock)doInBackground
onFinishWithResult:(AsynTaskParamBlock)finish;

+ (void)cancelTask:(NSString*)taskName;
@end
