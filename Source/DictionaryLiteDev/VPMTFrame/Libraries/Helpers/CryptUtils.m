//
//  CryptUtils.m
//  ViettelPI
//
//  Created by Trinh Duy Nhan on 3/28/14.
//  Copyright (c) 2014 viettel. All rights reserved.
//

#import "CryptUtils.h"
#import <CommonCrypto/CommonCrypto.h>

static char encodingTable[64] =
{
    'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P',
    'Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f',
    'g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v',
    'w','x','y','z','0','1','2','3','4','5','6','7','8','9','+','/'
};

@implementation CryptUtils

+ (NSString *)encryptAES256WithString:(NSString *)text withKey:(NSString *)key withIV:(NSData *)ivdata
{
    NSData *plainData = [text dataUsingEncoding:NSUTF8StringEncoding];
    NSData *encryptData = [self encryptAES256WithData:plainData withKey:key withIV:ivdata];
    
    return [self encodeBase64WithData:encryptData];
}


+ (NSString *)decryptAES256WithString:(NSString *)text withKey:(NSString *)key withIV:(NSData *)ivdata
{
    NSData *encryptedData = [self encodeBase64WithString:text];
    NSData *plainData = [self decryptAES256WithData:encryptedData withKey:key withIV:ivdata];
    NSString *plainString = [[NSString alloc] initWithData:plainData encoding:NSUTF8StringEncoding];
    
    return plainString;
}


+ (NSString *)encryptAES128WithString:(NSString *)text withKey:(NSString *)key withIV:(NSData *)ivdata
{
    NSData *plainData = [text dataUsingEncoding:NSUTF8StringEncoding];
    NSData *encryptData = [self encryptAES128WithData:plainData withKey:key withIV:ivdata];
    
    return [self encodeBase64WithData:encryptData];
}


+ (NSString *)decryptAES128WithString:(NSString *)text withKey:(NSString *)key withIV:(NSData *)ivdata
{
    NSData *encryptedData = [self encodeBase64WithString:text];
    NSData *plainData = [self decryptAES128WithData:encryptedData withKey:key withIV:ivdata];
    NSString *plainString = [[NSString alloc] initWithData:plainData encoding:NSUTF8StringEncoding];
    
    return plainString;
}


+ (NSString *)encryptDESWithString:(NSString *)text withKey:(NSString *)key withIV:(NSData *)ivdata
{
    NSData *plainData = [text dataUsingEncoding:NSUTF8StringEncoding];
    NSData *encryptData = [self encryptDESWithData:plainData withKey:key withIV:ivdata];
    
    return [self encodeBase64WithData:encryptData];
}


+ (NSString *)decryptDESWithString:(NSString *)text withKey:(NSString *)key withIV:(NSData *)ivdata
{
    NSData *encryptedData = [self encodeBase64WithString:text];
    NSData *plainData = [self decryptDESWithData:encryptedData withKey:key withIV:ivdata];
    NSString *plainString = [[NSString alloc] initWithData:plainData encoding:NSUTF8StringEncoding];
    
    return plainString;
}


+ (NSString *)encryptTripleDESWithString:(NSString *)text withKey:(NSString *)key withIV:(NSData *)ivdata
{
    NSData *plainData = [text dataUsingEncoding:NSUTF8StringEncoding];
    NSData *encryptData = [self encryptTripleDESWithData:plainData withKey:key withIV:ivdata];
    
    return [self encodeBase64WithData:encryptData];
}


+ (NSString *)decryptTripleDESWithString:(NSString *)text withKey:(NSString *)key withIV:(NSData *)ivdata
{
    NSData *encryptedData = [self encodeBase64WithString:text];
    NSData *plainData = [self decryptTripleDESWithData:encryptedData withKey:key withIV:ivdata];
    NSString *plainString = [[NSString alloc] initWithData:plainData encoding:NSUTF8StringEncoding];
    
    return plainString;
}


+ (NSData *)encryptAES256WithData:(NSData *)data withKey:(NSString *)key withIV:(NSData *)ivdata
{
    // 'key' should be 32 bytes for AES256, will be null-padded otherwise
    char keyPtr[kCCKeySizeAES256 + 1]; // room for terminator (unused)
    bzero( keyPtr, sizeof( keyPtr ) ); // fill with zeroes (for padding)
    
    // fetch key data
    [key getCString:keyPtr maxLength:sizeof( keyPtr ) encoding:NSUTF8StringEncoding];
    
    char cIv[kCCBlockSizeAES128];
    bzero(cIv, kCCBlockSizeAES128);
    if (ivdata) {
        [ivdata getBytes:cIv length:kCCBlockSizeAES128];
    }
    
    NSUInteger dataLength = [data length];
    
    //See the doc: For block ciphers, the output size will always be less than or
    //equal to the input size plus the size of one block.
    //That's why we need to add the size of one block here
    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer = malloc( bufferSize );
    
    size_t numBytesEncrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt( kCCEncrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding,
                                          keyPtr, kCCKeySizeAES256,
                                          cIv /* initialization vector (optional) */,
                                          [data bytes], dataLength, /* input */
                                          buffer, bufferSize, /* output */
                                          &numBytesEncrypted );
    if( cryptStatus == kCCSuccess )
    {
        //the returned NSData takes ownership of the buffer and will free it on deallocation
        return [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
    }
    
    free( buffer ); //free the buffer
    return nil;
}


+ (NSData *)decryptAES256WithData:(NSData *)data withKey:(NSString *)key withIV:(NSData *)ivdata
{
    // 'key' should be 32 bytes for AES256, will be null-padded otherwise
    char keyPtr[kCCKeySizeAES256+1]; // room for terminator (unused)
    bzero( keyPtr, sizeof( keyPtr ) ); // fill with zeroes (for padding)
    
    // fetch key data
    [key getCString:keyPtr maxLength:sizeof( keyPtr ) encoding:NSUTF8StringEncoding];
    
    char cIv[kCCBlockSizeAES128];
    bzero(cIv, kCCBlockSizeAES128);
    if (ivdata) {
        [ivdata getBytes:cIv length:kCCBlockSizeAES128];
    }
    
    NSUInteger dataLength = [data length];
    
    //See the doc: For block ciphers, the output size will always be less than or
    //equal to the input size plus the size of one block.
    //That's why we need to add the size of one block here
    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer = malloc( bufferSize );
    
    size_t numBytesDecrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt( kCCDecrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding,
                                          keyPtr, kCCKeySizeAES256,
                                          cIv /* initialization vector (optional) */,
                                          [data bytes], dataLength, /* input */
                                          buffer, bufferSize, /* output */
                                          &numBytesDecrypted );
    
    if( cryptStatus == kCCSuccess )
    {
        //the returned NSData takes ownership of the buffer and will free it on deallocation
        return [NSData dataWithBytesNoCopy:buffer length:numBytesDecrypted];
    }
    
    free( buffer ); //free the buffer
    return nil;
}


+ (NSData *)encryptAES128WithData:(NSData *)data withKey:(NSString *)key withIV:(NSData *)ivdata
{
    // 'key' should be 32 bytes for AES256, will be null-padded otherwise
    char keyPtr[kCCKeySizeAES128 + 1]; // room for terminator (unused)
    bzero( keyPtr, sizeof( keyPtr ) ); // fill with zeroes (for padding)
    
    // fetch key data
    [key getCString:keyPtr maxLength:sizeof( keyPtr ) encoding:NSUTF8StringEncoding];
    
    char cIv[kCCBlockSizeAES128];
    bzero(cIv, kCCBlockSizeAES128);
    if (ivdata) {
        [ivdata getBytes:cIv length:kCCBlockSizeAES128];
    }
    
    NSUInteger dataLength = [data length];
    
//    const unsigned char iv[] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
    
    //See the doc: For block ciphers, the output size will always be less than or
    //equal to the input size plus the size of one block.
    //That's why we need to add the size of one block here
    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer = malloc( bufferSize );
    
    size_t numBytesEncrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt( kCCEncrypt, kCCAlgorithmAES128, kCCOptionECBMode + kCCOptionPKCS7Padding,
                                          keyPtr, kCCKeySizeAES128,
                                          cIv /* initialization vector (optional) */,
                                          [data bytes], dataLength, /* input */
                                          buffer, bufferSize, /* output */
                                          &numBytesEncrypted );
    if( cryptStatus == kCCSuccess )
    {
        //the returned NSData takes ownership of the buffer and will free it on deallocation
        return [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
    }
    
    free( buffer ); //free the buffer
    return nil;
}


+ (NSData *)decryptAES128WithData:(NSData *)data withKey:(NSString *)key withIV:(NSData *)ivdata
{
    // 'key' should be 32 bytes for AES256, will be null-padded otherwise
    char keyPtr[kCCKeySizeAES128 +1]; // room for terminator (unused)
    bzero( keyPtr, sizeof( keyPtr ) ); // fill with zeroes (for padding)
    
    // fetch key data
    [key getCString:keyPtr maxLength:sizeof( keyPtr ) encoding:NSUTF8StringEncoding];
    
    char cIv[kCCBlockSizeAES128];
    bzero(cIv, kCCBlockSizeAES128);
    if (ivdata) {
        [ivdata getBytes:cIv length:kCCBlockSizeAES128];
    }
    
    NSUInteger dataLength = [data length];
    
    //See the doc: For block ciphers, the output size will always be less than or
    //equal to the input size plus the size of one block.
    //That's why we need to add the size of one block here
    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer = malloc( bufferSize );
    
    size_t numBytesDecrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt( kCCDecrypt, kCCAlgorithmAES128, kCCOptionPKCS7Padding,
                                          keyPtr, kCCKeySizeAES128,
                                          cIv /* initialization vector (optional) */,
                                          [data bytes], dataLength, /* input */
                                          buffer, bufferSize, /* output */
                                          &numBytesDecrypted );
    
    if( cryptStatus == kCCSuccess )
    {
        //the returned NSData takes ownership of the buffer and will free it on deallocation
        return [NSData dataWithBytesNoCopy:buffer length:numBytesDecrypted];
    }
    
    free( buffer ); //free the buffer
    return nil;
}


+ (NSData *)encryptDESWithData:(NSData *)data withKey:(NSString *)key withIV:(NSData *)ivdata
{
    char keyPtr[kCCKeySizeDES]; // room for terminator (unused)
    bzero( keyPtr, sizeof( keyPtr ) ); // fill with zeroes (for padding)
    
    // fetch key data
    [key getCString:keyPtr maxLength:sizeof( keyPtr ) encoding:NSUTF8StringEncoding];
    
    char cIv[kCCBlockSizeDES];
    bzero(cIv, kCCBlockSizeDES);
    if (ivdata) {
        [ivdata getBytes:cIv length:kCCBlockSizeDES];
    }
    
    NSUInteger dataLength = [data length];
    
    //See the doc: For block ciphers, the output size will always be less than or
    //equal to the input size plus the size of one block.
    //That's why we need to add the size of one block here
    size_t bufferSize = dataLength + kCCBlockSizeDES;
    void *buffer = malloc( bufferSize );
    
    size_t numBytesEncrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt( kCCEncrypt, kCCAlgorithmDES, kCCOptionPKCS7Padding,
                                          keyPtr, kCCKeySizeDES,
                                          cIv /* initialization vector (optional) */,
                                          [data bytes], dataLength, /* input */
                                          buffer, bufferSize, /* output */
                                          &numBytesEncrypted );
    if( cryptStatus == kCCSuccess )
    {
        //the returned NSData takes ownership of the buffer and will free it on deallocation
        return [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
    }
    
    free( buffer ); //free the buffer
    return nil;
}


+ (NSData *)decryptDESWithData:(NSData *)data withKey:(NSString *)key withIV:(NSData *)ivdata
{
    char keyPtr[kCCKeySizeDES]; // room for terminator (unused)
    bzero( keyPtr, sizeof( keyPtr ) ); // fill with zeroes (for padding)
    
    // fetch key data
    [key getCString:keyPtr maxLength:sizeof( keyPtr ) encoding:NSUTF8StringEncoding];
    
    char cIv[kCCBlockSizeDES];
    bzero(cIv, kCCBlockSizeDES);
    if (ivdata) {
        [ivdata getBytes:cIv length:kCCBlockSizeDES];
    }
    
    NSUInteger dataLength = [data length];
    
    //See the doc: For block ciphers, the output size will always be less than or
    //equal to the input size plus the size of one block.
    //That's why we need to add the size of one block here
    size_t bufferSize = dataLength + kCCBlockSizeDES;
    void *buffer = malloc( bufferSize );
    
    size_t numBytesDecrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt( kCCDecrypt, kCCAlgorithmDES, kCCOptionPKCS7Padding,
                                          keyPtr, kCCBlockSizeDES,
                                          cIv /* initialization vector (optional) */,
                                          [data bytes], dataLength, /* input */
                                          buffer, bufferSize, /* output */
                                          &numBytesDecrypted );
    
    if( cryptStatus == kCCSuccess )
    {
        //the returned NSData takes ownership of the buffer and will free it on deallocation
        return [NSData dataWithBytesNoCopy:buffer length:numBytesDecrypted];
    }
    
    free( buffer ); //free the buffer
    return nil;
}


+ (NSData *)encryptTripleDESWithData:(NSData *)data withKey:(NSString *)key withIV:(NSData *)ivdata
{
    char keyPtr[kCCKeySize3DES]; // room for terminator (unused)
    bzero( keyPtr, sizeof( keyPtr ) ); // fill with zeroes (for padding)
    
    // fetch key data
    [key getCString:keyPtr maxLength:sizeof( keyPtr ) encoding:NSUTF8StringEncoding];
    
    char cIv[kCCBlockSize3DES];
    bzero(cIv, kCCBlockSize3DES);
    if (ivdata) {
        [ivdata getBytes:cIv length:kCCBlockSize3DES];
    }
    
    NSUInteger dataLength = [data length];
    
    //See the doc: For block ciphers, the output size will always be less than or
    //equal to the input size plus the size of one block.
    //That's why we need to add the size of one block here
    size_t bufferSize = dataLength + kCCBlockSize3DES;
    void *buffer = malloc( bufferSize );
    
    size_t numBytesEncrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt( kCCEncrypt, kCCAlgorithm3DES, kCCOptionPKCS7Padding,
                                          keyPtr, kCCBlockSize3DES,
                                          cIv /* initialization vector (optional) */,
                                          [data bytes], dataLength, /* input */
                                          buffer, bufferSize, /* output */
                                          &numBytesEncrypted );
    if( cryptStatus == kCCSuccess )
    {
        //the returned NSData takes ownership of the buffer and will free it on deallocation
        return [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
    }
    
    free( buffer ); //free the buffer
    return nil;
}


+ (NSData *)decryptTripleDESWithData:(NSData *)data withKey:(NSString *)key withIV:(NSData *)ivdata
{
    char keyPtr[kCCKeySize3DES]; // room for terminator (unused)
    bzero( keyPtr, sizeof( keyPtr ) ); // fill with zeroes (for padding)
    
    // fetch key data
    [key getCString:keyPtr maxLength:sizeof( keyPtr ) encoding:NSUTF8StringEncoding];
    
    char cIv[kCCBlockSize3DES];
    bzero(cIv, kCCBlockSize3DES);
    if (ivdata) {
        [ivdata getBytes:cIv length:kCCBlockSize3DES];
    }
    
    NSUInteger dataLength = [data length];
    
    //See the doc: For block ciphers, the output size will always be less than or
    //equal to the input size plus the size of one block.
    //That's why we need to add the size of one block here
    size_t bufferSize = dataLength + kCCBlockSize3DES;
    void *buffer = malloc( bufferSize );
    
    size_t numBytesDecrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt( kCCDecrypt, kCCAlgorithm3DES, kCCOptionPKCS7Padding,
                                          keyPtr, kCCBlockSize3DES,
                                          cIv /* initialization vector (optional) */,
                                          [data bytes], dataLength, /* input */
                                          buffer, bufferSize, /* output */
                                          &numBytesDecrypted );
    
    if( cryptStatus == kCCSuccess )
    {
        //the returned NSData takes ownership of the buffer and will free it on deallocation
        return [NSData dataWithBytesNoCopy:buffer length:numBytesDecrypted];
    }
    
    free( buffer ); //free the buffer
    return nil;
}


+ (NSString *)encodeBase64WithData:(NSData *)data
{
    return [self encodeBase64WithData:data withLineLength:0];
}


+ (NSString *)encodeBase64WithData:(NSData *)data withLineLength:(NSUInteger)lineLength
{
    const unsigned char   *bytes = [data bytes];
    NSMutableString *result = [NSMutableString stringWithCapacity:data.length];
    unsigned long ixtext = 0;
    unsigned long lentext = data.length;
    long ctremaining = 0;
    unsigned char inbuf[3], outbuf[4];
    unsigned short i = 0;
    unsigned short charsonline = 0, ctcopy = 0;
    unsigned long ix = 0;
    
    while( YES )
    {
        ctremaining = lentext - ixtext;
        if( ctremaining <= 0 ) break;
        
        for( i = 0; i < 3; i++ )
        {
            ix = ixtext + i;
            if( ix < lentext ) inbuf[i] = bytes[ix];
            else inbuf [i] = 0;
        }
        
        outbuf [0] = (inbuf [0] & 0xFC) >> 2;
        outbuf [1] = ((inbuf [0] & 0x03) << 4) | ((inbuf [1] & 0xF0) >> 4);
        outbuf [2] = ((inbuf [1] & 0x0F) << 2) | ((inbuf [2] & 0xC0) >> 6);
        outbuf [3] = inbuf [2] & 0x3F;
        ctcopy = 4;
        
        switch( ctremaining )
        {
            case 1:
                ctcopy = 2;
                break;
            case 2:
                ctcopy = 3;
                break;
        }
        
        for( i = 0; i < ctcopy; i++ )
            [result appendFormat:@"%c", encodingTable[outbuf[i]]];
        
        for( i = ctcopy; i < 4; i++ )
            [result appendString:@"="];
        
        ixtext += 3;
        charsonline += 4;
        
        if( lineLength > 0 )
        {
            if( charsonline >= lineLength )
            {
                charsonline = 0;
                [result appendString:@"\n"];
            }
        }
    }
    
    return [NSString stringWithString:result];
}



+ (NSData *)encodeBase64WithString:(NSString *)text
{
    NSMutableData *mutableData = nil;
    
    if( text )
    {
        unsigned long ixtext = 0;
        unsigned long lentext = 0;
        unsigned char ch = 0;
        unsigned char inbuf[4], outbuf[3];
        short i = 0, ixinbuf = 0;
        BOOL flignore = NO;
        BOOL flendtext = NO;
        NSData *base64Data = nil;
        const unsigned char *base64Bytes = nil;
        
        // Convert the string to ASCII data.
        base64Data = [text dataUsingEncoding:NSASCIIStringEncoding];
        base64Bytes = [base64Data bytes];
        mutableData = [NSMutableData dataWithCapacity:base64Data.length];
        lentext = base64Data.length;
        
        while( YES )
        {
            if( ixtext >= lentext ) break;
            ch = base64Bytes[ixtext++];
            flignore = NO;
            
            if( ( ch >= 'A' ) && ( ch <= 'Z' ) ) ch = ch - 'A';
            else if( ( ch >= 'a' ) && ( ch <= 'z' ) ) ch = ch - 'a' + 26;
            else if( ( ch >= '0' ) && ( ch <= '9' ) ) ch = ch - '0' + 52;
            else if( ch == '+' ) ch = 62;
            else if( ch == '=' ) flendtext = YES;
            else if( ch == '/' ) ch = 63;
            else flignore = YES;
            
            if( ! flignore )
            {
                short ctcharsinbuf = 3;
                BOOL flbreak = NO;
                
                if( flendtext )
                {
                    if( ! ixinbuf ) break;
                    if( ( ixinbuf == 1 ) || ( ixinbuf == 2 ) ) ctcharsinbuf = 1;
                    else ctcharsinbuf = 2;
                    ixinbuf = 3;
                    flbreak = YES;
                }
                
                inbuf [ixinbuf++] = ch;
                
                if( ixinbuf == 4 )
                {
                    ixinbuf = 0;
                    outbuf [0] = ( inbuf[0] << 2 ) | ( ( inbuf[1] & 0x30) >> 4 );
                    outbuf [1] = ( ( inbuf[1] & 0x0F ) << 4 ) | ( ( inbuf[2] & 0x3C ) >> 2 );
                    outbuf [2] = ( ( inbuf[2] & 0x03 ) << 6 ) | ( inbuf[3] & 0x3F );
                    
                    for( i = 0; i < ctcharsinbuf; i++ )
                        [mutableData appendBytes:&outbuf[i] length:1];
                }
                
                if( flbreak )  break;
            }
        }
    }
    
    return [[NSData alloc] initWithData:mutableData];
}


+ (NSString *)encodeHexStringWithString:(NSString *)text
{
    NSUInteger len = [text length];
    unichar *chars = malloc(len * sizeof(unichar));
    [text getCharacters:chars];
    
    NSMutableString *hexString = [[NSMutableString alloc] init];
    
    for(NSUInteger i = 0; i < len; i++ )
    {
        [hexString appendFormat:@"%02x", chars[i]]; /*EDITED PER COMMENT BELOW*/
    }
    free(chars);
    
    return hexString;
}


+ (NSString *)decodeHexStringWithString:(NSString *)hexText
{
    NSMutableData *stringData = [[NSMutableData alloc] init] ;
    unsigned char whole_byte;
    char byte_chars[3] = {'\0','\0','\0'};
    int i;
    for (i=0; i < [hexText length] / 2; i++) {
        byte_chars[0] = [hexText characterAtIndex:i*2];
        byte_chars[1] = [hexText characterAtIndex:i*2+1];
        whole_byte = strtol(byte_chars, NULL, 16);
        [stringData appendBytes:&whole_byte length:1];
    }
    
    return [[NSString alloc] initWithData:stringData encoding:NSASCIIStringEncoding];
}


+ (NSString *)encodeStringXOR:(NSString *)text withKey:(NSString *)key
{
    NSData *data = [text dataUsingEncoding:NSUTF8StringEncoding];
    NSData *result = [self encodeDataXOR:data withKey:key];
    NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    return string;
}


+ (NSData *)encodeDataXOR:(NSData *)data withKey:(NSString *)key
{
    NSMutableData *result = [data mutableCopy];
    
    
    // Get pointer to data to obfuscate
    char *dataPtr = (char *) [result mutableBytes];
    
    // Get pointer to key data
    char *keyData = (char *) [[key dataUsingEncoding:NSUTF8StringEncoding] bytes];
    
    // Points to each char in sequence in the key
    char *keyPtr = keyData;
    int keyIndex = 0;
    
    // For each character in data, xor with current value in key
    for (int x = 0; x < [data length]; x++)
    {
        // Replace current character in data with
        // current character xor'd with current key value.
        // Bump each pointer to the next character
        *dataPtr = *dataPtr ^ *keyPtr;
        dataPtr++;
        keyPtr++;
        
        // If at end of key data, reset count and
        // set key pointer back to start of key value
        if (++keyIndex == [key length])
            keyIndex = 0, keyPtr = keyData;
    }
    
    return result;
}


+ (NSString *)encryptMD4WithString:(NSString *)text
{
    const char *cstr = [text UTF8String];
    unsigned char md4Buffer[CC_MD4_DIGEST_LENGTH];
    CC_MD4(cstr, (int)strlen(cstr), md4Buffer);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD4_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD4_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x",md4Buffer[i]];
    return output;
}


+ (NSString *)encryptMD5WithString:(NSString *)text
{
    const char *cstr = [text UTF8String];
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cstr, (int)strlen(cstr), md5Buffer);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x",md5Buffer[i]];
    return output;
}


+ (NSString *)encryptMD5WithString:(NSString *)text withKey:(NSString *)key
{
    CCHmacContext    ctx;
    const char       *keyChar = [key UTF8String];
    const char       *str = [text UTF8String];
    unsigned char    mac[CC_MD5_DIGEST_LENGTH];
    char             hexmac[2 * CC_MD5_DIGEST_LENGTH + 1];
    char             *p;
    
    CCHmacInit( &ctx, kCCHmacAlgMD5, keyChar, strlen( keyChar ));
    CCHmacUpdate( &ctx, str, strlen(str) );
    CCHmacFinal( &ctx, mac );
    
    p = hexmac;
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++ ) {
        snprintf( p, 3, "%02x", mac[i]);
        p += 2;
    }
    
    return [NSString stringWithUTF8String:hexmac];
}


+ (NSString *)encryptSHA1WithString:(NSString *)text
{
    const char *cstr = [text UTF8String];
    unsigned char sha1Buffer[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1(cstr, (int)strlen(cstr), sha1Buffer);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x",sha1Buffer[i]];
    return output;
}


+ (NSString *)encryptSHA1WithString:(NSString *)text withKey:(NSString *)key
{
    CCHmacContext    ctx;
    const char       *keyChar = [key UTF8String];
    const char       *str = [text UTF8String];
    unsigned char    mac[CC_SHA1_DIGEST_LENGTH];
    char             hexmac[2 * CC_SHA1_DIGEST_LENGTH + 1];
    char             *p;
    
    CCHmacInit( &ctx, kCCHmacAlgSHA1, keyChar, strlen( keyChar ));
    CCHmacUpdate( &ctx, str, strlen(str) );
    CCHmacFinal( &ctx, mac );
    
    p = hexmac;
    for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++ ) {
        snprintf( p, 3, "%02x", mac[i]);
        p += 2;
    }
    
    return [NSString stringWithUTF8String:hexmac];
}


+ (NSString *)encryptSHA256WithString:(NSString *)text
{
    const char *cstr = [text UTF8String];
    unsigned char sha256Buffer[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(cstr, (int)strlen(cstr), sha256Buffer);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x",sha256Buffer[i]];
    return output;
}


+ (NSString *)encryptSHA256WithString:(NSString *)text withKey:(NSString *)key
{
    CCHmacContext    ctx;
    const char       *keyChar = [key UTF8String];
    const char       *str = [text UTF8String];
    unsigned char    mac[CC_SHA256_DIGEST_LENGTH];
    char             hexmac[2 * CC_SHA256_DIGEST_LENGTH + 1];
    char             *p;
    
    CCHmacInit( &ctx, kCCHmacAlgSHA256, keyChar, strlen( keyChar ));
    CCHmacUpdate( &ctx, str, strlen(str) );
    CCHmacFinal( &ctx, mac );
    
    p = hexmac;
    for (int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++ ) {
        snprintf( p, 3, "%02x", mac[i]);
        p += 2;
    }
    
    return [NSString stringWithUTF8String:hexmac];
}


+ (NSString *)encryptSHA512WithString:(NSString *)text
{
    const char *cstr = [text UTF8String];
    unsigned char sha512Buffer[CC_SHA512_DIGEST_LENGTH];
    CC_SHA512(cstr, (int)strlen(cstr), sha512Buffer);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA512_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_SHA512_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x",sha512Buffer[i]];
    return output;
}


+ (NSString *)encryptSHA512WithString:(NSString *)text withKey:(NSString *)key
{
    CCHmacContext    ctx;
    const char       *keyChar = [key UTF8String];
    const char       *str = [text UTF8String];
    unsigned char    mac[CC_SHA512_DIGEST_LENGTH];
    char             hexmac[2 * CC_SHA512_DIGEST_LENGTH + 1];
    char             *p;
    
    CCHmacInit( &ctx, kCCHmacAlgSHA512, keyChar, strlen( keyChar ));
    CCHmacUpdate( &ctx, str, strlen(str) );
    CCHmacFinal( &ctx, mac );
    
    p = hexmac;
    for (int i = 0; i < CC_SHA512_DIGEST_LENGTH; i++ ) {
        snprintf( p, 3, "%02x", mac[i]);
        p += 2;
    }
    
    return [NSString stringWithUTF8String:hexmac];
}


+ (NSData *)generateDataInstanceVectorForAES
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        srand((unsigned)time(NULL));
    });
    
    char cIv[kCCBlockSizeAES128];
    for (int i=0; i < kCCBlockSizeAES128; i++) {
        cIv[i] = rand() % 256;
    }
    return [NSData dataWithBytes:cIv length:kCCBlockSizeAES128];
}


+ (NSString *)generateStringInstanceVectorForAES
{
    return [self encodeBase64WithData:[self generateDataInstanceVectorForAES]];
}


+ (NSData *)generateDataInstanceVectorForDES
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        srand((unsigned)time(NULL));
    });
    
    char cIv[kCCBlockSizeDES];
    for (int i=0; i < kCCBlockSizeDES; i++) {
        cIv[i] = rand() % 256;
    }
    return [NSData dataWithBytes:cIv length:kCCBlockSizeDES];
}


+ (NSString *)generateStringInstanceVectorForDES
{
    return [self encodeBase64WithData:[self generateDataInstanceVectorForDES]];
}


@end
