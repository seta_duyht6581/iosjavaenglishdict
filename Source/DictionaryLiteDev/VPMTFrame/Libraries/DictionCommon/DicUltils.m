//
//  DicUltils.m
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/16/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import "DicUltils.h"
#import "WordItem.h"
#import "VerbItem.h"
#import "NSDateUtils.h"

#define ROW_TABLE(text1,text2,text3) [NSString stringWithFormat:@"<tr><td style=\"text-align: center;\">%@</th><td style=\"text-align: center;\">%@</th><td style=\"text-align: center;\">%@</th></tr>",text1,text2,text3]
#define PART_STRING(text1,text2) [NSString stringWithFormat:@"%@(%@),",text1,text2]

#define LINE_KANJI(text1,text2) [NSString stringWithFormat:@"%@* <b>%@:</b> %@%@",@"<div><font color=\"#000000\" size=\"3.5\">",text1,text2,@"</font></div>"]
#define LINE_SUB_KANJI(text) [NSString stringWithFormat:@"%@%@%@",@"<div><font color=\"#009df7\" size=\"3\">",text,@"</font></div>"]

#define LINE_SUB_1(text) [NSString stringWithFormat:@"%@%@%@",@"<b><font color=\"#000000\" size=\"3.2\">",text,@"</font></b>"]
#define LINE_SUB_2(text) [NSString stringWithFormat:@"%@%@%@",@"<b><font color=\"#007da6\" size=\"3.0\">",text,@"</font></b>"]
#define LINE_SUB_3(text) [NSString stringWithFormat:@"%@%@%@",@"<div style=\"padding-left: 0.5cm;\"><font color=\"#000000\" size=\"3.0\">",text,@"</font></div>"]
#define LINE_TEXT(text) [NSString stringWithFormat:@"%@%@%@",@"<div><font color=\"#000000\" size=\"3.0\">",text,@"</font></div>"]
#define HIGHLINE_TEXT(text) [NSString stringWithFormat:@"%@%@%@",@"<b><font color=\"#08a30c\">",text,@"</font></b>"]

@implementation DicUltils

/*

 
 はーい, こんにちは, どうも, やあ（挨拶に使う）***それから彼は同じ言葉でその老人にこんにちはといいました。$$$Then he said hello to the old man in the same language.###しかし、私たちが口にする第一声は「こんにちは」だ。$$$But the first thing we say is "hello."###こんにちは、トム。$$$Hello, Tom.###「世界のみなさん、こんにちは」$$$"Hello, people of the world!"###こんにちは、私、ナンシーです。$$$Hello, I am Nancy.###彼は会うといつも「こんにちは」と私に言う。$$$He always says "Hello" when I see him.###こんにちは。$$$Hello!###世界、こんにちは！$$$Hello world!###こんにちは、ぼくはライアンです。$$$Hello, I'm Ryan.###皆さんどうも、マイクと申します。$$$Hello everyone, I'm Mike.
 おーい
 もしもし***もしもしジョンソンさんをお願いします。$$$Hello. May I speak to Mr Johnson, please?###もしもしジョーカールトンですが、マイケル君をおねがいしたいのですが。$$$Hello. This is Joe Carlton. May I speak to Michael?###もしもし、日本から来た佐藤智子です。$$$Hello, I'm Tomoko Sato from Japan.###もしもし、人事課ですが。$$$Hello, is this the personnel department?###もしもし、後藤さんのお宅ですか。$$$Hello. Is this the Gotos' residence?###もしもし、会計士はいますか。$$$Hello, is the accountant there, please?###もしもし、フリーマン氏はおられますか。$$$Hello, is Mr Freeman in?###もしもし、こちらはマイクです。$$$Hello, this is Mike.###「もしもし、ブラウンさんですか」「はい、そうです」$$$"Hello, is this Mrs. Brown?" "Yes, this is Mrs. Brown."###「もしもし、お母さん。お母さんよね。」と彼女が言う。$$$"Hello, Mum. Is that you?", she says.
 おや（注意を引くのに使う）


 */

+(NSString*)parseMeanEVWordtoHtml:(WordItem*)wordItem
{
    //\n,***,###,$$$
    NSArray *datas = [wordItem.toStr componentsSeparatedByString:@"\n"];

    
    NSMutableString *htmlString = [[NSMutableString alloc] init];
    [htmlString appendString:@"<html>"];
    [htmlString appendString:@"<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\" />"];
    [htmlString appendString:@"<meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;\"/>"];
    [htmlString appendString:@"<body>"];
    
    for (NSString *line in datas ) {
        if ([[line trims] isEqualToString:@""]) {
            break;
        }
        NSArray *arrline = [line componentsSeparatedByString:@"***"];
        [htmlString appendString:LINE_SUB_1(@"* ")];
        NSString *line0 = [arrline objectAtIndex:0];
        [htmlString appendString:LINE_SUB_1(line0)];
        [htmlString appendString:@"<br>"];
        if ([arrline count]>1) {
            NSArray *examples = [[arrline objectAtIndex:1] componentsSeparatedByString:@"###"];
            for (NSString *example in examples) {
                NSArray *exampleReplace = [example componentsSeparatedByString:@"$$$"];
                NSString *newword = [exampleReplace objectAtIndex:0];
                NSString *trans = [exampleReplace objectAtIndex:1];
                if (wordItem.tableType == TYPE_EV) {
                    newword = [exampleReplace objectAtIndex:1];
                    trans = [exampleReplace objectAtIndex:0];
                }
                
                NSRange rang = [line0 rangeOfString:@")" options:NSBackwardsSearch];
                NSString *news = line0;
                
                if (rang.location!=NSNotFound) {
                    news = [line0 substringFromIndex:rang.location+1];
                }
                
                NSArray *words = [news componentsSeparatedByString:@","];
                for (NSString *word in words) {
                    if ([newword containsString:[word trims]]) {
                        newword =  [newword stringByReplacingOccurrencesOfString:[word trims] withString:HIGHLINE_TEXT([word trims])];
                        break;
                    }
                    
                }
                //stringByReplacingOccurrencesOfString:(NSString *)target withString:(NSString *)replacement options:(NSStringCompareOptions)options range:(NSRange)searchRange;
                trans = [trans stringByReplacingOccurrencesOfString:wordItem.fromSign withString:HIGHLINE_TEXT(wordItem.fromSign) options:NSCaseInsensitiveSearch range:NSMakeRange(0, trans.length)];
                if (wordItem.tableType == TYPE_EV) {
                    if (![trans containsString:wordItem.fromSign]) {
                        trans = [trans stringByReplacingOccurrencesOfString:wordItem.pronuncea withString:HIGHLINE_TEXT(wordItem.pronuncea) options:NSCaseInsensitiveSearch range:NSMakeRange(0, trans.length)];
                    }
                    
                }
                
                

                NSString *tmp = [NSString stringWithFormat:@"● <font color = \"357b9f\">%@</font><br>● %@",newword,trans];
                [htmlString appendString:LINE_SUB_3(tmp)];
                [htmlString appendString:@"<br>"];
            }
            
        }
        
    }
    
    [htmlString appendString:@"</body>"];
    [htmlString appendString:@"</html>"];
    
    return htmlString;
}
+(NSString*)parseKanjiWordtoHtml:(VerbItem*)wordItem
{
    NSString* content;
    if (wordItem.img!= nil && wordItem.img.length > 1) {
        
        content = [[AppDelegate sharedManager].contentHtml stringByReplacingOccurrencesOfString:@"kanjidivdisplay" withString:@"block"];
        wordItem.img = [wordItem.img stringByReplacingOccurrencesOfString:@" kvg:" withString:@" kvgggg"];
        content = [content stringByReplacingOccurrencesOfString:@"contentwordelement" withString:wordItem.img];
        content = [content stringByReplacingOccurrencesOfString:@"kanjisymble" withString:wordItem.Nhat];
    }else
    {
         content = [[AppDelegate sharedManager].contentHtml stringByReplacingOccurrencesOfString:@"kanjidivdisplay" withString:@"none"];
    }
    
    
    //contentofWordDetail
    NSMutableString *htmlString = [[NSMutableString alloc] init];
    //[htmlString appendString:LINE_KANJI(@"Âm Hán", wordItem.Han)];
    [htmlString appendString:LINE_KANJI(@"Onyomi", wordItem.OnYomi)];
    [htmlString appendString:LINE_KANJI(@"Kunyomi", wordItem.kunYomi)];
    if (wordItem.stroke_count == [NSNull null]) {
         [htmlString appendString:LINE_KANJI(@"Stroke count",@"" )];
    }else {
        [htmlString appendString:LINE_KANJI(@"Stroke count", [wordItem.stroke_count stringValue])];
    }
    NSMutableString *thanhPhan = [[NSMutableString alloc] init];
    if (wordItem.parts != nil && wordItem.parts.length > 0) {
        NSData* data = [wordItem.parts dataUsingEncoding:NSUTF8StringEncoding];
        NSArray *values = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        
        for (NSDictionary *item in values) {
            NSString* han = [item objectOrNilForKey:@"w"];
            NSString * nghia = [item objectOrNilForKey:@"h"];
            [thanhPhan appendString:PART_STRING(han,nghia)];
        }
    }
    
    [htmlString appendString:LINE_KANJI(@"Mean", @"")];
    NSString *linemean = [wordItem.Content stringByReplacingOccurrencesOfString:@"_" withString:@", "];
    [htmlString appendString:LINE_SUB_3(linemean)];
    /*NSArray *array = [wordItem.Content componentsSeparatedByString:@"_"];

    for (NSString* item in array) {
        NSString *line = [NSString stringWithFormat:@", %@",item];
        [htmlString appendString:LINE_SUB_2(line)];
    }*/
    content = [content stringByReplacingOccurrencesOfString:@"contentofWordDetail" withString:htmlString];
    
    //table
    //tableexample
    NSMutableString *example = [[NSMutableString alloc] initWithString:@""];
    if (wordItem.examples != nil && wordItem.examples.length > 0) {
        NSArray *values = [wordItem.examples componentsSeparatedByString:@";"];
        for (NSString *item in values) {
            NSArray *parts = [item componentsSeparatedByString:@"_"];
            if (parts==nil) {
                continue;
            }
            if (parts.count > 2) {
                 [example appendString:ROW_TABLE([[parts objectAtIndex:0] stringByReplacingOccurrencesOfString:@"|" withString:@", "], [[parts objectAtIndex:1] stringByReplacingOccurrencesOfString:@"|" withString:@", "], [[parts objectAtIndex:2] stringByReplacingOccurrencesOfString:@"|" withString:@", "])];
            }else
            if ( parts.count > 1) {
                [example appendString:ROW_TABLE([[parts objectAtIndex:0] stringByReplacingOccurrencesOfString:@"|" withString:@", "], [[parts objectAtIndex:1] stringByReplacingOccurrencesOfString:@"|" withString:@", "], @"")];
            } else
            if (parts.count > 0) {
                [example appendString:ROW_TABLE([[parts objectAtIndex:0] stringByReplacingOccurrencesOfString:@"|" withString:@", "], @"",@"")];
            }
            
           
        }
        
    }
    content = [content stringByReplacingOccurrencesOfString:@"tableexample" withString:example];
    
    
    return content;
}

+(NSString*)parseMeanGrammertoHtml:(GrammerItem*)wordItem
{
    
    NSArray *datas = [wordItem.toStr componentsSeparatedByString:@"\n"];
    NSMutableArray *array = [[NSMutableArray alloc] initWithArray:datas];
    
    NSMutableString *htmlString = [[NSMutableString alloc] init];
    [htmlString appendString:@"<html>"];
    [htmlString appendString:@"<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\" />"];
    [htmlString appendString:@"<meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;\"/>"];
    [htmlString appendString:@"<body>"];
    
    [htmlString appendString:LINE_SUB_1(@"+ Ngữ pháp")];
    [htmlString appendString:LINE_SUB_3(wordItem.fromStr)];
    [htmlString appendString:LINE_SUB_1(@"</br>+ Level")];
    NSString *level = [NSString stringWithFormat:@"N%d",wordItem.level.intValue];
    [htmlString appendString:LINE_SUB_3(level)];
    [htmlString appendString:LINE_SUB_1(@"</br>+ Giải thích")];
    
    for (NSString *lineitem in array)
    {
        NSString *lineS = [NSString stringWithFormat:@"- %@</br>",lineitem];
        [htmlString appendString:LINE_SUB_3(lineS)];
    }
    
    [htmlString appendString:@"</body>"];
    [htmlString appendString:@"</html>"];
    
    return htmlString;
}

+(BOOL)isShowAds
{
    NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
    BOOL myBool = [[infoDictionary objectForKey:@"ShowAds"] boolValue];
    //neu khong show ads
    if (!myBool) {
        return false;
    }
    
    ConfigUserDefault *config = [self configUserDefault];
    if ([NSDate isEarlierThanDate:config.dateRemoveAds]) {
        return false;
    }
    
    return myBool;
}

+(void)showReviewPopUpInViewController:(UIViewController*)controller
{
    
      [AlertViewUniversal showAlertInViewController:controller withTitle:TITLE_RATEAPP message:RATE_APP_MESSAGE cancelButtonTitle:RATE_APP_NO_THANKS destructiveButtonTitle:nil otherButtonTitles:@[RATE_APP_RATE_NOW,RATE_APP_REMIND_LATER] tapBlock:^(AlertViewUniversal * _Nonnull alert, NSInteger buttonIndex) {
            if (buttonIndex == alert.cancelButtonIndex) {
                [UserDefault addObjectForKey:REVIEW_CANCEL key:KEY_REVIEW];
            } else if (buttonIndex == alert.firstOtherButtonIndex) {
                [UserDefault addObjectForKey:REVIEWED key:KEY_REVIEW];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:ITUN_MYAPP]];
            } else if (buttonIndex == alert.firstOtherButtonIndex + 1) {
                [UserDefault addObjectForKey:REVIEW_LATER key:KEY_REVIEW];
                NSNumber *number = [UserDefault getObjectWithKey:KEY_REVIEW_LATER];
                if (number == nil) number = [NSNumber numberWithInt:0];
                number = [NSNumber numberWithInt:number.intValue+1];
                [UserDefault addObjectForKey:number key:KEY_REVIEW_LATER];
            }
            
        }];
    
}

+(ConfigUserDefault*) configUserDefault
{
   ConfigUserDefault *result = [UserDefault loadCustomObjectWithKey:KEY_ConfigUserDefault];
    if (result == nil) {
        result = [[ConfigUserDefault alloc] init];
        result.isFirstStart = YES;
        result.numRequestFullAds = 0;
        result.numRequestBannerAds = 0;
        result.dateRemoveAds = [NSDate date];
    }
    return result;
}

+ (BOOL)canShowFullAds
{
    ConfigUserDefault *config = [self configUserDefault];
    if (config.numRequestFullAds >= 1000) {
        config.numRequestFullAds = 40;
    }
    config.numRequestFullAds++;
    //sysnchronize
    [UserDefault saveCustomObject:config key:KEY_ConfigUserDefault];
    if (config.numRequestFullAds > 40 && config.numRequestFullAds % 20 == 0) {
        return true;
    }
    return false;
}

+ (BOOL)canShowBannerAds
{
    ConfigUserDefault *config = [self configUserDefault];
    if (config.numRequestBannerAds >= 100) {
        config.numRequestBannerAds = 10;
    }
    config.numRequestBannerAds++;
    //sysnchronize
    [UserDefault saveCustomObject:config key:KEY_ConfigUserDefault];
    return true;
}

@end
