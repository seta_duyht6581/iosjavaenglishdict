//
//  DicUltils.h
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/16/15.
//  Copyright (c) 2015 CHUONGPD2. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WordItem.h"
#import "VerbItem.h"
#import "GrammerItem.h"
#import "ConfigUserDefault.h"
@interface DicUltils : NSObject
/**
 *  Parse to html string
 *
 */


+(NSString*)parseMeanEVWordtoHtml:(WordItem*)wordItem;
+(NSString*)parseKanjiWordtoHtml:(VerbItem*)wordItem;
+(NSString*)parseMeanGrammertoHtml:(GrammerItem*)wordItem;
+(void)showReviewPopUpInViewController:(UIViewController*)controller;
+(BOOL)isShowAds;
+(ConfigUserDefault*) configUserDefault;
+ (BOOL)canShowFullAds;
+ (BOOL)canShowBannerAds;
@end
