//
//  AlertViewUniversal.m
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/25/15.
//  Copyright © 2015 CHUONGPD2. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIAlertView+Blocks/UIAlertView+Blocks.h>
#import <UIAlertController+Blocks/UIAlertController+Blocks.h>

#import "AlertViewUniversal.h"

static NSInteger const AlertViewUniversalNoButtonExistsIndex = -1;

static NSInteger const AlertViewUniversalCancelButtonIndex = 0;
static NSInteger const AlertViewUniversalDestructiveButtonIndex = 1;
static NSInteger const AlertViewUniversalFirstOtherButtonIndex = 2;

@interface AlertViewUniversal ()

@property (nonatomic) UIAlertController *alertController;
@property (nonatomic) UIAlertView *alertView;

@property (nonatomic, assign) BOOL hasCancelButton;
@property (nonatomic, assign) BOOL hasDestructiveButton;
@property (nonatomic, assign) BOOL hasOtherButtons;

@end

@implementation AlertViewUniversal

+ (instancetype)showAlertInViewController:(UIViewController *)viewController
                                withTitle:(NSString *)title
                                  message:(NSString *)message
                        cancelButtonTitle:(NSString *)cancelButtonTitle
                   destructiveButtonTitle:(NSString *)destructiveButtonTitle
                        otherButtonTitles:(NSArray *)otherButtonTitles
                                 tapBlock:(AlertViewUniversalCompletionBlock)tapBlock
{
    AlertViewUniversal *alert = [[AlertViewUniversal alloc] init];
    
    alert.hasCancelButton = cancelButtonTitle != nil;
    alert.hasDestructiveButton = destructiveButtonTitle != nil;
    alert.hasOtherButtons = otherButtonTitles.count > 0;
    
    if ([UIAlertController class]) {
        alert.alertController = [UIAlertController showAlertInViewController:viewController
                                                                   withTitle:title message:message
                                                           cancelButtonTitle:cancelButtonTitle
                                                      destructiveButtonTitle:destructiveButtonTitle
                                                           otherButtonTitles:otherButtonTitles
                                                                    tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                                                                        if (tapBlock) {
                                                                            tapBlock(alert, buttonIndex);
                                                                        }
                                                                    }];
    } else {
        NSMutableArray *other = [NSMutableArray array];
        
        if (destructiveButtonTitle) {
            [other addObject:destructiveButtonTitle];
        }
        
        if (otherButtonTitles) {
            [other addObjectsFromArray:otherButtonTitles];
        }
        
        alert.alertView =  [UIAlertView showWithTitle:title
                                              message:message
                                    cancelButtonTitle:cancelButtonTitle
                                    otherButtonTitles:other
                                             tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex){
                                                 if (tapBlock) {
                                                     if (buttonIndex == alertView.cancelButtonIndex) {
                                                         tapBlock(alert, AlertViewUniversalCancelButtonIndex);
                                                     } else if (destructiveButtonTitle) {
                                                         if (buttonIndex == alertView.firstOtherButtonIndex) {
                                                             tapBlock(alert, AlertViewUniversalDestructiveButtonIndex);
                                                         } else if (otherButtonTitles.count) {
                                                             NSInteger otherOffset = buttonIndex - alertView.firstOtherButtonIndex;
                                                             tapBlock(alert, AlertViewUniversalFirstOtherButtonIndex + otherOffset - 1);
                                                         }
                                                     } else if (otherButtonTitles.count) {
                                                         NSInteger otherOffset = buttonIndex - alertView.firstOtherButtonIndex;
                                                         tapBlock(alert, AlertViewUniversalFirstOtherButtonIndex + otherOffset);
                                                     }
                                                 }
                                             }];
    }
    
    return alert;
}


#pragma mark -

- (BOOL)visible
{
    if (self.alertController) {
        return self.alertController.visible;
    } else if (self.alertView) {
        return self.alertView.visible;
    }
    NSAssert(false, @"Unsupported alert.");
    return NO;
}

- (NSInteger)cancelButtonIndex
{
    if (!self.hasCancelButton) {
        return AlertViewUniversalNoButtonExistsIndex;
    }
    
    return AlertViewUniversalCancelButtonIndex;
}

- (NSInteger)firstOtherButtonIndex
{
    if (!self.hasOtherButtons) {
        return AlertViewUniversalNoButtonExistsIndex;
    }
    
    return AlertViewUniversalFirstOtherButtonIndex;
}

- (NSInteger)destructiveButtonIndex
{
    if (!self.hasDestructiveButton) {
        return AlertViewUniversalNoButtonExistsIndex;
    }
    
    return AlertViewUniversalDestructiveButtonIndex;
}

@end

