//
//  AlertViewUniversal.h
//  VPMTFrameExample
//
//  Created by Hoang Tan Duy on 10/25/15.
//  Copyright © 2015 CHUONGPD2. All rights reserved.
//
@class AlertViewUniversal;

typedef void(^AlertViewUniversalCompletionBlock)(AlertViewUniversal * __nonnull alert, NSInteger buttonIndex);

@interface AlertViewUniversal : NSObject
+ (nonnull instancetype)showAlertInViewController:(nonnull UIViewController *)viewController
                                        withTitle:(nullable NSString *)title
                                          message:(nullable NSString *)message
                                cancelButtonTitle:(nullable NSString *)cancelButtonTitle
                           destructiveButtonTitle:(nullable NSString *)destructiveButtonTitle
                                otherButtonTitles:(nullable NSArray *)otherButtonTitles
                                         tapBlock:(nullable AlertViewUniversalCompletionBlock)tapBlock;
@property (readonly, nonatomic) BOOL visible;
@property (readonly, nonatomic) NSInteger cancelButtonIndex;
@property (readonly, nonatomic) NSInteger firstOtherButtonIndex;
@property (readonly, nonatomic) NSInteger destructiveButtonIndex;
@end