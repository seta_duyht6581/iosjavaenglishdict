

#import <UIKit/UIKit.h>


@class DHSearchBar;

/**
 *  The delegate is responsible for providing values to the search bar that it can use to determine its size.
 */

@protocol DHSearchBarDelegate <NSObject>

- (void)searchBarTextDidChange:(DHSearchBar *)searchBar;

@end

/**
 *  An animating search bar.
 */

@interface DHSearchBar : UIView

/**
 *  The text field used for entering search queries. Visible only when search is active.
 */

/**
 *  The borderedframe of the search bar. Visible only when search mode is active.
 */

@property (nonatomic, strong) UIView *searchFrame;

@property (nonatomic, strong) UITextField *searchField;

/**
 *  The image view containing the search magnifying glass icon in white. Visible when search is not active.
 */

@property (nonatomic, strong) UIImageView *searchImageViewOff;

/**
 *  The image view containing the search magnifying glass icon in blue. Visible when search is active.
 */

@property (nonatomic, strong) UIImageView *searchImageViewOn;

/**
 *  The image view containing the circle part of the magnifying glass icon in blue.
 */

@property (nonatomic, strong) UIImageView *searchImageCircle;

/**
 *  The image view containing the left cross part of the magnifying glass icon in blue.
 */

@property (nonatomic, strong) UIImageView *searchImageCrossLeft;

/**
 *  The image view containing the right cross part of the magnifying glass icon in blue.
 */

@property (nonatomic, strong) UIImageView *searchImageCrossRight;

/**
 *  The frame of the search bar before a transition started. Only set if delegate is not nil.
 */

@property (nonatomic, assign) CGRect	originalFrame;

/**
 *  The (optional) delegate is responsible for providing values necessary for state change animations of the search bar. @see INSSearchBarDelegate.
 */

@property (nonatomic, weak) id<DHSearchBarDelegate>	delegate;

@end
