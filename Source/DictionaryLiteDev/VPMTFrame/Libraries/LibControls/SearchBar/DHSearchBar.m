//
//  INSSearchBar.m
//  Berlin Insomniac
//
//  Created by Salánki, Benjámin on 06/03/14.
//  Copyright (c) 2014 Berlin Insomniac. All rights reserved.
//

#import "DHSearchBar.h"

@interface DHSearchBar () <UITextFieldDelegate, UIGestureRecognizerDelegate>




@end


@implementation DHSearchBar

#pragma mark - initialization

- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:CGRectMake(CGRectGetMinX(frame), CGRectGetMinY(frame), CGRectGetWidth(frame), 34.0)]))
	{
        // Initialization code
		self.opaque = NO;
		self.backgroundColor = [UIColor clearColor];
		
		self.searchFrame = [[UIView alloc] initWithFrame:self.bounds];
		self.searchFrame.backgroundColor = [UIColor whiteColor];
		self.searchFrame.opaque = NO;
            
        self.searchFrame.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.searchFrame.layer.masksToBounds = YES;
        self.searchFrame.layer.cornerRadius = 5.0f;
        self.searchFrame.layer.borderWidth = 0.5;
        self.searchFrame.layer.borderColor = [UIColor clearColor].CGColor;
        self.searchFrame.contentMode = UIViewContentModeRedraw;
        [self addSubview:self.searchFrame];
        
        //create imageSearch;
        _searchImageCircle = [[UIImageView alloc] initWithFrame:CGRectMake(5, 8, 15, 20)];
        [_searchImageCircle setBackgroundColor:[UIColor clearColor]];
        _searchImageCircle.contentMode = UIViewContentModeScaleAspectFit;
        [_searchImageCircle setImage:[UIImage imageNamed:@"nav_search_icon_gray"]];
        [self.searchFrame addSubview:_searchImageCircle];
        
        int width = self.bounds.size.width;
        
        _searchField = [[UITextField alloc] initWithFrame:CGRectMake(28, 3,width - 28,26)];
        [_searchField setBackgroundColor:[UIColor clearColor]];
        _searchField.font = [UIFont systemFontOfSize:14.0f];
        _searchField.keyboardType = UIKeyboardTypeWebSearch;
        _searchField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _searchField.placeholder = @"Enter word to search";
        [self.searchFrame addSubview:_searchField];
        
    
    }
	
    return self;
}





#pragma mark - cleanup

- (void)dealloc
{

}

@end
