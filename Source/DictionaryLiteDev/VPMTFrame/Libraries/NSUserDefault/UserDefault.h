//
//  NHUserDefault.h
//  ViettelPI
//
//  Created by nhantd on 3/11/14.
//  Copyright (c) 2014 viettel. All rights reserved.
//

#import "JNKeychain.h"

@interface UserDefault : NSObject 

+ (void)addObjectForKey:(id)value key:(NSString*)key;
+ (id)getObjectWithKey:(NSString*)key;

+ (void)addBoolForKey:(BOOL)value key:(NSString*)key;
+ (BOOL)getBoolWithKey:(NSString*)key;

+ (void)removeObjectForKey:(NSString*)key;

+(NSString*)getKeychainValueForKey:(NSString*)key;

+(void)storeKeychainValue:(NSString*)value forKey:(NSString*)key;

+(void)removeKeychainForKey:(NSString*)key;
/**
 *  @author DuyHt, 15-11-15 15:11:45
 *
 *  Save custom object can co function Encodeder va decoder
 *
 */
+ (void)saveCustomObject:(id)object key:(NSString *)key ;

/**
 *  @author DuyHt, 15-11-15 15:11:49
 *
 */
+ (id)loadCustomObjectWithKey:(NSString *)key;
@end
